﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace BoE.ClientUpdater
{
    class Program
    {
        const string FTP_URL = @"ftp://ftp.battleofeurope.net";

        static void Main(string[] args)
        {
            string updaterPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            try
            {
                string launcherPath = args[0];
                FtpClient ftpClient = new FtpClient(FTP_URL, "anonymous", "");
                Console.WriteLine("Connecting to ftp");

                Console.WriteLine("Searching for the latest launcher");
                IEnumerable<string> files = ftpClient.DirectoryListSimple("");
                string boeLauncherPath = files.FirstOrDefault(f => f.StartsWith("boe_launcher"));

                if (string.IsNullOrWhiteSpace(boeLauncherPath))
                    throw new Exception("Lastest launcher not found on the ftp");

                Console.WriteLine("Downloading the new launcher");
                ftpClient.Download(boeLauncherPath, launcherPath);

                Console.WriteLine("Starting the new launcher");
                Process.Start(launcherPath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetBaseException().Message);
                Console.ReadLine();
            }
            finally
            {
                Console.WriteLine("Deleting itself, bye bye");
                ProcessStartInfo Info = new ProcessStartInfo();
                Info.Arguments = string.Format("/C choice /C Y /N /D Y /T 3 & Del {0}", updaterPath);
                Info.WindowStyle = ProcessWindowStyle.Hidden;
                Info.CreateNoWindow = true;
                Info.FileName = "cmd.exe";
                Process.Start(Info);
            }
            return;
        }
    }
}
