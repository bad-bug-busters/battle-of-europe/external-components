﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public abstract class UserAction
    {
        public UserAction()
        {
            CreationDate = DateTime.Now;
        }

        public int UserActionId { get; set; }

        public string AddressIp { get; set; }

        public DateTime CreationDate { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int? GameServerId { get; set; }
        public virtual GameServer GameServer { get; set; }
    }
}
