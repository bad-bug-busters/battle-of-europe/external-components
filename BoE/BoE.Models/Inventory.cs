﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Inventory
    {
        public Inventory()
        {
            CreationDate = DateTime.Now;
        }

        [Key]
        [DataMember]
        public int InventoryId { get; set; }

        [DataMember]
        public int? RepairPrice { get; set; }

        [DataMember]
        public bool Disabled { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual Item Item { get; set; }
        [DataMember]
        public int ItemId { get; set; }

        public virtual Character Character { get; set; }
        [DataMember]
        public int CharacterId { get; set; }

        public virtual ICollection<Build> BuildItem1s { get; set; }
        public virtual ICollection<Build> BuildItem2s { get; set; }
        public virtual ICollection<Build> BuildItem3s { get; set; }
        public virtual ICollection<Build> BuildItem4s { get; set; }
        public virtual ICollection<Build> BuildHeads { get; set; }
        public virtual ICollection<Build> BuildBodies { get; set; }
        public virtual ICollection<Build> BuildFoots { get; set; }
        public virtual ICollection<Build> BuildHands { get; set; }
        public virtual ICollection<Build> BuildHorses { get; set; }
    }
}
