﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Good : Item
    {
        [DataMember]
        public string Description { get; set; }
    }
}
