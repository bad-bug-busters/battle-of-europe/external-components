﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Loom
    {
        public Loom()
        {
            Level = LoomLevel.One;
            CreationDate = DateTime.Now;
        }

        [DataMember]
        public int LoomId { get; set; }

        [DataMember]
        public LoomType Type { get; set; }

        [DataMember]
        public LoomLevel Level { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual Character Character { get; set; }
        public int CharacterId { get; set; }
    }
}
