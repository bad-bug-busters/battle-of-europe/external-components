﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Runtime.Serialization;

namespace BoE.Models
{
    [DataContract]
    public abstract class Item
    {
        [Key]
        [DataMember]
        public int ItemId { get; set; }

        [Required]
        public string NameId { get; set; }

        [Required]
        public int GameId { get; set; }

        [DataMember]
        public LoomLevel LoomLevel { get; set; }

        [DataMember]
        public ItemSort ItemSort { get; set; }

        [DataMember]
        public ItemType Type { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int HitPoint { get; set; }

        [DataMember]
        public int Difficulty { get; set; }

        [DataMember]
        public int Value { get; set; }

        public bool FromKind { get; set; }

        public virtual Item NextLoom { get; set; }
        [DataMember]
        public int? NextLoomId { get; set; }

        public virtual Item PrevLoom { get; set; }
        [DataMember]
        public int? PrevLoomId { get; set; }

        public virtual ICollection<Inventory> Inventories { get; set; }
        public virtual ICollection<UserUpKeepUsage> UserUpKeepUsages { get; set; }

        public int SellValue
        {
            get { return (int)(Value / 2); }
        }

        public int BuyValue 
        {
            get 
            {
                return Value;
            } 
        }

        [DataMember]
        public bool Shopable
        {
            get { return !(Name.Contains("{!}") || NameId.Contains("_alt_mode")); }
        }

        public List<string[]> UpdateItem(Item item)
        {
            List<string[]> changes = new List<string[]>() { new string[] { item.Name } };
            Type itemType = item.GetType();
            Type type = this.GetType();
            PropertyInfo[] itemProps = itemType.GetProperties();

            foreach (PropertyInfo property in itemProps)
            {
                object itemVal = property.GetValue(item, null);
                if (itemVal == null)
                    continue;
                Type propType = itemVal.GetType();
                if (propType.IsPrimitive || propType == typeof(Decimal) || propType == typeof(String) || propType.IsEnum)
                {
                    object val = type.GetProperty(property.Name).GetValue(this, null);
                    if (!itemVal.Equals(val))
                    {
                        type.GetProperty(property.Name).SetValue(this, itemVal, null);
                        if (property.Name != "GameId")
                            changes.Add(new string[] {  property.Name, val.ToString(), itemVal.ToString() });
                    }
                }
            }

            return changes;
        }

        [DataMember]
        public LoomType? LoomType
        {
            get
            {
                if (ItemSort != ItemSort.Normal)
                    return null;

                if (Type == ItemType.Head)
                {
                    ArmorType armorType = ((Armor)this).ArmorType;
                    if (armorType == ArmorType.Light)
                        return (LoomType?)BoE.Models.LoomType.LHead;
                    if (armorType == ArmorType.Medium)
                        return (LoomType?)BoE.Models.LoomType.MHead;
                    if (armorType == ArmorType.Heavy)
                        return (LoomType?)BoE.Models.LoomType.HHead;
                }
                if (Type == ItemType.Body)
                {
                    ArmorType armorType = ((Armor)this).ArmorType;
                    if (armorType == ArmorType.Light)
                        return (LoomType?)BoE.Models.LoomType.LBody;
                    if (armorType == ArmorType.Medium)
                        return (LoomType?)BoE.Models.LoomType.MBody;
                    if (armorType == ArmorType.Heavy)
                        return (LoomType?)BoE.Models.LoomType.HBody;
                }
                if (Type == ItemType.Hand)
                {
                    ArmorType armorType = ((Armor)this).ArmorType;
                    if (armorType == ArmorType.Light)
                        return (LoomType?)BoE.Models.LoomType.LHand;
                    if (armorType == ArmorType.Medium)
                        return (LoomType?)BoE.Models.LoomType.MHand;
                    if (armorType == ArmorType.Heavy)
                        return (LoomType?)BoE.Models.LoomType.HHand;
                }
                if (Type == ItemType.Foot)
                {
                    ArmorType armorType = ((Armor)this).ArmorType;
                    if (armorType == ArmorType.Light)
                        return (LoomType?)BoE.Models.LoomType.LFoot;
                    if (armorType == ArmorType.Medium)
                        return (LoomType?)BoE.Models.LoomType.MFoot;
                    if (armorType == ArmorType.Heavy)
                        return (LoomType?)BoE.Models.LoomType.HFoot;
                }
                if (Type == ItemType.OneH)
                    return (LoomType?)BoE.Models.LoomType.OneH;
                if (Type == ItemType.OneHalf)
                    return (LoomType?)BoE.Models.LoomType.OneHalf;
                if (Type == ItemType.TwoH)
                    return (LoomType?)BoE.Models.LoomType.TwoH;
                if (Type == ItemType.Polearm)
                    return (LoomType?)BoE.Models.LoomType.Polearm;
                if (Type == ItemType.XBow || Type == ItemType.Bolt)
                    return (LoomType?)BoE.Models.LoomType.XBow;
                if (Type == ItemType.Bow || Type == ItemType.Arrow)
                    return (LoomType?)BoE.Models.LoomType.Bow;
                if (Type == ItemType.Throwing)
                    return (LoomType?)BoE.Models.LoomType.Throwing;
                if (Type == ItemType.Musket || Type == ItemType.Pistol || Type == ItemType.Bullet)
                    return (LoomType?)BoE.Models.LoomType.Musket;

                return null;
            }
        }
    }
}
