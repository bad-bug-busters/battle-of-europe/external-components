﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class User
    {
        public User()
        {
            CreationDate = DateTime.Now;
            AccountEnabled = false;
        }

        [Key]
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string Username { get; set; }

        public string Password { get; set; }

        public UserState State { get; set; }

        public int? UniqueId { get; set; }

        public bool AccountEnabled { get; set; }
        public DateTime? LastPasswordFailure { get; set; }
        public DateTime? LastPasswordSuccess { get; set; }
        public int PasswordFailures { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        public virtual Character Character { get; set; }

        public virtual ICollection<Code> Codes { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<News> Newses { get; set; }
        public virtual ICollection<UserAction> UserActions { get; set; }
    }
}
