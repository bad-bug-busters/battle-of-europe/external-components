﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Build
    {
        [DataMember]
        public int BuildId { get; set; }

        [DataMember]
        public string Name { get; set; }

        public virtual Character Character { get; set; }
        [DataMember]
        public int CharacterId { get; set; }

        public virtual Inventory Item1 { get; set; }
        [DataMember]
        public int? Item1Id { get; set; }

        public virtual Inventory Item2 { get; set; }
        [DataMember]
        public int? Item2Id { get; set; }

        public virtual Inventory Item3 { get; set; }
        [DataMember]
        public int? Item3Id { get; set; }

        public virtual Inventory Item4 { get; set; }
        [DataMember]
        public int? Item4Id { get; set; }

        public virtual Inventory Head { get; set; }
        [DataMember]
        public int? HeadId { get; set; }

        public virtual Inventory Body { get; set; }
        [DataMember]
        public int? BodyId { get; set; }

        public virtual Inventory Foot { get; set; }
        [DataMember]
        public int? FootId { get; set; }

        public virtual Inventory Hand { get; set; }
        [DataMember]
        public int? HandId { get; set; }

        public virtual Inventory Horse { get; set; }
        [DataMember]
        public int? HorseId { get; set; }

        [DataMember]
        public int Strength { get; set; }
        [DataMember]
        public int Agility { get; set; }

        [DataMember]
        public int HorseArchery { get; set; }
        [DataMember]
        public int Riding { get; set; }
        [DataMember]
        public int Athletics { get; set; }
        [DataMember]
        public int WeaponMaster { get; set; }
        [DataMember]
        public int Resistance { get; set; }
        [DataMember]
        public int PowerThrow { get; set; }
        [DataMember]
        public int PowerStrike { get; set; }
        [DataMember]
        public int PowerPull { get; set; }
        [DataMember]
        public int PowerReload { get; set; }
        [DataMember]
        public int OffHand { get; set; }
        
        [DataMember]
        public int OneHanded { get; set; }
        [DataMember]
        public int TwoHanded { get; set; }
        [DataMember]
        public int Polearm { get; set; }
        [DataMember]
        public int Archery { get; set; }
        [DataMember]
        public int Crossbow { get; set; }
        [DataMember]
        public int Throwing { get; set; }
        [DataMember]
        public int Firearm { get; set; }

        public int GetMaxSkillPoint
        {
            get
            {
                return Character != null ? Character.Level : 0;
            }
        }
        public int GetMaxAttPoint
        {
            get
            {
                return Character != null ? Character.Level + 5 : 0;
            }
        }
        public int GetMaxWpfPoint
        {
            get
            {
                return Character != null ? WeaponMaster * 50 : 0;
            }
        }

        public List<int> GetAttsList
        {
            get
            {
                return new List<int>()
                {
                    Strength,
                    Agility
                };
            }
        }
        public List<int> GetSkillsList
        {
            get
            {
                return new List<int>()
                {
                    HorseArchery,
                    Riding,
                    Athletics,
                    WeaponMaster,
                    Resistance,
                    PowerThrow,
                    PowerStrike,
                    PowerPull,
                    PowerReload,
                    OffHand
                };
            }
        }
        public List<int> GetWPFsList
        {
            get
            {
                return new List<int>()
                {
                    OneHanded,
                    TwoHanded,
                    Polearm,
                    Archery,
                    Crossbow,
                    Throwing,
                    Firearm
                };
            }
        }

        public List<int> GetStrSkillList 
        {
            get {
                return new List<int>()
                {
                    PowerThrow,
                    PowerStrike,
                    PowerPull,
                    PowerReload,
                    OffHand
                };
            }
        }
        public List<int> GetAgiSkillList
        {
            get
            {
                return new List<int>()
                {
                    HorseArchery,
                    Riding,
                    Athletics,
                    WeaponMaster,
                    Resistance
                };
            }
        }
    }
}
