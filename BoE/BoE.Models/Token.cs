﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class Token
    {
        public Token()
        {
            CreationDate = DateTime.Now;
            Enable = true;
        }

        public int TokenId { get; set; }

        public string Value { get; set; }

        public DateTime Dealine { get; set; }

        public bool Enable { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual GameServer GameServer { get; set; }
        public int GameServerId { get; set; }

        public virtual User User { get; set; }
        public int UserId { get; set; }

        public bool IsValid()
        {
            return Enable && Dealine < DateTime.Now;
        }
    }
}
