﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Shield : Item
    {
        [DataMember]
        public float Weight { get; set; }

        [DataMember]
        public int Armor { get; set; }
        [DataMember]
        public int Speed { get; set; }
        [DataMember]
        public int Thrust { get; set; }
        [DataMember]
        public DamageType ThrustType { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public int Width { get; set; }

        [DataMember]
        public Slot Slot { get; set; }

        [DataMember]
        public bool HorseBack { get; set; }
    }
}
