﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Armor : Item
    {
        [DataMember]
        public float Weight { get; set; }
        [DataMember]
        public int Head { get; set; }
        [DataMember]
        public int Body { get; set; }
        [DataMember]
        public int Foot { get; set; }

        [DataMember]
        public ArmorType ArmorType { get; set; }
    }
}
