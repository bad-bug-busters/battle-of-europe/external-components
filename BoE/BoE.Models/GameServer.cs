﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class GameServer
    {
        public GameServer()
        {
            CreationDate = DateTime.Now;
            Enabled = true;
        }

        [DataMember]
        public int GameServerId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Address { get; set; }

        public string CharList { get; set; }
        [DataMember]
        public DateTime? LastUpdate { get; set; }

        public DateTime? LastTick { get; set; }

        public virtual Token Token { get; set; }
        public int? TokenId { get; set; }

        [DataMember]
        public bool Enabled { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public int PlayerCount
        {
            get
            {
                if(string.IsNullOrWhiteSpace(CharList) || LastTick.Value < DateTime.Now.AddMinutes(-5))
                    return 0;

                return CharList.Split(',').Count();
            }
        }

        public ICollection<UserAction> UserActions { get; set; }
        public ICollection<CharacterGameRole> CharacterGameRoles { get; set; }
    }
}
