﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Character
    {
        public Character()
        {
            CreationDate = DateTime.Now;
        }

        [DataMember]
        public int CharacterId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Kills { get; set; }

        [DataMember]
        public int Deaths { get; set; }

        [DataMember]
        public int Wins { get; set; }

        [DataMember]
        public int Loses { get; set; }

        public int WinARow { get; set; }

        public int TicksSinceLastUpkeep { get; set; }

        [DataMember]
        public int Experience { get; set; }
        [DataMember]
        public int Gold { get; set; }

        [DataMember]
        public int Generation { get; set; }
        [DataMember]
        public int LoomPoints { get; set; }

        public DateTime? LastTick { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        public virtual Build Build { get; set; }
        [DataMember]
        public int? BuildId { get; set; }

        [DataMember]
        public int? ClanId { get; set; }
        public virtual Clan Clan { get; set; }

        [DataMember]
        public int? ClanRoleId { get; set; }
        public virtual ClanRole ClanRole { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<Inventory> Inventories { get; set; }
        [DataMember]
        public virtual ICollection<ClanApplication> ClanApplications { get; set; }
        [DataMember]
        public virtual ICollection<Build> Builds { get; set; }
        [DataMember]
        public virtual ICollection<Loom> Looms { get; set; }
        public virtual ICollection<Ban> Bans { get; set; }
        public virtual ICollection<CharacterGameRole> CharacterGameRoles { get; set; }

        [DataMember]
        public int Level
        {
            get
            {
                return GetLevel(Experience);
            }
        }
        [DataMember]
        public int NextLevel
        {
            get { return GetXPForLevel(Level + 1); } 
        }
        [DataMember]
        public int? ApplicationClanId
        {
            get
            {
                if (ClanApplications.Count == 0)
                    return null;

                return ClanApplications.First().ClanId;
            }
        }

        public bool IsBanned()
        {
            return Bans.Any(b => b.Expiration == null || DateTime.Now < b.Expiration.Value);
        }

        private static Dictionary<int, int> LevelXps = new Dictionary<int,int>() 
        { 
            {0,	0}, {1,	5000}, {2,	10500}, {3,	17000}, {4,	25000}, {5,	35000}, {6,	47500}, {7,	63000}, {8,	82000}, {9,	105000}, {10, 132500},
            {11, 165000}, {12, 203000}, {13, 247000}, {14, 297500}, {15, 355000}, {16, 420000}, {17, 493000}, {18, 574500}, {19, 665000},
            {20, 765000}, {21, 875000}, {22, 995500}, {23, 1127000}, {24, 1270000}, {25, 1425000}, {26	,1592500}, {27	,1773000},
            {28	,1967000}, {29	,2175000}, {30, 2397500}
        };
        public static int GetLevel(int exp)
        {
            int level = 0;
            while (GetXPForLevel(level) < exp)
            {
                level++;
                if (GetXPForLevel(level) > exp)
                {
                    level--;
                    break;
                }
            }
            return level;
        }
        public static int GetXPForLevel(int targetLevel)
        {
            if(targetLevel > 30)
            {
                int exp = LevelXps[30];
                for (int i = 0; i < targetLevel - 30; i++)
                    exp *= 2;
                return exp;
            }

            return LevelXps[targetLevel];
        }

        public void Retire()
        {
            Generation++;
            LoomPoints++;
            Experience = LevelXps[1];
            Kills = 0;
            Deaths = 0;
        }
    }
}
