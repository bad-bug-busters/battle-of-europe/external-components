﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public enum LoomLevel { 
        Zero=0,
        One=1,
        Two=2,
        Three=3
    }

    public enum UserState
    {
        New = 0,
        Normal = 1
    }

    public enum ItemSort
    {
        Normal=0,
        Special=1
    }

    public enum ItemType
    {
        Unknow = 0,
        Horse = 1,
        OneH = 2,
        TwoH = 3,
        Polearm = 4,
        Arrow = 5,
        Bolt = 6,
        Shield = 7,
        Bow = 8,
        XBow = 9,
        Throwing = 10,
        Goods = 11,
        Head = 12,
        Body = 13,
        Foot = 14,
        Hand = 15,
        Pistol = 16,
        Musket = 17,
        Bullet = 18,
        Animal = 19,
        Book = 20,
        OneHalf = 21
    }

    public enum ItemGender
    {
        Weapon = 0,
        Armor = 1,
        Shield = 2,
        Horse = 3,
        Goods = 4
    }

    public enum LoomType
    {
        LHead = (int)ItemType.Head + (32 * (int)ArmorType.Light),
        MHead = (int)ItemType.Head + (32 * (int)ArmorType.Medium),
        HHead = (int)ItemType.Head + (32 * (int)ArmorType.Heavy),

        LBody = (int)ItemType.Body + (32 * (int)ArmorType.Light),
        MBody = (int)ItemType.Body + (32 * (int)ArmorType.Medium),
        HBody = (int)ItemType.Body + (32 * (int)ArmorType.Heavy),

        LHand = (int)ItemType.Hand + (32 * (int)ArmorType.Light),
        MHand = (int)ItemType.Hand + (32 * (int)ArmorType.Medium),
        HHand = (int)ItemType.Hand + (32 * (int)ArmorType.Heavy),

        LFoot = (int)ItemType.Foot + (32 * (int)ArmorType.Light),
        MFoot = (int)ItemType.Foot + (32 * (int)ArmorType.Medium),
        HFoot = (int)ItemType.Foot + (32 * (int)ArmorType.Heavy),

        Hand=ItemType.Hand,
        OneH = ItemType.OneH,
        OneHalf = ItemType.OneHalf,
        TwoH = ItemType.TwoH,
        Polearm = ItemType.Polearm,
        XBow = ItemType.XBow,
        Bow = ItemType.Bow,
        Throwing = ItemType.Throwing,
        Musket = ItemType.Musket,
        Shield = ItemType.Shield,
        Horse = ItemType.Horse,
    }

    public enum ArmorType
    {
        Light = 0,
        Medium = 1,
        Heavy = 2
    }

    public enum Slot
    {
        CantSheath = 0,
        Back1 = 1,
        Back2 = 2,
        Back3 = 3,
        LeftStomach = 4,
        RightStomach = 5,
        LeftHip1 = 6,
        LeftHip2 = 7,
        LeftHip3 = 8,
        LeftHip4 = 9,
        RightHip = 10,
        RightUpperLeg = 11,
        None = 12
    }

    public enum DamageType
    {
        Cut=0,
        Pierce=1,
        Blunt=2
    }

    public enum CodeType
    {
        Register=0,
        ResetPassword=1
    }

    public enum RoundResult
    {
        Lost=0,
        Won=1,
        Left=2
    }

    public enum CharacterGameRoleType
    {
        Player=0,
        Admin=1
    }

    public static class EnumExtentions
    {
        public static string EnumToString(this CharacterGameRoleType cgrt)
        {
            switch (cgrt)
            {
                case CharacterGameRoleType.Player:
                    return "Player";
                case CharacterGameRoleType.Admin:
                    return "Admin";
            }
            return null;
        }
    }
}
