﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class UserLogin : UserAction
    {
        public string BrowserName { get; set; }

        public string BrowserVersion { get; set; }
    }
}
