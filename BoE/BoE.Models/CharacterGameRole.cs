﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class CharacterGameRole
    {
        public CharacterGameRole()
        {
            CreationDate = DateTime.Now;
        }

        public int CharacterGameRoleId { get; set; }

        public CharacterGameRoleType GameRoleType { get; set; }

        public DateTime CreationDate { get; set; }

        public int CharacterId { get; set; }
        public virtual Character Character { get; set; }

        public int GameServerId { get; set; }
        public virtual GameServer GameServer { get; set; }
    }
}
