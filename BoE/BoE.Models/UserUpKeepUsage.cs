﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class UserUpKeepUsage
    {
        public int UserUpKeepUsageId { get; set; }

        public int Usage { get; set; }

        public int UserUpKeepId { get; set; }
        public virtual UserUpKeep UserUpKeep { get; set; }

        public int ItemId { get; set; }
        public virtual Item Item { get; set; }
    }
}
