﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class ClanApplication
    {
        public ClanApplication()
        {
            CreationDate = DateTime.Now;
        }

        [DataMember]
        public int ClanApplicationId { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        public virtual Clan Clan { get; set; }
        [DataMember]
        public int ClanId { get; set; }

        public virtual Character Character { get; set; }
        [DataMember]
        public int CharacterId { get; set; }
    }
}
