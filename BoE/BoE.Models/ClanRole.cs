﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class ClanRole
    {
        public ClanRole()
        {
            CreationDate = DateTime.Now;
        }

        [DataMember]
        public int ClanRoleId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool Recruit { get; set; }

        [DataMember]
        public bool Lead { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        public virtual Clan Clan { get; set; }
        public int ClanId { get; set; }

        public virtual ICollection<Character> Charaters { get; set; }

        public bool CanKick(int characterId)
        {
            Character character = Clan.Characters.FirstOrDefault(c => c.CharacterId == characterId);

            return Lead && character != null && !character.ClanRole.Lead;
        }
    }
}
