﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Weapon : Item
    {
        [DataMember]
        public float Weight { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public int Speed { get; set; }

        [DataMember]
        public bool HorseBack { get; set; }
        [DataMember]
        public bool BonusShield { get; set; }

        [DataMember]
        public int Thrust { get; set; }
        [DataMember]
        public DamageType ThrustType { get; set; }
        [DataMember]
        public int Swing { get; set; }
        [DataMember]
        public DamageType SwingType { get; set; }
        [DataMember]
        public bool Unbalanced { get; set; }
        [DataMember]
        public bool Knockdown { get; set; }
        [DataMember]
        public bool Crushthrough { get; set; }
        [DataMember]
        public bool Couchable { get; set; }

        [DataMember]
        public int MissileSpeed { get; set; }
        [DataMember]
        public int Accuracy { get; set; }
        [DataMember]
        public int Ammo { get; set; }

        [DataMember]
        public Slot Slot { get; set; }

        public virtual Weapon Secondary { get; set; }
        [DataMember]
        public int? SecondaryId { get; set; }
    }
}
