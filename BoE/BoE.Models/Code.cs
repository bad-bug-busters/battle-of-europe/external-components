﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class Code
    {
        public Code()
        {
            CreationDate = DateTime.Now;
        }

        public int CodeId { get; set; }

        public string Value { get; set; }

        public virtual User User { get; set; }
        public int UserId { get; set; }

        public CodeType Type { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
