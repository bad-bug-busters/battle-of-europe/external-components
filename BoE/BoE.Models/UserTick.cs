﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class UserTick : UserAction
    {
        public int Score { get; set; }

        public int WinARow { get; set; }

        public int Kills { get; set; }

        public int Deaths { get; set; }

        public int Gold { get; set; }

        public int Experience { get; set; }

        public int Tick { get; set; }
    }
}
