﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Clan
    {
        public Clan()
        {
            CreationDate = DateTime.Now;
        }

        [DataMember]
        public int ClanId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        public byte[] Banner { get; set; }

        public int? GameId { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public int? DefaultRoleId { get; set; }
        public virtual ClanRole DefaultRole { get; set; }

        public virtual ICollection<Character> Characters { get; set; }
        public virtual ICollection<ClanApplication> ClanApplications { get; set; }
        public virtual ICollection<ClanRole> ClanRoles { get; set; }

        public int CharactersCount 
        {
            get { return Characters.Count; }
        }
    }
}
