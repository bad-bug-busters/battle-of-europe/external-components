﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class Ban
    {
        public Ban()
        {
            CreationDate = DateTime.Now;
        }

        public int BanId { get; set; }

        public string Reason { get; set; }

        public DateTime? Expiration { get; set; }

        public DateTime CreationDate { get; set; }

        public int CharacterId { get; set; }
        public virtual Character Character { get; set; }
    }
}
