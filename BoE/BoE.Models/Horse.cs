﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    [DataContract]
    public class Horse : Item
    {
        [DataMember]
        public int Armor { get; set; }
        [DataMember]
        public int Speed { get; set; }
        [DataMember]
        public int Maneuver { get; set; }
        [DataMember]
        public int Charge { get; set; }
    }
}
