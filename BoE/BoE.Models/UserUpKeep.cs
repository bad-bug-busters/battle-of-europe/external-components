﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class UserUpKeep : UserAction
    {
        public int Golds { get; set; }

        public RoundResult RoundResult { get; set; }

        public int RespawnCount { get; set; }

        public int TickCount { get; set; }

        public virtual ICollection<UserUpKeepUsage> UserUpKeepUsages { get; set; }
    }
}
