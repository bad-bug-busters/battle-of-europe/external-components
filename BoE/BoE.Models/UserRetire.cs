﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Models
{
    public class UserRetire : UserAction
    {
        public int Experience { get; set; }

        public int Generation { get; set; }
    }
}
