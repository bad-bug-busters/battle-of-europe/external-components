﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class UpKeepDifferencesModel
    {
        public IEnumerable<int> CharactersId { get; set; }

        public IEnumerable<HttpPostedFileBase> Files { get; set; }

        public DateTime? StartDate { get; set; }
    }
}