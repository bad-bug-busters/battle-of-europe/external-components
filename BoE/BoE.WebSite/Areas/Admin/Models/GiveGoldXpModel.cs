﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class GiveGoldXpModel
    {
        [Display(Name="Character")]
        public int CharacterId { get; set; }

        public int? Gold { get; set; }

        public int? Experience { get; set; }
    }
}