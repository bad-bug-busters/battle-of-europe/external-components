﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class CharacterFilterModel
    {
        public CharacterFilterModel()
        {
            Take = 100;
        }

        public int Take { get; set; }

        public string Name { get; set; }

        public int? UniqueId { get; set; }
    }
}