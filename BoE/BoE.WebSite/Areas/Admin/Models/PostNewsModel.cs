﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class PostNewsModel
    {
        public string Title { get; set; }

        public string Content { get; set; }
    }
}