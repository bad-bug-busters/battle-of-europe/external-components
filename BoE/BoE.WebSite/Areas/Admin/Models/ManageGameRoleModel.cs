﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class ManageGameRoleModel
    {
        public int CharacterId { get; set; }

        public CharacterGameRoleType Role { get; set; }

        public IEnumerable<int> GameServersId { get; set; }
    }
}