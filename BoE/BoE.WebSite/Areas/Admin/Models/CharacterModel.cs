﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class CharacterModel
    {
        public int CharacterId { get; set; }

        public string Name { get; set; }

        public int Generation { get; set; }

        public int Gold { get; set; }

        public int Experience { get; set; }

        public DateTime? LastSeen { get; set; }

        public DateTime CreationDate { get; set; }

        public int Level
        {
            get { return Character.GetLevel(Experience); }
        }
    }
}