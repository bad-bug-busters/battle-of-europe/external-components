﻿using BoE.Contexts;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class BanCharacterModel
    {
        public BanCharacterModel() 
        {
            Days = 1;
        }

        public int CharacterId { get; set; }

        public string Reason { get; set; }

        public int Days { get; set; }

        public Ban ToBan(BoEContext db)
        {
            Ban ban = db.Bans.Create();
            ban.CharacterId = CharacterId;
            ban.Reason = Reason;
            if(Days != 0)
                ban.Expiration = DateTime.Now.AddDays(Days);

            return ban;
        }
    }
}