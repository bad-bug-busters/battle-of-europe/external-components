﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Areas.Admin.Models
{
    public class UpKeepDifferencesReportModel
    {
        public UpKeepDifferencesReportModel()
        {
            ItemUsages = new List<ItemUsageModel>();
        }

        public string CharacterName { get; set; }

        public int UniqueId { get; set; }

        public List<ItemUsageModel> ItemUsages { get; set; }
    }

    public class ItemUsageModel
    {
        public ItemUsageModel()
        {
            NumberOfUse = 1;
        }

        public int GameId { get; set; }

        public string ItemName { get; set; }

        public int NumberOfUse { get; set; }

        public int TotalUsage { get; set; }
    }
}