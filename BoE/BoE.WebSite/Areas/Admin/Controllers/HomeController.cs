﻿using BoE.Business;
using BoE.Contexts;
using BoE.Lib;
using BoE.Models;
using BoE.WebSite.Areas.Admin.Filters;
using BoE.WebSite.Areas.Admin.Models;
using BoE.WebSite.Business;
using BoE.WebSite.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using io = System.IO;

namespace BoE.WebSite.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (!BoESecurity.IsAuthenticated)
                return RedirectToAction("Login");

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                if (BoESecurity.Login(model.Username, model.Password,Request.UserHostAddress, this.GetBrowserInfo(), db))
                {
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            BoESecurity.Logout();
            return RedirectToAction("Index");
        }


    }
}
