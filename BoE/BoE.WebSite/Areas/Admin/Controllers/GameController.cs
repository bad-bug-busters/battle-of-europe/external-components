﻿using BoE.Business;
using BoE.Contexts;
using BoE.Models;
using BoE.WebSite.Areas.Admin.Filters;
using BoE.WebSite.Areas.Admin.Models;
using BoE.WebSite.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoE.WebSite.Areas.Admin.Controllers
{
    [RequireIpMvc]
    [RequireRoleMvc(Role = RolesFactory.Admin)]
    public class GameController : Controller
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PostNews()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PostNews(PostNewsModel model)
        {
            if (ModelState.IsValid)
            {
                db.Newses.Add(new News()
                {
                    Title = model.Title,
                    Content = model.Content,
                    AuthorId = BoESecurity.CurrentUserId
                });
                db.SaveChanges();
                TempData["alert"] = "News created !";
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        [RequireRoleMvc(Role = RolesFactory.SuperAdmin)]
        public ActionResult ResetBuilds()
        {
            return View();
        }

        [HttpPost]
        [RequireRoleMvc(Role = RolesFactory.SuperAdmin)]
        public ActionResult ResetBuilds(bool a = false)
        {
            foreach (Build build in db.Builds)
                BuildB.ResetBuild(build);
            db.SaveChanges();
            TempData["alert"] = "All builds have been reset.";
            return RedirectToAction("Index");
        }
    }
}
