﻿using BoE.Business;
using BoE.Contexts;
using BoE.Lib;
using BoE.Models;
using BoE.WebSite.Areas.Admin.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using io = System.IO;

namespace BoE.WebSite.Areas.Admin.Controllers
{
    [RequireIpMvc]
    [RequireRoleMvc(Role = RolesFactory.Admin)]
    public class ItemController : Controller
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [RequireRoleMvc(Role = RolesFactory.SuperAdmin)]
        public ActionResult UpdateItems()
        {
            return View();
        }

        [HttpPost]
        [RequireRoleMvc(Role = RolesFactory.SuperAdmin)]
        public ActionResult UpdateItems(HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength == 0)
                return View();

            io.MemoryStream target = new io.MemoryStream();
            file.InputStream.CopyTo(target);
            byte[] fileByte = target.ToArray();

            int updated = 0;
            int added = 0;
            int removed = 0;

            string start0 = "regular_items_begin";
            string end0 = "itm_mod_1_items_begin";

            string start1 = "itm_mod_1_items_begin";
            string end1 = "itm_mod_2_items_begin";

            string start2 = "itm_mod_2_items_begin";
            string end2 = "itm_mod_3_items_begin";

            string start3 = "itm_mod_3_items_begin";
            string end3 = "itm_special_items_begin";

            string startS = "itm_special_items_begin";
            string endS = "itm_admin_items_begin";

            List<Item> items0 = ItemB.GetItemsFromKind(fileByte, LoomLevel.Zero, ItemSort.Normal, start0, end0);
            List<Item> items1 = ItemB.GetItemsFromKind(fileByte, LoomLevel.One, ItemSort.Normal, start1, end1);
            List<Item> items2 = ItemB.GetItemsFromKind(fileByte, LoomLevel.Two, ItemSort.Normal, start2, end2);
            List<Item> items3 = ItemB.GetItemsFromKind(fileByte, LoomLevel.Three, ItemSort.Normal, start3, end3);
            List<Item> itemsS = ItemB.GetItemsFromKind(fileByte, LoomLevel.Zero, ItemSort.Special, startS, endS);

            List<Item> allItems = itemsS.Concat(items0).Concat(items1).Concat(items2).Concat(items3).GroupBy(i => i.NameId).Select(i => i.First()).ToList();

            Dictionary<string, Item> itemsDbByNameId = db.Items.Where(i => i.FromKind).ToDictionary(i => i.NameId);

            List<List<string[]>> changesList = ItemB.AddOrUpdateItemsFromKind(allItems, itemsDbByNameId, db, out added, out updated);
            ItemB.RemoveInventoryByKinds(itemsDbByNameId, db);

            db.SaveChanges();

            removed = ItemB.RemoveItemsByKinds(itemsDbByNameId, db);

            db.SaveChanges();

            Dictionary<int, Item> itemsDbByGameId = db.Items.Where(i => i.FromKind).ToDictionary(i => i.GameId);
            ItemB.SetLoomsRelations(items0, items1, items2, items3, itemsDbByGameId);

            db.SaveChanges();

            io.File.WriteAllText(Server.MapPath("~/App_Data/itemsHash.txt"), "" + fileByte.GetHashCode());

            io.File.WriteAllText(
                Server.MapPath(string.Format("~/App_Data/{0} - items changelog.txt", DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")))
                , ItemB.GetChangeLogFromChangeList(changesList));

            ViewBag.GoBak = "Index";
            return View("_Report", new string[] 
            { 
                "Success", 
                string.Format("Items added: {0}", added), 
                string.Format("Items updated: {0}", updated), 
                string.Format("Items removed: {0}", removed),
                "The changelog has been created, ask the web dev, he may know how to get it."
            });
        }

        [HttpGet]
        public ActionResult UpdateImages()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdateImages(IEnumerable<HttpPostedFileBase> files)
        {
            if (files == null || files.Count() == 0)
                return View();
            string imagesPath = Server.MapPath("/Content/images/items");
            foreach (HttpPostedFileBase file in files)
            {
                int id = int.Parse(io.Path.GetFileNameWithoutExtension(file.FileName));
                Item item = db.Items.FirstOrDefault(i => i.GameId == id);
                if (item == null)
                    continue;
                io.MemoryStream target = new io.MemoryStream();
                file.InputStream.CopyTo(target);
                byte[] imageByte = ImageUtility.GetImageContent(target.ToArray(), 600, 600, false);
                byte[] thumbByte = ImageUtility.GetImageContent(target.ToArray(), 100, 100, false);
                string name = "" + item.ItemId;

                if (id == 150)
                {
                    bool a = true;
                }

                System.IO.File.WriteAllBytes(imagesPath + "\\" + name + ".jpg", imageByte);
                System.IO.File.WriteAllBytes(imagesPath + "\\" + name + "_thumb.jpg", thumbByte);
            }

            TempData["alert"] = "Sucess";
            return RedirectToAction("Index");
        }
    }
}
