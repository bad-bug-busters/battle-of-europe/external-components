﻿using BoE.Contexts;
using BoE.Models;
using BoE.WebSite.Areas.Admin.Filters;
using BoE.WebSite.Areas.Admin.Models;
using BoE.WebSite.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoE.WebSite.Areas.Admin.Controllers
{
    [RequireIpMvc]
    [RequireRoleMvc(Role = RolesFactory.Moderator)]
    public class UserController : Controller
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Characters(CharacterFilterModel model)
        {
            ViewBag.Characters = FilterB.FilterCharacters(db, model);
            return View(model);
        }

        [HttpGet]
        public ActionResult CharacterDetails(int characterId)
        {
            Character character = db.Characters.Find(characterId);
            return View(character);
        }

        private void GiveGoldXpViewBag(int characterId)
        {
            ViewBag.CharacterName = db.Characters.Select(c => new
            { 
                Characterid=c.CharacterId, 
                Name=c.Name 
            }).First(c => c.Characterid == characterId).Name;
        }
        [HttpGet]
        public ActionResult GiveGoldXp(int characterId)
        {
            GiveGoldXpViewBag(characterId);
            return View(new GiveGoldXpModel() { CharacterId = characterId });
        }
        [HttpPost]
        public ActionResult GiveGoldXp(GiveGoldXpModel model)
        {
            if (ModelState.IsValid)
            {
                Character character = db.Characters.Find(model.CharacterId);
                character.Gold += model.Gold.HasValue ? model.Gold.Value : 0;
                character.Experience += model.Experience.HasValue ? model.Experience.Value : 0;
                db.SaveChanges();
                AdminLogUtils.AddLogs(new string[] { 
                    string.Format("Administrator {0} added {1} experience and {2} gold to player {3}", 
                    BoESecurity.CurrentUserId,
                    model.Experience, model.Gold, model.CharacterId)
                });
                TempData["alert"] = "Success";
                return RedirectToAction("Characters");
            }
            GiveGoldXpViewBag(model.CharacterId);
            return View(model);
        }

        private void ManageGameRoleViewBag(int characterId)
        {
            ViewBag.CharacterName = db.Characters.Select(c => new
            {
                Characterid = c.CharacterId,
                Name = c.Name
            }).First(c => c.Characterid == characterId).Name;
            ViewBag.GameServers = db.GameServers.Select(gs => new
            {
                GameServerId = gs.GameServerId,
                Name = gs.Name
            }).AsEnumerable().Select(gs => new
            {
                GameServerId = gs.GameServerId,
                Name=string.Format("{0} - {1}", gs.Name, 
                    db.CharacterGameRoles.Any(cgr => cgr.GameServerId==gs.GameServerId && 
                        cgr.CharacterId==characterId &&
                        cgr.GameRoleType == CharacterGameRoleType.Admin) ?
                            CharacterGameRoleType.Admin.EnumToString() :
                            CharacterGameRoleType.Player.EnumToString())
            });
        }
        [HttpGet]
        public ActionResult ManageGameRole(int characterId)
        {
            ManageGameRoleViewBag(characterId);
            return View(new ManageGameRoleModel() { CharacterId=characterId });
        }
        [HttpPost]
        public ActionResult ManageGameRole(ManageGameRoleModel model)
        {
            if (ModelState.IsValid)
            {
                Character character = db.Characters.Find(model.CharacterId);
                if (character == null)
                {
                    TempData["alert"] = "Character not found";
                    ManageGameRoleViewBag(model.CharacterId);
                    return View(model);
                }

                foreach (int gameServerId in model.GameServersId)
                {
                    CharacterGameRole cGameRole = db.CharacterGameRoles
                        .FirstOrDefault(cgr => cgr.GameServerId == gameServerId && cgr.CharacterId == model.CharacterId);
                    if (cGameRole == null)
                    {
                        db.CharacterGameRoles.Add(new CharacterGameRole()
                        {
                            CharacterId = model.CharacterId,
                            GameServerId = gameServerId,
                            GameRoleType = model.Role
                        });
                    }
                    else
                        cGameRole.GameRoleType = model.Role;
                }

                db.SaveChanges();
                AdminLogUtils.AddLogs(new string[] { 
                    string.Format("Administrator {0} gave player {1} the role {2} on servers: {3}", 
                    BoESecurity.CurrentUserId,
                    model.CharacterId, 
                    model.Role.EnumToString(), 
                    model.GameServersId.Select(g => g.ToString()).Aggregate((a, b) => a + " - " + b))
                });

                TempData["alert"] = string.Format(
                    "Success ! you have updated {0} ingame role to {1}.",
                    character.Name,
                    model.Role.EnumToString());
                return RedirectToAction("Characters");
            }

            return View(model);
        }

        private void BanCharacterViewBag(int characterId)
        {
            ViewBag.CharacterName = db.Characters.Select(c => new
            {
                Characterid = c.CharacterId,
                Name = c.Name
            }).First(c => c.Characterid == characterId).Name;
        }
        [HttpGet]
        public ActionResult BanCharacter(int characterId)
        {
            BanCharacterViewBag(characterId);
            return View(new BanCharacterModel() { CharacterId=characterId });
        }
        [HttpPost]
        public ActionResult BanCharacter(BanCharacterModel model)
        {
            if (ModelState.IsValid)
            {
                Ban ban = model.ToBan(db);
                db.Bans.Add(ban);
                db.SaveChanges();
                AdminLogUtils.AddLogs(new string[] { 
                    string.Format("Administrator {0} banned character {1} for {2} days (BanId: {3})",
                    BoESecurity.CurrentUserId, model.CharacterId, model.Days, ban.BanId)
                });
                TempData["alert"] = "Success !";
                return RedirectToAction("Characters");
            }

            BanCharacterViewBag(model.CharacterId);
            return View(model);
        }

        private void GrantHosterViewBag()
        {
            ViewBag.Users = db.Users.Select(u => new
            {
                Roles = u.Roles,
                Username = u.Username,
                UserId = u.UserId
            }).Where(u => u.Roles.Where(r => r.Name == RolesFactory.Hoster).Count() == 0)
                .OrderBy(u => u.Username);
        }
        [HttpGet]
        public ActionResult GrantHoster()
        {
            GrantHosterViewBag();
            return View();
        }
        [HttpPost]
        public ActionResult GrantHoster(int userId)
        {
            User user = db.Users.Find(userId);
            if (user == null)
            {
                TempData["alert"] = "User not found";
                GrantHosterViewBag();
                return View(userId);
            }
            user.Roles.Add(db.Roles.First(r => r.Name == RolesFactory.Hoster));
            db.SaveChanges();
            TempData["alert"] = "Success " + user.Username + " is now a hoster";
            return RedirectToAction("Index");
        }

        private void GrantAdminViewBag()
        {
            ViewBag.Users = db.Users.Select(u => new
            {
                Roles = u.Roles,
                Username = u.Username,
                UserId = u.UserId
            }).Where(u => u.Roles.Where(r => r.Name == RolesFactory.Admin).Count() == 0)
                .OrderBy(u => u.Username);
        }
        [HttpGet]
        public ActionResult GrantAdmin()
        {
            GrantAdminViewBag();
            return View();
        }
        [HttpPost]
        [RequireRoleMvc(Role = RolesFactory.SuperAdmin)]
        public ActionResult GrantAdmin(int userId)
        {
            User user = db.Users.Find(userId);
            if (user == null)
            {
                TempData["alert"] = "User not found";
                GrantAdminViewBag();
                return View(userId);
            }
            user.Roles.Add(db.Roles.First(r => r.Name == RolesFactory.Admin));
            db.SaveChanges();
            TempData["alert"] = "Success " + user.Username + " is now an administrator";
            return RedirectToAction("Index");
        }
    }
}
