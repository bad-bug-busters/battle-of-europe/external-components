﻿using BoE.Contexts;
using BoE.Models;
using BoE.WebSite.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoE.WebSite.Areas.Admin.Controllers
{
    public class StatsController : Controller
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        private void UpKeepDifferencesViewBag()
        {
            ViewBag.Characters = db.Characters.Select(c => new
            {
                Name = c.Name,
                CharacterId = c.CharacterId
            }).OrderBy(c => c.Name);
        }
        [HttpGet]
        public ActionResult UpKeepDifferences()
        {
            UpKeepDifferencesViewBag();
            return View();
        }

        [HttpPost]
        public ActionResult UpKeepDifferences(UpKeepDifferencesModel model)
        {
            if (ModelState.IsValid)
            {
                List<int> uniqueIds = db.Users.Select(u => new
                {
                    UserId=u.UserId,
                    UniqueId=u.UniqueId
                }).Where(u => model.CharactersId.Contains(u.UserId))
                .Select(c => c.UniqueId.Value).ToList();
                Dictionary<int, List<KeyValuePair<int, int>>> upKeepsRecords = new Dictionary<int,List<KeyValuePair<int,int>>>();
                foreach (int uniqueId in uniqueIds)
                    upKeepsRecords.Add(uniqueId, new List<KeyValuePair<int, int>>());

                foreach(HttpPostedFileBase file in model.Files)
                {
                    StreamReader reader = new StreamReader(file.InputStream);
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains("Upkeep?usages="))
                        {
                            foreach (var upKeepsRecord in upKeepsRecords)
                            {
                                List<KeyValuePair<int, int>> usageRecords = GetUpKeepRecordFromLine(upKeepsRecord.Key, line);
                                if (usageRecords != null)
                                    upKeepsRecord.Value.AddRange(usageRecords);
                            }
                        }
                    }
                }
                List<UpKeepDifferencesReportModel> reports = new List<UpKeepDifferencesReportModel>();
                foreach (var upKeepsRecord in upKeepsRecords)
                {
                    int userId = db.Users.First(u => u.UniqueId == upKeepsRecord.Key).UserId;
                    UpKeepDifferencesReportModel report = new UpKeepDifferencesReportModel() 
                    { 
                        CharacterName=db.Characters.Select(c => new 
                        { 
                            Name=c.Name, 
                            CharacterId=c.CharacterId 
                        }).First(c => c.CharacterId == userId).Name,
                        UniqueId = upKeepsRecord.Key
                    };
                    foreach (var itemRecord in upKeepsRecord.Value)
                    {
                        int gameId = itemRecord.Key;
                        int usage = itemRecord.Value;
                        ItemUsageModel itemUsage = report.ItemUsages.FirstOrDefault(ig => ig.GameId == gameId);
                        if (itemUsage == null)
                        {
                            var items = db.Items.Select(it => new
                                {
                                    Name = it.Name,
                                    GameId = it.GameId
                                }).Where(it => it.GameId == gameId || it.GameId == gameId + 1 || it.GameId == gameId - 1)
                                .Select(it => it.Name).AsEnumerable();
                            itemUsage = new ItemUsageModel()
                            {
                                GameId = gameId,
                                TotalUsage = usage,
                                ItemName = items.Count() != 0 ? items.Aggregate((a, b) => a + " or " + b) : "Unknown"
                            };
                            report.ItemUsages.Add(itemUsage);
                        }
                        else
                        {
                            itemUsage.NumberOfUse++;
                            itemUsage.TotalUsage += usage;
                        }
                    }
                    reports.Add(report);
                }
                return View("UpKeepDifferencesReport", reports);
            }
            UpKeepDifferencesViewBag();
            return View(model);
        }
        private List<KeyValuePair<int, int>> GetUpKeepRecordFromLine(int uniqueId, string line)
        {
            string records = line.Substring(line.IndexOf("usages=") + "usages=".Length);
            records = records.Substring(0, records.IndexOf("&token"));
            IEnumerable<string> usages = records.Split(new string[] { "]," }, StringSplitOptions.None).Select(r => r.Replace("[", "").Replace("]",""));
            foreach (string usage in usages)
            {
                List<KeyValuePair<int, int>> usageRecords = null;
                IEnumerable<int> itemUsages = usage.Split(',').Select(iu => int.Parse(iu));
                if (itemUsages.ElementAt(0) == uniqueId)
                {
                    if (usageRecords == null)
                        usageRecords = new List<KeyValuePair<int, int>>();
                    for (int i = 3; i < itemUsages.Count(); i+=2)
                        usageRecords.Add(new KeyValuePair<int, int>(itemUsages.ElementAt(i), itemUsages.ElementAt(i + 1)));
                }
                return usageRecords;
            }

            return null;
        }
    }
}
