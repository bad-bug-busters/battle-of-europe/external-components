﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BoE.WebSite.Areas.Admin.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class RequireIpMvcAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                string[] ips = File.ReadAllLines(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/IpWhiteList.txt"));
                if (ips.Contains(filterContext.HttpContext.Request.UserHostAddress))
                {
                    return;
                }
            }
            catch(Exception e)
            {
                
            }

            RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
            redirectTargetDictionary.Add("action", "Login");
            redirectTargetDictionary.Add("controller", "Home");

            filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
        }
    }
}