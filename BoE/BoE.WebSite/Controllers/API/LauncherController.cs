﻿using BoE.Contexts;
using BoE.Models;
using BoE.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoE.Business;
using WebMatrix.WebData;
using System.Web.Security;
using BoE.WebSite.Business;
using System.Web.Configuration;

namespace BoE.WebService.Controllers
{
    [AllowAnonymous]
    public class LauncherController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        public HttpResponseMessage GetToken(string username, string password, string server)
        {
            User user = db.Users.FirstOrDefault(u => u.Username == username);
            if (!Membership.ValidateUser(username, password))
                return Request.CreateResponse(HttpStatusCode.OK, "Username or password incorrect.");

            if (!Roles.IsUserInRole(username, RolesFactory.Hoster))
                return Request.CreateResponse(HttpStatusCode.OK, "You must be a hoster to do this.");

            string address = this.GetClientIp();
            GameServer gameServ = db.GameServers.FirstOrDefault(gs => gs.Name == server);
            if (gameServ == null)
                gameServ = GameB.CreateGameServer(address, server, db);

            if (gameServ.TokenId.HasValue)
                gameServ.Token.Enable = false;

            Token token = GameB.CreateToken(gameServ, int.Parse(WebConfigurationManager.AppSettings["TokenTTL"]), user.UserId, db);
            return Request.CreateResponse(HttpStatusCode.OK, token.Value);
        }
    }
}
