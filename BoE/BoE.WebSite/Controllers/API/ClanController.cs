﻿using BoE.Business;
using BoE.Contexts;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using BoE.WebSite.Business;
using BoE.WebSite.Models;

namespace BoE.WebSite.Controllers
{
    public class ClanController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpPost]
        public HttpResponseMessage CreateClan(CreateClanModel model)
        {
            if (ModelState.IsValid)
            {
                Character character = db.Characters.Find(BoESecurity.CurrentUserId);

                if(character.ClanId.HasValue)
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "You already have a clan.");

                if(db.Clans.Any(c => c.Name == model.Name))
                    return Request.CreateResponse(HttpStatusCode.Conflict, "A clan with this name already exist.");

                Clan clan = model.ToClan(db);
                db.Clans.Add(clan);
                db.SaveChanges();

                clan.DefaultRoleId = clan.ClanRoles.First(cr => cr.Name == "Member").ClanRoleId;
                character.ClanId = clan.ClanId;
                character.ClanRoleId = clan.ClanRoles.First(cr => cr.Name == "Leader").ClanRoleId;
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, new 
                {
                    Clan= new ClanModel(clan),
                    ClanRoles = clan.ClanRoles
                });
            }

            return Request.CreateResponse(HttpStatusCode.Conflict, "The model is not valid.");
        }

        [HttpGet]
        public HttpResponseMessage GetClan(int clanId)
        {
            Clan clan = db.Clans.Find(clanId);
            if (clan == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            return Request.CreateResponse(HttpStatusCode.OK, new ClanModel(clan));
        }

        [HttpPost]
        public HttpResponseMessage KickMember([FromBody]int characterId)
        {
            Character current = db.Characters.Find(BoESecurity.CurrentUserId);
            Character character = db.Characters.Find(characterId);

            if(!current.ClanRoleId.HasValue || !current.ClanRole.CanKick(characterId))
                return Request.CreateResponse(HttpStatusCode.Forbidden);

            character.ClanRoleId = null;
            character.ClanId = null;

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage AcceptApplication([FromBody]int applicationId)
        {
            ClanApplication application = db.ClanApplications.Find(applicationId);
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);

            if(application == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            if (!character.ClanRoleId.HasValue || !character.ClanRole.Lead || character.ClanRole.ClanId != application.ClanId)
                return Request.CreateResponse(HttpStatusCode.Forbidden);

            Character applican = application.Character;
            applican.ClanId = application.ClanId;
            applican.ClanRoleId = application.Clan.DefaultRoleId;
            db.ClanApplications.Remove(application);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, new MemberModel(applican));
        }

        [HttpPost]
        public HttpResponseMessage RejectApplication([FromBody]int applicationId)
        {
            ClanApplication application = db.ClanApplications.Find(applicationId);
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);

            if (application == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            if (!character.ClanRoleId.HasValue || !character.ClanRole.Lead || character.ClanRole.ClanId != application.ClanId)
                return Request.CreateResponse(HttpStatusCode.Forbidden);

            db.ClanApplications.Remove(application);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage EditDescription([FromBody]string description)
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);

            if (!character.ClanRoleId.HasValue || !character.ClanRole.Lead)
                return Request.CreateResponse(HttpStatusCode.Forbidden);

            character.Clan.Description = description;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage Apply([FromBody]int clanId)
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);

            if (character.ClanId.HasValue || character.ApplicationClanId.HasValue)
                return Request.CreateResponse(HttpStatusCode.Forbidden);

            ClanApplication clanApplication = db.ClanApplications.Create();
            clanApplication.CharacterId = character.CharacterId;
            clanApplication.ClanId = clanId;
            db.ClanApplications.Add(clanApplication);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage Leave()
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            Clan clan = character.Clan;

            if(!clan.Characters.Where(ch => ch.CharacterId != character.CharacterId).Any(ch => ch.ClanRole.Lead))
                return Request.CreateResponse(HttpStatusCode.Conflict);

            character.ClanId = null;
            character.ClanRoleId = null;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage Disband()
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            Clan clan = character.Clan;

            if (!character.ClanRoleId.HasValue || !character.ClanRole.Lead)
                return Request.CreateResponse(HttpStatusCode.Forbidden);

            clan.DefaultRoleId = null;
            foreach (Character member in clan.Characters)
            {
                member.ClanId = null;
                member.ClanRoleId = null;
            }
            foreach (ClanRole clanRole in clan.ClanRoles.ToList())
                db.ClanRoles.Remove(clanRole);
            foreach (ClanApplication clanApplication in clan.ClanApplications.ToList())
                db.ClanApplications.Remove(clanApplication);

            db.SaveChanges();
            db.Clans.Remove(clan);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage CancelApplication()
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            foreach (ClanApplication application in character.ClanApplications.ToList())
                db.ClanApplications.Remove(application);
            
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }
}
