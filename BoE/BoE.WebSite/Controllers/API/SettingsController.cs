﻿using BoE.Contexts;
using BoE.WebSite.Business;
using BoE.WebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BoE.WebSite.Controllers.API
{
    public class SettingsController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpPost]
        public HttpResponseMessage ChangeCharName([FromBody]string newName)
        {
            if(db.Characters.FirstOrDefault(c => c.Name == newName) != null)
                return Request.CreateResponse(HttpStatusCode.Conflict);

            db.Characters.Find(BoESecurity.CurrentUserId).Name = newName;
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage ChangePassword(ChangePasswordModel model)
        {
            if (BoESecurity.ChangePassword(BoESecurity.CurrentUserName, model.CurrentPassword, model.NewPassword))
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.Conflict);
        }
    }
}
