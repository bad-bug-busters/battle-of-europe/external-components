﻿using BoE.Business;
using BoE.Contexts;
using BoE.Lib;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoE.WebSite.Business;

namespace BoE.WebSite.Controllers.API
{
    public class InvController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpPost]
        public HttpResponseMessage Repair([FromBody]int id)
        {
            Inventory inv = db.Inventories.Find(id);
            if(inv == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            inv.Character.Gold -= inv.RepairPrice.Value;
            inv.RepairPrice = null;
            if(inv.Character.Gold < 0)
                return Request.CreateResponse(HttpStatusCode.Conflict);

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.Accepted, inv.Character.Gold);
        }
    }
}
