﻿using BoE.Lib;
using BoE.Models;
using BoE.Contexts;
using BoE.Business;
using BoE.WebSite.Filters;
using BoE.Formatters;
using BoE.WebSite.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Configuration;
using BoE.WebSite.Models;


namespace BoE.WebSite.Controllers
{
    [AllowAnonymous]
    [WSEModelError]
    [WSEException]
    public class GameController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        [RequireToken]
        public HttpResponseMessage GetChar(int reqId, int id, string name, string token, int withStats = 0)
        {
            User user = db.Users.FirstOrDefault(u => u.UniqueId == id);

            if (user == null)
            {
                if (db.Characters.Any(c => c.Name == name))
                    return this.CreateWSEResponse(HttpStatusCode.OK, reqId, new 
                    { 
                        Status = (int)GetCharStatus.NameTaken, 
                        Id = id 
                    });
                else
                    user = UserB.CreateUser(id, name, db);
            }
            else if (user.Character.Name != name)
            {
                return this.CreateWSEResponse(HttpStatusCode.OK, reqId, new 
                { 
                    Status = (int)GetCharStatus.WrongName, 
                    Id = id, 
                    Name = user.Character.Name 
                });
            }
            if (user.State == UserState.New)
            {
                var code = user.Codes.Select(c => new { Type=c.Type, Value=c.Value }).FirstOrDefault(c => c.Type == CodeType.Register);
                if (withStats == 1)
                {
                    return this.CreateWSEResponse(HttpStatusCode.OK, reqId, new
                    {
                        Status = (int)GetCharStatus.NewUser,
                        Code = code.Value,
                        UniqueId = id,
                        Character = user.Character
                    });
                }
                else
                {
                    return this.CreateWSEResponse(HttpStatusCode.OK, reqId, new
                    {
                        Status = (int)GetCharStatus.NewUser,
                        Code = code.Value,
                        UniqueId = id
                    });
                }
            }

            GameServer gameServer = db.Tokens.First(t => t.Value == token).GameServer;
            bool isAdmin = db.CharacterGameRoles.Select(cgr => new 
            {
                CharacterId=cgr.CharacterId,
                GameServerId=cgr.GameServerId,
                GameRoleType = cgr.GameRoleType
            }).Any(cgr => cgr.CharacterId==user.UserId && cgr.GameServerId == gameServer.GameServerId && cgr.GameRoleType == CharacterGameRoleType.Admin);

            int status = isAdmin ? (int)GetCharStatus.Admin : (int)GetCharStatus.Ok;
            status = user.Character.IsBanned() ? (int)GetCharStatus.Banned : status;

            if (withStats == 1)
            {
                return this.CreateWSEResponse(HttpStatusCode.OK, reqId, new
                {
                    Status = status,
                    UniqueId = id,
                    Character = user.Character
                });
            }
            else
            {
                return this.CreateWSEResponse(HttpStatusCode.OK, reqId, new
                {
                    Status = status,
                    UniqueId = id
                });
            }
        }

        [HttpGet]
        [RequireToken]
        public HttpResponseMessage DoTick(int reqId, string ids, string token)
        {
            GameServer gameServer = db.Tokens.First(t => t.Value == token).GameServer;
            int tickCD = int.Parse(WebConfigurationManager.AppSettings["TickCD"]);

            if (gameServer.LastTick.HasValue && DateTime.Now < gameServer.LastTick.Value.AddSeconds(tickCD))
                return this.CreateWSEResponse(HttpStatusCode.Conflict, reqId);

            int gold = int.Parse(WebConfigurationManager.AppSettings["GoldTick"]);
            int exp = int.Parse(WebConfigurationManager.AppSettings["ExpTick"]);

            IEnumerable<UserScore> userScores = GetUserScoresFromString(ids, db);
            foreach (UserScore userScore in userScores)
            {
                if (userScore.Tick == 0)
                    continue;
                //Hotfix check for weird kill/death values
                if (GameB.KDIsBullShit(userScore.Kills, userScore.Deaths, userScore.Score))
                {
                    userScore.Kills = 0;
                    userScore.Deaths = 0;
                }

                if (userScore.User.Character.LastTick.HasValue && DateTime.Now < userScore.User.Character.LastTick.Value.AddSeconds(tickCD))
                    continue;

                Character character = userScore.User.Character;
                int goldWon = GameB.GetTickGold(gold, userScore.Score, character.Generation, character.WinARow);
                int ExperienceWon = GameB.GetTickExp(exp, userScore.Score, character.Generation, character.WinARow);

                character.Experience += ExperienceWon;
                character.Gold += goldWon;
                character.Kills += userScore.Kills;
                character.Deaths += userScore.Deaths;
                character.LastTick = DateTime.Now;
                character.TicksSinceLastUpkeep++;

                db.UserActions.Add(new UserTick()
                {
                    UserId = userScore.User.UserId,
                    Score = userScore.Score,
                    WinARow = character.WinARow,
                    Kills = userScore.Kills,
                    Deaths = userScore.Deaths,
                    AddressIp = this.GetClientIp(),
                    GameServerId = gameServer.GameServerId,
                    Gold = goldWon,
                    Experience = ExperienceWon,
                    Tick = userScore.Tick
                });
            }
            gameServer.CharList = ids.Replace(" ", "").Split(new string[] { "],[" }, StringSplitOptions.None)
                .Select(s => s.Replace("[", "").Replace("]", "").Split(',').First()).Aggregate((a,b) => a + "," + b);
            gameServer.LastTick = DateTime.Now;
            db.SaveChanges();

            return this.CreateWSEResponse(HttpStatusCode.OK, reqId);
        }

        [HttpGet]
        [RequireToken]
        public HttpResponseMessage Upkeep(int reqId, string usages, string token)
        {
            GameServer gameServer = db.Tokens.First(t => t.Value == token).GameServer;
            IEnumerable<UserUsagesModel> userUsages = GetUserUpKeepFromString(usages, db);
            foreach (UserUsagesModel userUsage in userUsages)
            {
                Character character = userUsage.User.Character;
                int tickCount = character.TicksSinceLastUpkeep;
                List<int> gameIds = userUsage.Usages.Select(u => u.Key).ToList();
                IEnumerable<int> charItems = character.Inventories.Select(i => i.ItemId);
                var items = db.Items.Select(i => new UpKeepCalculationModel()
                {  
                    ItemId=i.ItemId,
                    HitPoint= i.HitPoint,
                    Type=i.Type,
                    GameId=i.GameId,
                    ItemValue=i.Value
                }).Where(i => gameIds.Contains(i.GameId));

                int total = 0;
                List<UserUpKeepUsage> userUpUsages = new List<UserUpKeepUsage>();
                foreach (KeyValuePair<int, int> usgs in userUsage.Usages)
                {
                    UpKeepCalculationModel itemUsed = items.FirstOrDefault(i => i.GameId == usgs.Key);
                    if (!charItems.Contains(itemUsed.ItemId))
                        continue; //Sebastian the noob

                    if (itemUsed != null)
                    {
                        if (itemUsed.Type == ItemType.Arrow || itemUsed.Type == ItemType.Bullet || itemUsed.Type == ItemType.Throwing || itemUsed.Type == ItemType.Bolt)
                            itemUsed.Ammo = db.Items.OfType<Weapon>().Select(w => new { ItemId = w.ItemId, Ammo = w.Ammo }).First(w => w.ItemId == itemUsed.ItemId).Ammo;

                        int upkeep = GameB.GetUpkeepValue(itemUsed.HitPoint, itemUsed.Ammo,
                            itemUsed.ItemValue, usgs.Value, itemUsed.Type, userUsage.RespawnCount, tickCount);
                        if (userUsage.User.Character.Gold > upkeep)
                        {
                            userUsage.User.Character.Gold -= upkeep;
                            total += upkeep;
                        }
                        else
                            character.Inventories.First(iv => iv.RepairPrice == null && iv.ItemId == itemUsed.ItemId).RepairPrice = upkeep;
                        userUpUsages.Add(new UserUpKeepUsage() { 
                            ItemId=itemUsed.ItemId,
                            Usage = usgs.Value
                        });
                    }
                }
                db.UserActions.Add(new UserUpKeep() { 
                    AddressIp = this.GetClientIp(),
                    GameServerId = gameServer.GameServerId,
                    Golds = total,
                    UserId=userUsage.User.UserId,
                    UserUpKeepUsages = userUpUsages,
                    RoundResult = userUsage.RoundResult,
                    RespawnCount = userUsage.RespawnCount,
                    TickCount = tickCount
                });

                character.TicksSinceLastUpkeep = 0;
                if (userUsage.RoundResult == RoundResult.Won)
                {
                    character.Wins++;
                    character.WinARow++;
                }
                else
                {
                    if(userUsage.RoundResult == RoundResult.Lost)
                        character.Loses++;

                    character.WinARow = 0;
                }
            }
            db.SaveChanges();

            return this.CreateWSEResponse(HttpStatusCode.OK, reqId);
        }

        [HttpGet]
        public HttpResponseMessage UpdateServerStatus(int reqId, int players, string token, string server)
        {
            string address = this.GetClientIp();
            Token tokenDb = db.Tokens.FirstOrDefault(t => t.Value == token);
            TokenStatus status = GameB.GetTokenStatus(tokenDb, server, address);

            if(status == TokenStatus.TokenOK)
            {
                GameServer gameServer = tokenDb.GameServer;
                gameServer.Address = address;
                gameServer.LastUpdate = DateTime.Now;
                if (players == 0)
                    gameServer.CharList = null;
                db.SaveChanges();
            }
            return this.CreateWSEResponse(HttpStatusCode.OK, reqId, (int)status);
        }

        [HttpGet]
        [RequireToken]
        public HttpResponseMessage ResetPassword(int reqId, int id)
        {
            var user = db.Users.Select(u => new { UserId = u.UserId, UniqueId = u.UniqueId, Username=u.Username })
                .FirstOrDefault(u => u.UniqueId == id);
            Code code = UserB.GetCode(CodeType.ResetPassword, db);
            code.UserId = user.UserId;
            db.Codes.Add(code);
            db.SaveChanges();

            return this.CreateWSEResponse(HttpStatusCode.OK, reqId, new { Status = (int)GetCharStatus.Ok, user.UniqueId, user.Username, code.Value });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private IEnumerable<UserScore> GetUserScoresFromString(string stringIdScores, BoEContext db)
        {
            IEnumerable<int[]> idScs = stringIdScores.Replace(" ", "").Split(new string[] { "],[" }, StringSplitOptions.None)
                .Select(s => s.Replace("[", "").Replace("]", "").Split(',').Select(ss => int.Parse(ss)).ToArray());
            List<int?> uniqueIds = idScs.Select(ids => (int?)ids[0]).ToList();
            IEnumerable<User> users = db.Users.Include("Character").Where(u => uniqueIds.Contains(u.UniqueId));
            List<UserScore> userScores = new List<UserScore>();
            foreach (User user in users)
            {
                int[] userStats = idScs.First(ids => ids[0] == user.UniqueId);
                userScores.Add(new UserScore()
                {
                    User = user,
                    Score = userStats[1],
                    Kills = userStats[2],
                    Deaths = userStats[3],
                    Tick = userStats[4]
                });
            }
            return userScores;
        }
        private IEnumerable<UserUsagesModel> GetUserUpKeepFromString(string usgs, BoEContext db)
        {
            IEnumerable<int[]> usages = usgs.Replace(" ", "").Split(new string[] { "],[" }, StringSplitOptions.None).Select(s => s.Replace("[", "").Replace("]", "").Split(',').Select(ss => int.Parse(ss)).ToArray());
            List<int> uniqueIds = usages.Select(ids => ids[0]).ToList();
            IEnumerable<User> users = db.Users.Where(c => uniqueIds.Contains(c.UniqueId.Value));
            List<UserUsagesModel> userUks = new List<UserUsagesModel>();
            foreach (User user in users)
            {
                Dictionary<int, int> usg = new Dictionary<int, int>();
                int[] uusgs = usages.First(u => u[0] == user.UniqueId);
                for (int j = 3; j < uusgs.Length; j += 2)
                    usg.Add(uusgs[j], uusgs[j + 1]);

                userUks.Add(new UserUsagesModel()
                {
                    User = user,
                    RoundResult = (RoundResult)uusgs[1],
                    RespawnCount = uusgs[2],
                    Usages = usg
                });
            }
            return userUks;
        }
    }
}