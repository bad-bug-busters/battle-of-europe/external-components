﻿using BoE.Contexts;
using BoE.Models;
using BoE.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoE.WebSite.Business;
using BoE.Business;

namespace BoE.WebSite.Controllers.API
{
    public class ShopController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpPost]
        public HttpResponseMessage Buy([FromBody]IEnumerable<Int32> ids)
        {
            IEnumerable<Item> items = db.Items.Where(i => ids.Contains(i.ItemId));
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            List<Inventory> invs = new List<Inventory>();
            foreach (Item item in items)
            {
                if(!ShopB.CanBuyItem(item, character, db))
                    return Request.CreateResponse(HttpStatusCode.Conflict, "No.");

                for (int i = 0; i < ids.Where(n => n == item.ItemId).Count(); i++)
                {
                    character.Gold -= item.BuyValue;
                    Inventory inv = new Inventory()
                    {
                        ItemId = item.ItemId,
                        CharacterId = character.CharacterId
                    };
                    invs.Add(inv);
                    db.Inventories.Add(inv);
                }
            }
            if (character.Gold < 0)
                return Request.CreateResponse(HttpStatusCode.Conflict);

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Accepted, new { Gold=character.Gold, Invs=invs } );
        }

        [HttpPost]
        public HttpResponseMessage Sell([FromBody]IEnumerable<Int32> ids)
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            List<Inventory> toSell = character.Inventories.Where(iv => ids.Contains(iv.InventoryId) && iv.RepairPrice == null).ToList();
            foreach (Inventory inv in toSell)
                ShopB.SellItem(inv, false, db);

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Accepted, character.Gold);
        }
    }
}
