﻿using BoE.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BoE.WebSite.Business;
using BoE.Models;
using System.Web.Script.Serialization;
using System.IO;
using BoE.Lib;
using BoE.Business;
using BoE.WebSite.Models;

namespace BoE.WebSite.Controllers
{
    [Authorize]
    public class DataController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetUser()
        {
            User user = db.Users.Find(BoESecurity.CurrentUserId);
            if (user == null) 
            {
                BoESecurity.Logout();
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }

            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        [HttpGet]
        public HttpResponseMessage GetCharacter()
        {
            return Request.CreateResponse(HttpStatusCode.OK, db.Characters.Find(BoESecurity.CurrentUserId));
        }

        [HttpGet]
        public HttpResponseMessage GetInventory()
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            return Request.CreateResponse(HttpStatusCode.OK, character != null ? character.Inventories : null);
        }

        [HttpGet]
        public HttpResponseMessage GetItemsHash()
        {
            return Request.CreateResponse(HttpStatusCode.OK, 
                File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/itemsHash.txt")));
        }

        [HttpGet]
        public HttpResponseMessage GetItems()
        {
            List<Item> items = db.Items.ToList();
            return Request.CreateResponse(HttpStatusCode.OK, new { 
                items = items, 
                hash = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/itemsHash.txt")) 
            });
        }

        [HttpGet]
        public HttpResponseMessage GetServers()
        {
            DateTime limit =  DateTime.Now.AddHours(-24);
            return Request.CreateResponse(HttpStatusCode.OK, 
                db.GameServers.Where(gs => gs.LastUpdate.HasValue && (gs.LastUpdate.Value > limit || (gs.LastTick.HasValue && gs.LastTick.Value > limit))));
        }

        [HttpGet]
        public HttpResponseMessage GetCharacters(int page = 0)
        {
            int skip = page * 50;
            int take = skip + 50;
            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                Characters = db.Characters.OrderByDescending(c => c.Generation)
                .OrderByDescending(c => c.Experience)
                .Skip(skip).Take(take)
                .Select(c => new CharacterModel() 
                {
                    CharacterId = c.CharacterId,
                    Name = c.Name,
                    Experience = c.Experience,
                    Generation = c.Generation,
                    CreationDate = c.CreationDate,
                }),
                Count = db.Characters.Count()
            });
        }

        [HttpGet]
        public HttpResponseMessage GetClans()
        {
            Clan clan = db.Characters.Find(BoESecurity.CurrentUserId).Clan;
            List<ClanModel> clans = db.Clans.Select(c => new ClanModel() 
            { 
                ClanId=c.ClanId,
                Name = c.Name,
                Description = c.Description,
                MembersCount = c.Characters.Count,
                CreationDate = c.CreationDate
            }).ToList();

            if (clan != null)
            {
                ClanModel toEdit = clans.First(c => c.ClanId == clan.ClanId);
                toEdit.Members = clan.Characters.Select(c => new MemberModel(c));
                toEdit.Applications = clan.ClanApplications.Select(ca => new ApplicationModel(ca));
            }

            return Request.CreateResponse(HttpStatusCode.OK, clans.OrderBy(c => c.Name));
        }

        [HttpGet]
        public HttpResponseMessage GetClanRoles()
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);

            return Request.CreateResponse(HttpStatusCode.OK, 
                character.ClanId.HasValue ? 
                character.Clan.ClanRoles : 
                new List<ClanRole>());
        }

        [HttpGet]
        public HttpResponseMessage GetNews()
        {
            return Request.CreateResponse(HttpStatusCode.OK, db.Newses.OrderByDescending(n => n.CreationDate).Take(5));
        }

        [HttpGet]
        public HttpResponseMessage GetLooms()
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            return Request.CreateResponse(HttpStatusCode.OK, character != null ? character.Looms : null);
        }
    }
}
