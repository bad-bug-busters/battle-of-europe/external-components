﻿using BoE.Contexts;
using BoE.Models;
using BoE.WebSite.Business;
using BoE.WebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BoE.WebSite.Controllers
{
    public class AccountController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Login(LoginModel model)
        {
            if (BoESecurity.Login(model.Username, model.Password, this.GetClientIp(), this.GetBrowserInfo(), db))
                return Request.CreateResponse(HttpStatusCode.Accepted, db.Users.FirstOrDefault(u => u.Username == model.Username));

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Register(RegisterModel model)
        {
            string result = BoESecurity.CreateAccount(model.Username, model.Password, model.Code);
            if (result != model.Username)
                return Request.CreateResponse(HttpStatusCode.Forbidden, result);

            BoESecurity.Login(model.Username, model.Password, this.GetClientIp(), this.GetBrowserInfo(), db);

            return Request.CreateResponse(HttpStatusCode.Accepted, db.Users.FirstOrDefault(u => u.Username == model.Username));
        }

        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage ResetPassword(ResetPasswordModel model)
        {
            if (BoESecurity.ResetPassword(model.Username, model.Code, model.Password))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpGet]
        public HttpResponseMessage Logout()
        {
            BoESecurity.Logout();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
