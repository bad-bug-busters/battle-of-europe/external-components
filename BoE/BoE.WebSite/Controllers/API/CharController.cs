﻿using BoE.Business;
using BoE.Contexts;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using BoE.WebSite.Business;

namespace BoE.WebSite.Controllers
{
    public class CharController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpPost]
        public HttpResponseMessage SetActifBuild([FromBody]int id)
        {
            if (db.Builds.Find(id).CharacterId != BoESecurity.CurrentUserId)
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            db.Users.Find(BoESecurity.CurrentUserId).Character.BuildId = id;
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage AddBuild()
        {
            int max = int.Parse(WebConfigurationManager.AppSettings["MaxBuilds"]);
            if (db.Users.Find(BoESecurity.CurrentUserId).Character.Builds.Count >= max)
                return Request.CreateResponse(HttpStatusCode.Conflict);
            Build build = BuildB.GetNewBuild(BoESecurity.CurrentUserId);
            db.Builds.Add(build);
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Created, build);
        }

        [HttpPost]
        public HttpResponseMessage DeleteBuild([FromBody]int id)
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            Build build = character.Builds.FirstOrDefault(b => b.BuildId == id);
            if(build == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            if(build.BuildId == character.BuildId.Value)
                return Request.CreateResponse(HttpStatusCode.Conflict);

            db.Builds.Remove(build);
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage SaveBuild(Build b)
        {
            Build build = db.Builds.Find(b.BuildId);
            b.Character = build.Character;
            if (db.Builds.Find(build.BuildId).CharacterId != BoESecurity.CurrentUserId)
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            if (BuildB.IsValid(b) && BuildB.SetBuild(b, build))
            {
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.Accepted);
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }

        [HttpPost]
        public HttpResponseMessage Retire()
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            if(character.Level < 31)
                return Request.CreateResponse(HttpStatusCode.Conflict);

            db.UserActions.Add(new UserRetire() { 
                AddressIp=this.GetClientIp(),
                Experience=character.Experience,
                Generation=character.Generation,
                UserId=character.CharacterId
            });

            character.Retire();
            foreach (Build build in character.Builds.ToList())
                db.Builds.Remove(build);
            character.Builds.Add(UserB.GetStartBuild(db, character));
            db.SaveChanges();
            character.BuildId = character.Builds.First().BuildId;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, character);
        }
    }
}
