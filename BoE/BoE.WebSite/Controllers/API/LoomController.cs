﻿using BoE.Contexts;
using BoE.Models;
using BoE.WebSite.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BoE.WebSite.Controllers.API
{
    public class LoomController : ApiController
    {
        BoEContext db = new BoEContext();

        [HttpPost]
        public HttpResponseMessage BuyLoom([FromBody]LoomType type)
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            if (character.LoomPoints <= 0)
                return Request.CreateResponse(HttpStatusCode.Conflict);

            Loom loom = db.Looms.Create();
            loom.Type = type;
            loom.CharacterId = character.CharacterId;
            db.Looms.Add(loom);
            character.LoomPoints--;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, loom);
        }

        [HttpPost]
        public HttpResponseMessage UpgradeLoom([FromBody]int loomId)
        {
            Character character = db.Characters.Find(BoESecurity.CurrentUserId);
            Loom loom = db.Looms.Find(loomId);
            if (character.LoomPoints <= 0)
                return Request.CreateResponse(HttpStatusCode.Conflict);
            if(loom == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            loom.Level++;
            character.LoomPoints--;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, loom);
        }
    }
}
