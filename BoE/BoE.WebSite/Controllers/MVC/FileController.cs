﻿using BoE.Contexts;
using BoE.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using io = System.IO;

namespace BoE.WebSite.Controllers
{
    public class FileController : Controller
    {
        [OutputCache(CacheProfile = "ImagesResult")]
        public FileResult GetItemImage(int id = 0, bool full = false)
        {
            string folderPath = Server.MapPath("/Content/images/items");
            string path = folderPath + "\\" + id + ( full ? "" : "_thumb") + ".jpg";
            if (!io.File.Exists(path))
                return File(io.File.ReadAllBytes(folderPath + "\\not-found" + (full ? "" : "_thumb") + ".jpg"), "image/jpeg");
            return File(io.File.ReadAllBytes(path), "image/jpeg");
        }

        [OutputCache(CacheProfile = "ImagesResult")]
        public FileResult GetClanBanner(int id = 0)
        {
            using(BoEContext db = new BoEContext())
            {
                string fileName = "no_banner.dds";
                var clan = db.Clans.Select(c => new 
                { 
                    ClanId=c.ClanId,
                    Banner=c.Banner
                }).FirstOrDefault(c => c.ClanId == id);

                if (clan != null && clan.Banner != null && clan.Banner.Length != 0)
                {
                    fileName = string.Format("clan_{0}.png", clan.ClanId);
                    return File(clan.Banner, MIMEAssistant.GetMIMEType(fileName), fileName);
                }

                string folderPath = Server.MapPath("/Content/images");
                return File(io.File.ReadAllBytes(folderPath + "\\clan-no-banner.png"), "image/png");
            }
        }
    }
}
