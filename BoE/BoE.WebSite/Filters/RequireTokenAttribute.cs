﻿using BoE.Business;
using BoE.Contexts;
using BoE.Formatters;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BoE.WebSite.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public sealed class RequireTokenAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            int reqId = int.Parse(HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query).Get("reqId"));

            if (!HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query).AllKeys.Contains("token"))
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new WSEModel(HttpStatusCode.Unauthorized, reqId, GameB.TokenMessages[TokenStatus.WrongToken]));
                return;
            }
            if (!HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query).AllKeys.Contains("server"))
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                    new WSEModel(HttpStatusCode.Unauthorized, reqId, GameB.TokenMessages[TokenStatus.WrongToken]));
                return;
            }
            using (BoEContext db = new BoEContext())
            {
                string address = ((System.Web.HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"]).Request.UserHostAddress;
                string tokenValue = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query).Get("token");
                string name = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query).Get("server");
                Token token = null;
                if (tokenValue != null)
                    token = db.Tokens.FirstOrDefault(t => t.Value == tokenValue);
                if (token == null)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new WSEModel(HttpStatusCode.Unauthorized, reqId, GameB.TokenMessages[TokenStatus.WrongToken]));
                    return;
                }
                TokenStatus status = GameB.GetTokenStatus(token, name, address);
                if (status != TokenStatus.TokenOK)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                        new WSEModel(HttpStatusCode.Unauthorized, reqId, GameB.TokenMessages[status]));
                }
            }
        }
    }
}