﻿using BoE.WebSite.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Security;
using WebMatrix.WebData;

namespace BoE.WebSite.Filters
{

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class RequireRoleAttribute : ActionFilterAttribute
    {
        public string Role { get; set; }


        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (!Roles.IsUserInRole(BoESecurity.CurrentUserName, Role))
                actionContext.Response.StatusCode = HttpStatusCode.Unauthorized;
        }
    }
}