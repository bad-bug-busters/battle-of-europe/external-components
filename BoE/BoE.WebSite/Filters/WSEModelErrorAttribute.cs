﻿using BoE.Formatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;


namespace BoE.WebSite.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class WSEModelErrorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string reqIdString = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query).AllKeys.Contains("reqId") ?
                HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query).Get("reqId") :
                "";
            int reqId = -1;
            int.TryParse(reqIdString, out reqId);

            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, 
                    new WSEModel(HttpStatusCode.BadRequest, reqId, "Invalid parameters"));
            }
        }
    }
}