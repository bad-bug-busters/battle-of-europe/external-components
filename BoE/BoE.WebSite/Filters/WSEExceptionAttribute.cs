﻿using BoE.Formatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace BoE.WebSite.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class WSEExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            string reqIdString = HttpUtility.ParseQueryString(context.Request.RequestUri.Query).AllKeys.Contains("reqId") ?
                HttpUtility.ParseQueryString(context.Request.RequestUri.Query).Get("reqId") :
                "";
            int reqId = -1;
            int.TryParse(reqIdString, out reqId);

            context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError,
                new WSEModel(HttpStatusCode.InternalServerError, reqId, context.Exception.GetBaseException().Message));
        }
    }
}