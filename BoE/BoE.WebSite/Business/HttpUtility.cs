﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;

namespace BoE.WebSite.Business
{
    public static class HttpUtility
    {
        public static string GetClientIp(this ApiController controller)
        {
            if (controller.Request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((System.Web.HttpContextWrapper)controller.Request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (controller.Request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                RemoteEndpointMessageProperty prop;
                prop = (RemoteEndpointMessageProperty)controller.Request.Properties[RemoteEndpointMessageProperty.Name];
                return prop.Address;
            }
            else
            {
                return null;
            }
        }

        public static HttpBrowserCapabilities GetBrowserInfo(this ApiController controller)
        {
            string userAgent = HttpContext.Current.Request.UserAgent;
            HttpBrowserCapabilities userBrowser = new HttpBrowserCapabilities { Capabilities = new Hashtable { { string.Empty, userAgent } } };
            BrowserCapabilitiesFactory factory = new BrowserCapabilitiesFactory();
            factory.ConfigureBrowserCapabilities(new NameValueCollection(), userBrowser);

            return userBrowser;
        }

        public static HttpBrowserCapabilities GetBrowserInfo(this Controller controller)
        {
            string userAgent = HttpContext.Current.Request.UserAgent;
            HttpBrowserCapabilities userBrowser = new HttpBrowserCapabilities { Capabilities = new Hashtable { { string.Empty, userAgent } } };
            BrowserCapabilitiesFactory factory = new BrowserCapabilitiesFactory();
            factory.ConfigureBrowserCapabilities(new NameValueCollection(), userBrowser);

            return userBrowser;
        }
    }
}