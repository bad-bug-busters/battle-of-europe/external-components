﻿using BoE.Formatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BoE.WebSite.Business
{
    public static class HttpB
    {
        public static HttpResponseMessage CreateWSEResponse(this ApiController controller, HttpStatusCode code, int reqId)
        {
            return controller.Request.CreateResponse(code, new WSEModel(code, reqId));
        }

        public static HttpResponseMessage CreateWSEResponse(this ApiController controller, HttpStatusCode code, int reqId, object content)
        {
            return controller.Request.CreateResponse(code, new WSEModel(code, reqId, content));
        }
    }
}