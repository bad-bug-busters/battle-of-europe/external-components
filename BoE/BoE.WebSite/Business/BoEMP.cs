﻿using BoE.Contexts;
using BoE.Lib;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using WebMatrix.WebData;

namespace BoE.WebSite.Business
{
    public class BoEMP : ExtendedMembershipProvider
    {
        private string EncodePassword(string pass)
        {
            return Utils.HashPassword(pass, "TUBjay9EdWRl");
        }

        public bool ResetPassword(string username, string code, string password)
        {
            using (BoEContext db = new BoEContext())
            {
                User user = db.Users.FirstOrDefault(u => u.Username == username);
                if (user == null)
                    return false;
                Code codeDb = user.Codes.FirstOrDefault(c => c.Type == CodeType.ResetPassword && c.Value == code);
                if (codeDb == null || codeDb.CreationDate.AddMinutes(-10) > DateTime.Now)
                    return false;
                user.Password = EncodePassword(password);
                db.Codes.Remove(codeDb);
                db.SaveChanges();
                return true;
            }
        }

        public string CreateAccount(string username, string password, string codeValue)
        {
            using (BoEContext db = new BoEContext())
            {
                Code code = db.Codes.FirstOrDefault(c => c.Type== CodeType.Register && c.Value == codeValue);
                if (code == null)
                    return "Code incorrect";
                if(db.Users.FirstOrDefault(u => u.Username == username) != null)
                    return "Username taken";
                User user = code.User;
                user.Username = username;
                user.Password = EncodePassword(password);
                user.State = UserState.Normal;
                user.LastPasswordSuccess = DateTime.Now;
                user.AccountEnabled = true;
                db.Codes.Remove(code);
                db.SaveChanges();
                return user.Username;
            }
        }

        public override bool ValidateUser(string username, string password)
        {
            using (BoEContext db = new BoEContext())
            {
                User user = db.Users.FirstOrDefault(u => u.Username == username);
                if (user == null)
                    return false;

                bool passwordIsValid = user.Password == EncodePassword(password);
                if (!passwordIsValid)
                {
                    user.PasswordFailures++;
                    user.LastPasswordFailure = DateTime.Now;
                }
                else
                {
                    user.PasswordFailures = 0;
                    user.LastPasswordSuccess = DateTime.Now;
                }
                db.SaveChanges();
                return user.AccountEnabled && passwordIsValid;
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            using (BoEContext db = new BoEContext())
            {
                User user = db.Users.FirstOrDefault(u => u.Username == username);
                if (user.Password != EncodePassword(oldPassword))
                    return false;
                user.Password = EncodePassword(newPassword);
                db.SaveChanges();
                return true;
            }
        }

        public override bool DeleteAccount(string userName)
        {
            using (BoEContext db = new BoEContext())
            {
                User user = db.Users.FirstOrDefault(u => u.Username == userName);
                user.Password = null;
                user.State = UserState.New;
                db.SaveChanges();
                return true;
            }
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            using (BoEContext db = new BoEContext())
            {
                User user = db.Users.FirstOrDefault(u => u.Username == username);
                if (user == null)
                    return null;
                return new BoEMU(
                    "BoEMP",
                    user.Username,
                    user.UserId,
                    null,
                    null,
                    null,
                    true,
                    !user.AccountEnabled,
                    user.CreationDate,
                    user.LastPasswordSuccess.Value,
                    DateTime.Now,
                    DateTime.Now,
                    DateTime.Now);
            }
        }

        public override DateTime GetCreateDate(string userName)
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Users.FirstOrDefault(u => u.Username == userName).CreationDate;
            }
        }

        public override DateTime GetLastPasswordFailureDate(string userName)
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Users.FirstOrDefault(u => u.Username == userName).LastPasswordFailure.Value;
            }
        }

        public override int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Users.FirstOrDefault(u => u.Username == userName).PasswordFailures;
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Hashed; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return "^.*(?=.{6,})(?=.*\\d).*$"; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 1; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { return 10; }
        }

        public override string CreateAccount(string username, string password, bool requireConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override bool ConfirmAccount(string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override bool ConfirmAccount(string userName, string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override string CreateUserAndAccount(string userName, string password, bool requireConfirmation, IDictionary<string, object> values)
        {
            throw new NotImplementedException();
        }


        public override string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow)
        {
            throw new NotImplementedException();
        }

        public override ICollection<OAuthAccountData> GetAccountsForUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetPasswordChangedDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetUserIdFromPasswordResetToken(string token)
        {
            throw new NotImplementedException();
        }

        public override bool IsConfirmed(string userName)
        {
            throw new NotImplementedException();
        }

        public override bool ResetPasswordWithToken(string token, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out System.Web.Security.MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(System.Web.Security.MembershipUser user)
        {
            throw new NotImplementedException();
        }
    }
}