﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace BoE.WebSite.Business
{
    public class BoEMU : MembershipUser
    {
        //DAT PATE
        public BoEMU(string providername,
                    string username,
                    object providerUserKey,
                    string email,
                    string passwordQuestion,
                    string comment,
                    bool isApproved,
                    bool isLockedOut,
                    DateTime creationDate,
                    DateTime lastLoginDate,
                    DateTime lastActivityDate,
                    DateTime lastPasswordChangedDate,
                    DateTime lastLockedOutDate) :
            base(providername,
                    username,
                    providerUserKey,
                    email,
                    passwordQuestion,
                    comment,
                    isApproved,
                    isLockedOut,
                    creationDate,
                    lastLoginDate,
                    lastActivityDate,
                    lastPasswordChangedDate,
                    lastLockedOutDate)
        {

        }
    }
}