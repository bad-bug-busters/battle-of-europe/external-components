﻿using BoE.Contexts;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using MP = WebMatrix.WebData.WebSecurity;

namespace BoE.WebSite.Business
{
    public static class BoESecurity
    {
        public static string CreateAccount(string username, string password, string code)
        {
            return ((BoEMP)Membership.Provider).CreateAccount(username, password, code);
        }

        public static bool Login(string username, string password, string clientIp, HttpBrowserCapabilities browser, BoEContext db)
        {
            if (MP.Login(username, password, true))
            {
                User user = db.Users.FirstOrDefault(u => u.Username == username);
                db.UserActions.Add(new UserLogin()
                {
                    User = user,
                    AddressIp = clientIp,
                    BrowserName = browser.Browser,
                    BrowserVersion = browser.Version
                });
                db.SaveChanges();
                return true;
            }
            return false;
        }

        public static bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            return MP.ChangePassword(username, oldPassword, newPassword);
        }

        public static bool ResetPassword(string username, string code, string password)
        {
            return ((BoEMP)Membership.Provider).ResetPassword(username, code, password);
        }

        public static void Logout()
        {
            MP.Logout();
        }

        public static int CurrentUserId
        {
            get { return MP.CurrentUserId; }
        }

        public static string CurrentUserName
        {
            get { return MP.CurrentUserName; }
        }

        public static bool IsAuthenticated
        {
            get { return MP.IsAuthenticated; }
        }
    }
}