﻿using BoE.Contexts;
using BoE.Lib;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace BoE.WebSite.Business
{
    public class BoERP : RoleProvider
    {
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            using (BoEContext db = new BoEContext())
            {
                List<User> users = db.Users.Where(Utils.CollectionContainsValueSelectorExpression<User>(usernames, u => u.Username)).ToList();
                List<Role> roles = db.Roles.Where(Utils.CollectionContainsValueSelectorExpression<Role>(roleNames, r => r.Name)).ToList();
                users.ForEach(u => roles.ForEach(r => r.Users.Add(u)));
                db.SaveChanges();
            }
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            using (BoEContext db = new BoEContext())
            {
                db.Roles.Add(new Role() { Name = roleName });
                db.SaveChanges();
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            using (BoEContext db = new BoEContext())
            {
                Role role = db.Roles.FirstOrDefault(r => r.Name == roleName);
                if (role == null)
                    return false;
                foreach (User user in role.Users)
                {
                    user.Roles.Remove(role);
                    role.Users.Remove(user);
                }
                db.Roles.Remove(role);
                db.SaveChanges();
                return true;
            }
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Roles.Select(r => r.Name).ToArray();
            }
        }

        public override string[] GetRolesForUser(string username)
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Roles.Where(r => r.Users.FirstOrDefault(u => u.Username == username) != null).Select(r => r.Name).ToArray();
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Users.Where(u => u.Roles.FirstOrDefault(r => r.Name == roleName) != null).Select(u => u.Username).ToArray();
            }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Users.FirstOrDefault(u => u.Username == username).Roles.FirstOrDefault(r => r.Name == roleName) != null;
            }
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            using (BoEContext db = new BoEContext())
            {
                List<User> users = db.Users.Where(Utils.CollectionContainsValueSelectorExpression<User>(usernames, u => u.Username)).ToList();
                List<Role> roles = db.Roles.Where(Utils.CollectionContainsValueSelectorExpression<Role>(roleNames, r => r.Name)).ToList();
                foreach (User user in users)
                {
                    foreach (Role role in roles)
                    {
                        user.Roles.Remove(role);
                        role.Users.Remove(user);
                    }
                }
                db.SaveChanges();
            }
        }

        public override bool RoleExists(string roleName)
        {
            using (BoEContext db = new BoEContext())
            {
                return db.Roles.FirstOrDefault(r => r.Name == roleName) != null;
            }
        }
    }
}