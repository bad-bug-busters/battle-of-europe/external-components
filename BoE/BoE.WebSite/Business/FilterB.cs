﻿using BoE.Contexts;
using BoE.Models;
using BoE.WebSite.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Business
{
    public class FilterB
    {
        public static IEnumerable<CharacterModel> FilterCharacters(BoEContext db, CharacterFilterModel model)
        {
            if(model == null)
                model = new CharacterFilterModel();

            IQueryable<CharacterModel> characters = db.Characters.Select(c => new CharacterModel()
            {
                CharacterId=c.CharacterId,
                Name=c.Name,
                Gold=c.Gold,
                Experience=c.Experience,
                CreationDate=c.CreationDate,
                LastSeen = c.LastTick
            });

            if(!string.IsNullOrEmpty(model.Name))
                characters = characters.Where(c => c.Name.Contains(model.Name));

            return characters.Take(model.Take);
        }
    }
}