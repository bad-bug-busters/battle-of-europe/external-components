﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace BoE.WebSite.Business
{
    public class AdminLogUtils
    {
        public static void AddLogs(IEnumerable<string> logs)
        {
            string logFile = HttpContext.Current.Server.MapPath("~/App_Data/AdminLog.txt");
            if (!File.Exists(logFile))
                File.Create(logFile);

            File.AppendAllLines(logFile, logs.Select(l => string.Format("{0} - {1}", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), l)));
        }

        public static IEnumerable<string> ReadLogs()
        {
            string logFile = HttpContext.Current.Server.MapPath("~/App_Data/AdminLog.txt");
            return File.ReadAllLines(logFile);
        }
    }
}