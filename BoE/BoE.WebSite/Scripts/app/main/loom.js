﻿/// <reference path="../_references.js" />
var boe = boe || {};
boe.loom = boe.loom || {};

boe.loom.navLefts = [
    boe.navLeft.shop,
    boe.navLeft.loom,
    boe.navLeft.trade
];

boe.loom.LoomTypeSelect = function (type, name, description) {
    var self = this;

    self.name = ko.observable(name);
    self.type = ko.observable(type);
    self.icon = ko.observable('/Content/images/looms/' + boe.loom.loomTypesIcon[type]);
    self.description = ko.observable(description);
};

boe.loom.LoomViewModel = function (db) {
    var self = this;

    self.db = db;

    self.working = ko.observable(false);

    self.character = self.db.character;

    self.looms = self.db.looms;
    self.loomPoints = self.character.LoomPoints;

    self.loomsFiltered = ko.pureComputed(function () {
        return self.looms.sortBy(function (l) { return -l.Level(); });
    });

    self.loomTypesSelect = ko.observableArray([
        new boe.loom.LoomTypeSelect(boe.models.loomType.LHead, 'Light head armor', 'all light head armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.MHead, 'Medium head armor', 'all medium head armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.HHead, 'Heavy head armor', 'all heavy head armors'),

        new boe.loom.LoomTypeSelect(boe.models.loomType.LBody, 'Light body armor', 'all light body armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.MBody, 'Medium body armor', 'all medium body armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.HBody, 'Heavy body armor', 'all heavy body armors'),

        new boe.loom.LoomTypeSelect(boe.models.loomType.LHand, 'Light hand armor', 'all light hand armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.MHand, 'Medium hand armor', 'all medium hand armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.HHand, 'Heavy hand armor', 'all heavy hand armors'),

        new boe.loom.LoomTypeSelect(boe.models.loomType.LFoot, 'Light foot armor', 'all light foot armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.MFoot, 'Medium foot armor', 'all medium foot armors'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.HFoot, 'Heavy foot armor', 'all heavy foot armors'),

        new boe.loom.LoomTypeSelect(boe.models.loomType.OneH, 'One handed', 'all one handed weapons'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.OneHalf, 'One half handed', 'all one and half handed weapons'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.TwoH, 'Two handed', 'all two handed weapons'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.Polearm, 'Polearm', 'all polearms'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.XBow, 'Crossbow', 'all crossbows and bolts'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.Bow, 'Bow', 'all bows and arrows'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.Throwing, 'Throwing', 'all throwing weapons'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.Musket, 'Firearm', 'all firearms and bullets'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.Shield, 'Shield', 'all shields'),
        new boe.loom.LoomTypeSelect(boe.models.loomType.Horse, 'Horse', 'all horses')
    ]);
    self.selectedLT = ko.observable();
    self.selectedLoomType = ko.pureComputed(function () {
        return self.loomTypesSelect.find(function (lt) { return lt.type() == self.selectedLT(); });
    });

    self.loomSelectFormat = function (opt) {
        if (!opt.id)
            return opt.text;
        return "<img width='50' height='50' class='loom-select-icon' src='" + self.loomTypesSelect.find(function (lts) {
            return lts.type() == opt.id;
        }).icon() + "'/>" + opt.text;
    }

    self.buyLoom = function () {
        if (!self.looms.any(function (lo) { return lo.Type() == self.selectedLT(); }) ||
            confirm('Are you sure you want to buy this heriloom ? You already have an heirloom card of the same type, two card will not add themselves.')) {
            self.working(true);
            boe.ajax("POST", "/api/Loom/BuyLoom", self.selectedLT()).done(function (loom) {
                self.loomPoints(self.loomPoints() - 1);
                self.looms.add(loom);
            }).always(function () {
                self.working(false);
            });
        }
    };
    self.canBuyLoom = ko.pureComputed(function () {
        return self.loomPoints() > 0 && self.selectedLoomType() && !self.working();
    });

    self.upgradeLoom = function (loom) {
        if (confirm('Are you sure you want to upgrade ' + boe.loom.loomTypesName[loom.Type()] + ' to +' + (loom.Level() + 1))) {
            self.working(true);
            boe.ajax("POST", "/api/Loom/UpgradeLoom", loom.LoomId()).done(function (newLoom) {
                self.loomPoints(self.loomPoints() - 1);
                ko.mapping.fromJS(newLoom, loom);
            }).always(function () {
                self.working(false);
            });
        }
    };

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#market-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.loom.navLefts, boe.navLeft.loom);
        $("#loom").show();
    };
};

boe.loom.loomTypesName = {};
boe.loom.loomTypesName[boe.models.loomType.LHead] = "Light head armor";
boe.loom.loomTypesName[boe.models.loomType.MHead] = "Medium head armor";
boe.loom.loomTypesName[boe.models.loomType.HHead] = "Heavy head armor";
boe.loom.loomTypesName[boe.models.loomType.LBody] = "Light body armor";
boe.loom.loomTypesName[boe.models.loomType.MBody] = "Medium body armor";
boe.loom.loomTypesName[boe.models.loomType.HBody] = "Heavy body armor";
boe.loom.loomTypesName[boe.models.loomType.LHand] = "Light hand armor";
boe.loom.loomTypesName[boe.models.loomType.MHand] = "Medium hand armor";
boe.loom.loomTypesName[boe.models.loomType.HHand] = "Heavy hand armor";
boe.loom.loomTypesName[boe.models.loomType.LFoot] = "Light foot armor";
boe.loom.loomTypesName[boe.models.loomType.MFoot] = "Medium foot armor";
boe.loom.loomTypesName[boe.models.loomType.HFoot] = "Heavy foot armor";
boe.loom.loomTypesName[boe.models.loomType.OneH] = "One handed weapon";
boe.loom.loomTypesName[boe.models.loomType.OneHalf] = "One half handed weapon";
boe.loom.loomTypesName[boe.models.loomType.TwoH] = "Two handed weapon";
boe.loom.loomTypesName[boe.models.loomType.Polearm] = "Polearm";
boe.loom.loomTypesName[boe.models.loomType.XBow] = "Crossbow";
boe.loom.loomTypesName[boe.models.loomType.Bow] = "Bow";
boe.loom.loomTypesName[boe.models.loomType.Throwing] = "Throwing";
boe.loom.loomTypesName[boe.models.loomType.Musket] = "Firearm";
boe.loom.loomTypesName[boe.models.loomType.Shield] = "Shield";
boe.loom.loomTypesName[boe.models.loomType.Horse] = "Horse";

boe.loom.loomTypesIcon = {};
boe.loom.loomTypesIcon[boe.models.loomType.LHead] = "lhead.png";
boe.loom.loomTypesIcon[boe.models.loomType.MHead] = "mhead.png";
boe.loom.loomTypesIcon[boe.models.loomType.HHead] = "hhead.png";
boe.loom.loomTypesIcon[boe.models.loomType.LBody] = "lbody.png";
boe.loom.loomTypesIcon[boe.models.loomType.MBody] = "mbody.png";
boe.loom.loomTypesIcon[boe.models.loomType.HBody] = "hbody.png";
boe.loom.loomTypesIcon[boe.models.loomType.LHand] = "lhand.png";
boe.loom.loomTypesIcon[boe.models.loomType.MHand] = "mhand.png";
boe.loom.loomTypesIcon[boe.models.loomType.HHand] = "hhand.png";
boe.loom.loomTypesIcon[boe.models.loomType.LFoot] = "lfoot.png";
boe.loom.loomTypesIcon[boe.models.loomType.MFoot] = "mfoot.png";
boe.loom.loomTypesIcon[boe.models.loomType.HFoot] = "hfoot.png";
boe.loom.loomTypesIcon[boe.models.loomType.OneH] = "1h.png";
boe.loom.loomTypesIcon[boe.models.loomType.OneHalf] = "15h.png";
boe.loom.loomTypesIcon[boe.models.loomType.TwoH] = "2h.png";
boe.loom.loomTypesIcon[boe.models.loomType.Polearm] = "polearm.png";
boe.loom.loomTypesIcon[boe.models.loomType.XBow] = "crossbow.png";
boe.loom.loomTypesIcon[boe.models.loomType.Bow] = "bow.png";
boe.loom.loomTypesIcon[boe.models.loomType.Throwing] = "throwing.png";
boe.loom.loomTypesIcon[boe.models.loomType.Musket] = "musket.png";
boe.loom.loomTypesIcon[boe.models.loomType.Shield] = "shield.png";
boe.loom.loomTypesIcon[boe.models.loomType.Horse] = "horse.png";