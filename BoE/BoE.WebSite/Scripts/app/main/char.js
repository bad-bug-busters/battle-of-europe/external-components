﻿/// <reference path="../_references.js" />
var boe = boe || {};
boe.char = boe.char || {};

boe.char.navLefts = [
    boe.navLeft.char,
    boe.navLeft.inv,
    boe.navLeft.settings
];

boe.char.Value = function( value, name, description ) {
    var self = this;

    self.value = value;
    self.baseValue = ko.observable(self.value());
    self.name = ko.observable(name);
    self.description = ko.observable(description);
};
boe.char.Stat = function (value, name, description, point) {
    var self = this;
    boe.char.Value.call(self, value, name, description)
    self.point = point;

    self.up = function () {
        self.value(self.value() + 1);
    };
    self.down = function () {
        self.value(self.value() - 1);
    };
    self.canUp = ko.pureComputed(function () {
        return self.point.value() > 0;
    });
    self.canDown = ko.pureComputed(function () {
        return self.value() > self.baseValue();
    });
};
boe.char.Attr = function (value, name, description, point) {
    var self = this;
    boe.char.Stat.call(self, value, name, description, point);
    self.canUp = ko.pureComputed(function () {
        return self.point.value() > 0 && self.value() < boe.utils.build.attr.maxAttr;
    });
    self.canDown = ko.pureComputed(function () {
        return self.value() > self.baseValue() && self.value() > boe.utils.build.attr.minAttr;
    });
};
boe.char.Skill = function (value, name, description, point, attr) {
    var self = this;
    boe.char.Stat.call(self, value, name, description, point);
    self.attr = attr;
    self.canUp = ko.pureComputed(function () {
        return self.point.value() > 0 && self.value() + 1 <= self.attr.value() / 3 && self.value() < boe.utils.build.skill.maxSkill;
    });
};
boe.char.WPF = function (value, name, description, point) {
    var self = this;
    boe.char.Stat.call(self, value, name, description, point);
    self.up = function () {
        self.value(self.value() + 1);
    };
    self.down = function () {
        self.value(self.value() - 1);
    };
    self.canUp = ko.pureComputed(function () {
        return self.point.value() >= boe.utils.build.WPF.getWPFPointsToUp(self.value());
    });
};

boe.char.BuildViewModel = function (build, level, inventory) {
    var self = this;

    self.build = build;
    self.inventory = inventory;
    self.level = level;

    self.attrConverted = ko.observable(0);
    self.skillConverted = ko.observable(0);

    self.name = new boe.char.Value(self.build.Name, "Name", "");

    self.attrs = ko.observableArray();
    self.wpfs = ko.observableArray();
    self.skills = ko.observableArray();

    self.items = ko.pureComputed(function () {
        return  _.filter(self.inventory(), function (i) {
            return i.Item().Gender() == boe.models.itemGender.item;
        });
    });
    self.heads = ko.pureComputed(function () {
        return _.filter(self.inventory(), function (i) {
            return i.Item().Gender() == boe.models.itemGender.head;
        });
    });
    self.bodys = ko.pureComputed(function () {
        return _.filter(self.inventory(), function (i) {
            return i.Item().Gender() == boe.models.itemGender.body;
        });
    });
    self.foots = ko.pureComputed(function () {
        return _.filter(self.inventory(), function (i) {
            return i.Item().Gender() == boe.models.itemGender.foot;
        });
    });
    self.hands = ko.pureComputed(function () {
        return _.filter(self.inventory(), function (i) {
            return i.Item().Gender() == boe.models.itemGender.hand;
        });
    });
    self.horses = ko.pureComputed(function () {
        return _.filter(self.inventory(), function (i) {
            return i.Item().Gender() == boe.models.itemGender.horse;
        });
    });

    self.item1Id = new boe.char.Value(self.build.Item1Id, "Item1", "");
    self.item2Id = new boe.char.Value(self.build.Item2Id, "Item2", "");
    self.item3Id = new boe.char.Value(self.build.Item3Id, "Item3", "");
    self.item4Id = new boe.char.Value(self.build.Item4Id, "Item4", "");
    self.horseId = new boe.char.Value(self.build.HorseId, "Horse", "");

    self.headId = new boe.char.Value(self.build.HeadId, "Head", "");
    self.bodyId = new boe.char.Value(self.build.BodyId, "Body", "");
    self.footId = new boe.char.Value(self.build.FootId, "Foot", "");
    self.handId = new boe.char.Value(self.build.HandId, "Hand", "");

    self.getCost = function (inv) {
        return inv ? inv.Item().Value() : 0;
    };
    self.totalCost = ko.pureComputed(function () {
        return self.getCost(self.build.Item1()) + self.getCost(self.build.Item2()) + self.getCost(self.build.Item3()) +
            self.getCost(self.build.Item4()) + self.getCost(self.build.Head()) + self.getCost(self.build.Body()) +
            self.getCost(self.build.Hand()) + self.getCost(self.build.Foot()) + self.getCost(self.build.Horse());
    });

    self.attrPoints = new boe.char.Value(ko.computed(function() {
        var points = boe.utils.build.attr.getMaxAtt(self.level());
        _.each(self.attrs(), function (a) { points -= a.value(); });
        points -= self.attrConverted();
        points += (self.skillConverted() / boe.utils.build.skill.skillsForAtt);
        if (points < 0) {
            self.skillConverted(-points * boe.utils.build.skill.skillsForAtt);
            return 0;
        }
        return points;
        }), "Attribute points", "Sprend it to up attributes or convert to skill point");
    self.skillPoints = new boe.char.Value(ko.computed(function () {
        var points = boe.utils.build.skill.getMaxSkill(self.level());
        _.each(self.skills(), function (s) { points -= s.value(); });
        points -= self.skillConverted();
        points += (self.attrConverted() * boe.utils.build.skill.skillsForAtt);
        if (points < 0) {
            self.attrConverted(Math.ceil(-points / boe.utils.build.skill.skillsForAtt));
            return 0;
        }
        return points;
    }), "Skill points", "Spend it to up skills or convert it to attribute point");
    self.wpfPoints = new boe.char.Value(ko.computed(function () {
        var points = boe.utils.build.WPF.getMaxWPF(self.level(), self.weaponMaster ? self.weaponMaster.value() : 0);
        _.each(self.wpfs(), function (w) { points -= boe.utils.build.WPF.getWPFPointsUsed(w.value()); });
        return points;
    }), "Proficiency points", "Spend to up weapons proficiency");

    self.strength = new boe.char.Attr(self.build.Strength, "Strength", "Allows strength related skills to go up to 1/3 of the value. Increase melee damage by 0,2 flat and stamina by 60", self.attrPoints)
    self.agility = new boe.char.Attr(self.build.Agility, "Agility", "Allows agility related skills to go up to 1/3 of the value. Slightly increases running speed and increase stamina by 90", self.attrPoints)

    self.horseArchery = new boe.char.Skill(self.build.HorseArchery, "Horse archery", "Reduces damage and accuracy penalties for ranged weapons from horseback", self.skillPoints, self.agility);
    self.riding = new boe.char.Skill(self.build.Riding, "Riding", "Enables you to ride horses of higher difficulty levels and increases your riding speed and manuever", self.skillPoints, self.agility);
    self.athletics = new boe.char.Skill(self.build.Athletics, "Athletics", "Improves your running speed", self.skillPoints, self.agility);
    self.weaponMaster = new boe.char.Skill(self.build.WeaponMaster, "Weapon master", "Gives " + boe.utils.build.WPF.WPFPerWM + " weapon Proficiency for each point", self.skillPoints, self.agility);
    self.resistance = new boe.char.Skill(self.build.Resistance, "Resistance", "Reduces damage to shields by 5%", self.skillPoints, self.agility);
    self.powerThrow = new boe.char.Skill(self.build.PowerThrow, "Power throw", "Increases throwing damage by 10%", self.skillPoints, self.strength);
    self.powerStrike = new boe.char.Skill(self.build.PowerStrike, "Power strike", "Increases weapons melee damage by 8%", self.skillPoints, self.strength);
    self.powerPull = new boe.char.Skill(self.build.PowerPull, "Power pull", "Adds 15 points to archery and crossbow", self.skillPoints, self.strength);
    self.powerReload = new boe.char.Skill(self.build.PowerReload, "Power reload", "Adds 15 points to firearm and decrease missfire chance (0=55%, 1=45%, 2=36%, 3=28%, 4=21%, 5=15%, 6=10%, 7=6%, 8=3%, 9=1%, 10=0%)", self.skillPoints, self.strength);
    self.offHand = new boe.char.Skill(self.build.OffHand, "Offhand", "Increases the damage of kicking, fist fighting, punching and shield bashing by 8%", self.skillPoints, self.strength);

    self.oneHanded = new boe.char.WPF(self.build.OneHanded, "One handed", "Increases one handed weapons attack speed and damage", self.wpfPoints);
    self.twoHanded = new boe.char.WPF(self.build.TwoHanded, "Two handed", "Increases two handed weapons attack speed and damage", self.wpfPoints);
    self.polearm = new boe.char.WPF(self.build.Polearm, "Polearm", "Increases polearms attack speed and damage", self.wpfPoints);
    self.archery = new boe.char.WPF(self.build.Archery, "Archery", "Increases bows accuracy and attack speed", self.wpfPoints);
    self.crossbow = new boe.char.WPF(self.build.Crossbow, "Crossbow", "Increases crossbows accuracy and reload speed", self.wpfPoints);
    self.throwing = new boe.char.WPF(self.build.Throwing, "Throwing", "Increases throwing accuracy and throw speed", self.wpfPoints);
    self.firearm = new boe.char.WPF(self.build.Firearm, "Firearm", "Increases firearm accuracy and reload speed", self.wpfPoints);

    self.attrs([ self.strength, self.agility ]);
    self.skills([self.horseArchery, self.riding, self.athletics, self.weaponMaster, self.resistance,
        self.powerThrow, self.powerStrike, self.powerPull, self.powerReload, self.offHand]);
    self.wpfs([self.oneHanded, self.twoHanded, self.polearm, self.archery, self.crossbow, self.throwing, self.firearm]);

    self.convertAttr = function () { self.attrConverted(self.attrConverted() + 1); };
    self.canConvertAttr = ko.pureComputed(function () {
        return self.attrPoints.value() >= 1;
    });
    self.convertSkill = function () { self.skillConverted(self.skillConverted() + boe.utils.build.skill.skillsForAtt); };
    self.canConvertSkill = ko.pureComputed(function () {
        return self.skillPoints.value() >= boe.utils.build.skill.skillsForAtt;
    });

    self.changed = ko.pureComputed(function () {
        for (n in self) {
            if (self[n] instanceof boe.char.Value || self[n] instanceof boe.char.Attr || self[n] instanceof boe.char.Skill || self[n] instanceof boe.char.WPF)
                if (self[n].value() != self[n].baseValue())
                    return true;
        }
        return false;
    });

    self.setBases = function () {
        for (var n in self) {
            if (self[n] instanceof boe.char.Value || self[n] instanceof boe.char.Attr || self[n] instanceof boe.char.Skill || self[n] instanceof boe.char.WPF)
                self[n].baseValue(self[n].value());
        }
    };
    self.save = function () {
        boe.ajax("POST", "/api/Char/SaveBuild", ko.mapping.toJS(self.build)).done(function () {
            self.setBases();
        });
    };
};
boe.char.CharViewModel = function ( db ) {
    var self = this;
    
    self.db = db;

    self.character = self.db.character;
    self.builds = self.db.builds;
    self.inventory = self.db.inventory;

    self.filteredInventory = ko.pureComputed(function () {
        return _.sortBy(self.inventory.filter(function (iv) { return !iv.Disabled(); }), function (iv) { return -iv.Item().Value(); });
    });

    self.buildVms = ko.observableArray();
    self.updateBuildVms = function () {
        self.builds.each(function (b) {
            if(!self.buildVms.find(function(bv){ return bv.build.BuildId() == b.BuildId() }) )
                self.buildVms.push(new boe.char.BuildViewModel(b, self.character.Level, self.filteredInventory));
        });
        self.buildVms.each(function (bv) {
            if (!self.builds.find(function (b) { return b.BuildId() == bv.build.BuildId() }))
                self.buildVms.remove(bv);
        });
    };
    self.builds.subscribe(self.updateBuildVms);

    self.selectedBuild = ko.observable();
    self.actifBuild = ko.observable(self.selectedBuild());

    self.addBuild = function () {
        boe.ajax("GET", "/api/Char/AddBuild").done(function (build) {
            self.builds.add(build);
        });
    }
    self.deleteBuild = function (buildVm) {
        if (!confirm('Are you sure ?'))
            return;
        boe.ajax("POST", "/api/Char/DeleteBuild", buildVm.build.BuildId()).done(function () {
            if (self.selectedBuild() == buildVm)
                self.selectedBuild(self.actifBuild());

            self.builds.remove(buildVm.build);
        });

    }

    self.setActif = function (build) {
        if (self.actifBuild() == build)
            return;
        boe.ajax("POST", "/api/Char/SetActifBuild", build.build.BuildId()).done(function () {
            self.actifBuild(build);
        });
    };

    self.itemResultFormat = function (state) {
        if (!state.id)
            return state.text;
        return "<img width='25' height='25' class='item-select-icon' src='/File/GetItemImage?id=" + self.inventory.getById(state.id).Item().Loom0Id() + "'/>" + state.text;
    };
    self.itemSelectFormat = function (state) {
        if (!state.id)
            return state.text;
        return "<img width='50' height='50' class='item-select-icon' src='/File/GetItemImage?id=" + self.inventory.getById(state.id).Item().Loom0Id() + "'/>" + state.text;
    };

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#char-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.char.navLefts, boe.navLeft.char);
        $("#char").show();
    }

    var init = function () {
        self.buildVms.removeAll();
        self.builds.each(function (b) {
            self.buildVms.push(new boe.char.BuildViewModel(b, self.character.Level, self.filteredInventory));
        });
        self.selectedBuild(self.buildVms.find(function (b) { return b.build.BuildId() == self.character.BuildId(); }));
        self.actifBuild(self.selectedBuild());
    };
    init();
};