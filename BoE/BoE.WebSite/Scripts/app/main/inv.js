﻿/// <reference path="../_references.js" />
var boe = boe || {};
boe.inv = boe.inv || {};

boe.inv.navLefts = [
    boe.navLeft.char,
    boe.navLeft.inv,
    boe.navLeft.settings
];

boe.inv.invViewModel = function ( db ) {
    var self = this;

    self.db = db;

    self.gold = self.db.character.Gold;
    self.inventory = self.db.inventory;

    self.filter = {
        name: ko.observable('')
    };
    self.inventoryFiltered = ko.pureComputed(function () {
        return _.sortBy(self.inventory.filter(function (iv) {
            return !self.filter.name() || iv.Item().Name().toLowerCase().indexOf(self.filter.name().toLowerCase()) != -1
        }), function (i) {
            var repair = i.RepairPrice() || 0;
            return -i.Item().Value() - (repair * 4096);
        });
    });

    self.loomable = ko.pureComputed(function () {
        return self.inventory.filter(function (i) { return i.Item().LoomLevel() != 3 && i.Item().ItemSort() == boe.models.itemSort.normal; });
    });

    self.bag = ko.observableArray();

    self.addToBag = function (inv) {
        self.bag.push(inv);
        self.inventory.remove(inv);
    };
    self.removeFromBag = function (inv) {
        self.inventory.push(inv);
        self.bag.remove(inv);
    };

    self.bagTotal = ko.pureComputed(function () {
        var total = 0;
        self.bag.each(function (i) {
            total += i.Item().SellValue();
        });
        return Math.floor(total);
    });

    self.sell = function () {
        var ids = [];
        self.bag.each(function (i) { ids.push(i.InventoryId()); });
        self.bag.removeAll();
        boe.ajax('POST', '/api/Shop/Sell', ids).done(function (gold) {
            self.gold(gold);
        });
    };
    self.canSell = ko.pureComputed(function () {
        return self.bag().length != 0;
    });

    self.repair = function (inv) {
        boe.ajax('POST', '/api/Inv/Repair', inv.InventoryId()).done(function (gold) {
            self.gold(gold);
            inv.RepairPrice(null);
        });
    };

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#char-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.inv.navLefts, boe.navLeft.inv);
        $("#inv").show();
    };
};