﻿var boe = boe || {};
boe.settings = boe.settings || {};

boe.settings.navLefts = [
    boe.navLeft.char,
    boe.navLeft.inv,
    boe.navLeft.settings
];

boe.settings.SettingsViewModel = function (db) {
    var self = this;

    self.db = db;

    self.newName = ko.observable();

    self.ChangeCharNameModel = ko.validatedObservable({
        NewName: ko.observable().extend({
            required: { message: 'The new name is required', params: true },
            minLength: { message: 'The new name must be at least {0} character long', params: 3 }
        })
    });
    self.errorName = ko.observable();

    self.updateCharName = function () {
        boe.ajax('POST', '/api/Settings/ChangeCharName', self.ChangeCharNameModel().NewName()).done(function () {
            self.db.character.Name(self.ChangeCharNameModel().NewName());
            location.href = "#/char/main";
        }).fail(function (xhr) {
            self.errorName('This character name is already taken');
        }).always(function () {
            self.ChangeCharNameModel().NewName('');
        });
    };

    self.ChangePasswordModel = ko.validatedObservable({
        CurrentPassword: ko.observable().extend({
            required: { message: 'The current password is required', params: true },
        }),
        NewPassword: ko.observable().extend({
            required: { message: 'The new password is required', params: true },
            minLength: { message: 'The new password must be at least {0} character long', params: 6 }
        }),
        NewPasswordConfirm: ko.observable().extend({
            required: { message: 'The new password confirmation is required', params: true },
            equal: { message: 'The new password and the confirmation must match', params: function () { return self.ChangePasswordModel().NewPassword() || ''; } }
        })
    });
    self.errorPassword = ko.observable();

    self.isSure = ko.observable(false);
    self.isLevel31 = ko.pureComputed(function () {
        return self.db.character.Level() >= 31;
    });
    self.canRetire = ko.pureComputed(function () {
        return self.db.character.Level() >= 31 && self.isSure();
    });
    self.retire = function () {
        boe.ajax('post', '/api/Char/Retire', null, function (char) {
            self.db.refresh().done(function () {
                document.location.href = "#/char/main";
            });
        });
    };

    self.changePassword = function () {
        boe.ajax('POST', '/api/Settings/ChangePassword', ko.mapping.toJS(self.ChangePasswordModel())).done(function () {
            alert('Your password has been changed');
        }).fail(function (xhr) {
            self.errorPassword('Failed to change your password');
        }).always(function () {
            self.ChangePasswordModel().CurrentPassword('');
            self.ChangePasswordModel().NewPassword('');
            self.ChangePasswordModel().NewPasswordConfirm('');
        });
    };

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#char-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.settings.navLefts, boe.navLeft.settings);
        $("#settings").show();
    };
}