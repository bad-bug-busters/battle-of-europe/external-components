﻿var boe = boe || {};
boe.trade = boe.trade || {};

boe.trade.navLefts = [
    boe.navLeft.shop,
    boe.navLeft.loom,
    boe.navLeft.trade
];

boe.trade.TradeViewModel = function () {
    var self = this;

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#market-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.trade.navLefts, boe.navLeft.trade);
        $("#trade").show();
    };
}