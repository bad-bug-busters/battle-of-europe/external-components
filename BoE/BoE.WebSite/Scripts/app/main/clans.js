﻿/// <reference path="../_references.js" />
var boe = boe || {};
boe.clan = boe.clan || {};

boe.clan.ClansViewModel = function (db) {
    var self = this;

    self.db = db;

    self.character = self.db.character;
    self.clans = ko.pureComputed(function () {
        return self.db.clans.sortBy(function (cl) {
            return -cl.MembersCount();
        });
    });
    self.clan = self.db.character.Clan;

    self.canApply = ko.pureComputed(function () {
        return !self.character.ApplicationClanId() && !self.character.ClanId();
    });

    self.apply = function (clan) {
        var clanId = clan.ClanId();
        boe.ajax('POST', '/api/Clan/Apply', clanId).done(function () {
            self.character.ApplicationClanId(clanId);
        });
    };
    self.cancelAppli = function (clan) {
        boe.ajax('GET', '/api/Clan/CancelApplication').done(function () {
            self.character.ApplicationClanId(0);
        });
    };

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#clan-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.clan.navLefts, boe.navLeft.clans);
        $("#clans").show();
    };
};