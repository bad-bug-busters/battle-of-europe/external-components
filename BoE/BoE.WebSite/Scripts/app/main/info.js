﻿var boe = boe || {};
boe.info = boe.info || {};

boe.info.InfoViewModel = function ( db ) {
    var self = this;

    self.db = db;

    self.servers = self.db.servers;

    self.characters = self.db.characters;

    self.charactersCount = self.db.charactersCount

    self.show = function () {
        boe.setNavLefts([], '');
        $(".app-link").removeClass("current-link");
        $("#info-link").addClass("current-link");
        $(".view").hide();
        $("#info").show();
    };
};