﻿/// <reference path="../_references.js" />
var boe = boe || {};
boe.clan = boe.clan || {};

boe.clan.navLefts = [
    boe.navLeft.clan,
    boe.navLeft.clans
];

boe.clan.ClanViewModel = function ( db ) {
    var self = this;

    self.db = db;

    self.clans = self.db.clans;
    self.clanRoles = self.db.clanRoles;

    self.character = self.db.character;
    self.clan = self.db.character.Clan;
    self.clanRole = self.db.character.ClanRole;

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#clan-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.clan.navLefts, boe.navLeft.clan);
        $("#clan").show();
    };

    self.kickMember = function (member) {
        if (confirm('Are you sure you want to kick ' + member.Name() + ' from your clan ?')) {
            boe.ajax('POST', '/api/Clan/KickMember', member.CharacterId()).done(function () {
                self.clan().Members.remove(member);
            }).fail(function () {
            }).always(function () { })
        }
    };

    self.leave = function () {
        if (confirm('Are you sure you want to leave your clan ?')) {
            boe.ajax('POST', '/api/Clan/Leave').done(function () {
                self.character.ClanId(null);
            });
        }
    };
    self.disband = function () {
        if (confirm('Are you sure you want to disband your clan ?')) {
            var clan = self.clan();
            boe.ajax('POST', '/api/Clan/Disband', {}).done(function () {
                self.character.ClanId(null);
                self.db.clans.remove(clan);
            });
        }
    };

    self.acceptAppli = function (appli) {
        boe.ajax('POST', '/api/Clan/AcceptApplication', appli.ClanApplicationId()).done(function (member) {
            self.clan().Members.push(ko.mapping.fromJS(member, {}, new boe.models.Member(self.db)));
            self.clan().Applications.remove(appli);
        });
    };
    self.rejectAppli = function (appli) {
        boe.ajax('POST', '/api/Clan/RejectApplication', appli.ClanApplicationId()).done(function (member) {
            self.clan().Applications.remove(appli);
        });
    };
    
    self.membersOpened = ko.observable(false);
    self.toggleMembers = function () {
        self.membersOpened(!self.membersOpened());
    };

    self.applisOpened = ko.observable(false);
    self.toggleApplis = function () {
        self.applisOpened(!self.applisOpened());
    };

    self.editDescMod = ko.observable(false);
    self.toggleEditModel = function () {
        self.editDescMod(!self.editDescMod());
    };
    self.editDescMod.subscribe(function (val) {
        if (!val) {
            boe.ajax('POST', '/api/Clan/EditDescription', self.clan().Description()).done(function () {
            });
        }
    });

    self.init = function () {
    };
    self.init();
};

boe.clan.CreateClanViewModel = function (db) {
    var self = this;

    self.db = db;

    self.loading = ko.observable(false);
    
    self.clans = self.db.clans;
    self.clanRoles = self.db.clanRoles;

    self.character = self.db.character

    self.errorMessage = ko.observable();
    self.createClanModel = ko.validatedObservable({
        Name: ko.observable().extend({
            required: { message: 'The name is required', params: true },
            minLength: { message: 'The name must be at least {0} character long', params: 3 },
            maxLength: { message: 'The name cannot be more than {0} character long', params: 25 }
        }),
        Description: ko.observable().extend({
            required: { message: 'The description is required', params: true }
        })
    });

    self.createClan = function () {
        self.loading(true);
        boe.ajax('POST', '/api/Clan/CreateClan', ko.mapping.toJS(self.createClanModel, {
            ignore: ['error']
        })).done(function (data) {
            self.clanRoles.set(data.ClanRoles);
            self.clans.add(data.Clan);
            self.character.ClanId(clan.ClanId);
            location.href = '#/clan/main';
            location.reload();
        }).fail(function (xhr) {
            self.errorMessage(xhr.responseText);
        }).always(function () {
            self.loading(false);
        });
    };

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#clan-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.clan.navLefts, boe.navLeft.clan);
        $("#create-clan").show();
    };
};