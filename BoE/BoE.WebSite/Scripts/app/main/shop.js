﻿/// <reference path="../_references.js" />
var boe = boe || {};
boe.shop = boe.shop || {};

boe.shop.navLefts = [
    boe.navLeft.shop,
    boe.navLeft.loom,
    boe.navLeft.trade
];

boe.shop.button = function (text, icon, items, types, sort, height) {
    var self = this;

    self.text = text;
    self.itemsHeight = 0;
    self.icon = ko.observable(icon);

    self.types = ko.observableArray(types)
    self.sort = ko.observable(sort)

    self.selected = ko.observable(false);

    self.items = ko.pureComputed(function(){ 
        return _.filter(items(), function (it) {
            return it.ItemSort() == self.sort() && (self.types().length == 0 || self.types.any(function (ty) { return ty == it.Type(); }));
        });
    })
    self.filter = {
        armorType: ko.observable(null),
        isArmor: ko.pureComputed(function(){ return  self.types.any(function (ty) {
            return ty == boe.models.itemType.Head || ty == boe.models.itemType.Body || ty == boe.models.itemType.Hand || ty == boe.models.itemType.Foot;
        })})
    };

    self.filteredItems = ko.pureComputed(function () {
        return _.filter(self.items(), function (it) {
            if (self.filter.isArmor() && self.filter.armorType() !== null && it.ArmorType() !== self.filter.armorType())
                return false;

            return true;
        });
    });

    var init = function () {
        _.each(self.items(), function (i) {
            var h = height || boe.utils.itemTemplateHeight[boe.utils.itemTemplates[i.Type()]];
            if (h > self.itemsHeight)
                self.itemsHeight = h;
        });
    };
    init();
};

boe.shop.ShopViewModel = function ( db) {
    var self = this;

    self.db = db;

    self.gold = self.db.character ? self.db.character.Gold : ko.observable(0);
    self.items = self.db.items;
    self.inventory = self.db.inventory;
    self.looms = self.db.looms;

    self.armorTypes = ko.observableArray([]);

    self.shopItems = ko.pureComputed(function () {
        var looms = self.looms.sortBy(function(lo){
            return -lo.Level();
        });
        return _.sortBy(self.items.filter(function (it) {
            var loomType = it.LoomType();
            var loomLevel = 0;
            if (loomType) {
                loom = _.find(looms, function (lo) { return lo.Type() == loomType; });
                if (loom)
                    loomLevel = loom.Level();
            }
            return it.Shopable() && it.LoomLevel() == loomLevel;
        }), function (it) { return it.Value() + (it.Type() * 16384); });
    });

    self.buttons = {
        melees: [
            new boe.shop.button("One handed", "1h.png", self.shopItems, [ boe.models.itemType.OneH], boe.models.itemSort.normal, 275),
            new boe.shop.button("One/Two handed", "15h.png", self.shopItems, [boe.models.itemType.OneHalf], boe.models.itemSort.normal, 295),
            new boe.shop.button("Two handed", "2h.png", self.shopItems,[ boe.models.itemType.TwoH], boe.models.itemSort.normal, 285),
            new boe.shop.button("Polearm", "polearm.png", self.shopItems, [boe.models.itemType.Polearm], boe.models.itemSort.normal)
        ],
        rangeds: [
            new boe.shop.button("Crossbow", "crossbow.png", self.shopItems, [boe.models.itemType.XBow, boe.models.itemType.Bolt], boe.models.itemSort.normal),
            new boe.shop.button("Bow", "bow.png", self.shopItems, [boe.models.itemType.Bow, boe.models.itemType.Arrow], boe.models.itemSort.normal),
            new boe.shop.button("Throwing", "throwing.png", self.shopItems, [boe.models.itemType.Throwing], boe.models.itemSort.normal),
            new boe.shop.button("Musket", "musket.png", self.shopItems, [boe.models.itemType.Musket, boe.models.itemType.Pistol, boe.models.itemType.Bullet], boe.models.itemSort.normal)
        ],
        armors: [
            new boe.shop.button("Head armor", "head.png", self.shopItems, [boe.models.itemType.Head], boe.models.itemSort.normal),
            new boe.shop.button("Body armor", "body.png", self.shopItems, [boe.models.itemType.Body], boe.models.itemSort.normal),
            new boe.shop.button("Hand armor", "hand.png", self.shopItems, [boe.models.itemType.Hand], boe.models.itemSort.normal),
            new boe.shop.button("Foot armor", "foot.png", self.shopItems, [boe.models.itemType.Foot], boe.models.itemSort.normal)
        ],
        others: [
            new boe.shop.button("Shield", "shield.png", self.shopItems, [boe.models.itemType.Shield], boe.models.itemSort.normal),
            new boe.shop.button("Horse", "horse.png", self.shopItems, [boe.models.itemType.Horse], boe.models.itemSort.normal),
            new boe.shop.button("Special", "special.png", self.shopItems, null, boe.models.itemSort.special)
        ]
    };

    self.currentButton = ko.observable(self.buttons.melees[0]);

    self.bag = ko.observableArray();
    self.addToBag = function (item) {
        self.bag.push( self.items.mapData(ko.mapping.toJS(item)) );
    };
    self.removeFromBag = function (item) {
        self.bag.remove(item);
    };

    self.bagTotal = ko.pureComputed(function () {
        var total = 0;
        self.bag.each(function (i) { total += i.BuyValue(); });
        return total;
    });

    self.buy = function () {
        var ids = [];
        self.bag.each(function (i) { ids.push(i.ItemId()); });
        boe.ajax("POST", "/api/Shop/Buy", ids).done(function (data) {
            var gold = data.Gold,
                invs = data.Invs;
            self.gold(gold);
            _.each(invs, function (inv) {
                self.inventory.add(inv);
            });
            self.bag.removeAll();
        });
    };
    self.canBuy = ko.pureComputed(function () { return self.bag().length != 0 && self.gold() >= self.bagTotal(); });

    self.showItem = function (item) {
        window.open('/File/GetItemImage?id=' + item.ItemId() + '&full=true', '_blank');
    };

    self.show = function () {
        $(".app-link").removeClass("current-link");
        $("#market-link").addClass("current-link");
        $(".view").hide();
        boe.setNavLefts(boe.shop.navLefts, boe.navLeft.shop);
        $("#shop").show();
    };

    self.init = function () {
        self.armorTypes.push({
            name: 'All',
            value: null
        });
        for (var n in boe.models.armorType) {
            self.armorTypes.push({
                name: n,
                value: boe.models.armorType[n]
            });
        };
    };
    self.init();
};