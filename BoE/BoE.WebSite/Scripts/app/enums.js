﻿var boe = boe || {};

boe.models = boe.models || {};

boe.models.itemType = {
    'Unknow': 0,
    'Horse': 1,
    'OneH': 2,
    'TwoH': 3,
    'Polearm': 4,
    'Arrow': 5,
    'Bolt': 6,
    'Shield': 7,
    'Bow': 8,
    'XBow': 9,
    'Throwing': 10,
    'Goods': 11,
    'Head': 12,
    'Body': 13,
    'Foot': 14,
    'Hand': 15,
    'Pistol': 16,
    'Musket': 17,
    'Bullet': 18,
    'Animal': 19,
    'Book': 20,
    'OneHalf': 21
}

boe.models.itemSort = {
    normal: 0,
    special: 1
}

boe.models.itemGender = {
    'item': 0,
    'head': 1,
    'body': 2,
    'foot': 3,
    'hand': 4,
    'horse': 5
};

boe.models.typeToGender = {
    '1': boe.models.itemGender['horse'],
    '2': boe.models.itemGender['item'],
    '3': boe.models.itemGender['item'],
    '4': boe.models.itemGender['item'],
    '5': boe.models.itemGender['item'],
    '6': boe.models.itemGender['item'],
    '7': boe.models.itemGender['item'],
    '8': boe.models.itemGender['item'],
    '9': boe.models.itemGender['item'],
    '10': boe.models.itemGender['item'],
    '12': boe.models.itemGender['head'],
    '13': boe.models.itemGender['body'],
    '14': boe.models.itemGender['foot'],
    '15': boe.models.itemGender['hand'],
    '16': boe.models.itemGender['item'],
    '17': boe.models.itemGender['item'],
    '18': boe.models.itemGender['item'],
    '21': boe.models.itemGender['item'],
}

boe.models.damageTypes = {
    '0': 'cut',
    '1': 'pierce',
    '2': 'blunt'
};

boe.models.slots = {
    '0': "Can't seath",
    '1': "Back 1",
    '2': "Back 2",
    '3': "Back 3",
    '4': "Left stomach",
    '5': "Right stomach",
    '6': "Left hip 1",
    '7': "Left hip 2",
    '8': "Left hip 3",
    '9': "Left hip 4",
    '10': "Right hip",
    '11': "Right upper leg",
    '12': "?"
};

boe.models.armorType = {
    Light: 0,
    Medium: 1,
    Heavy: 2
};

boe.models.loomType = {
    LHead: boe.models.itemType.Head + (32 * boe.models.armorType.Light),
    MHead: boe.models.itemType.Head + (32 * boe.models.armorType.Medium),
    HHead: boe.models.itemType.Head + (32 * boe.models.armorType.Heavy),

    LBody: boe.models.itemType.Body + (32 * boe.models.armorType.Light),
    MBody: boe.models.itemType.Body + (32 * boe.models.armorType.Medium),
    HBody: boe.models.itemType.Body + (32 * boe.models.armorType.Heavy),

    LHand: boe.models.itemType.Hand + (32 * boe.models.armorType.Light),
    MHand: boe.models.itemType.Hand + (32 * boe.models.armorType.Medium),
    HHand: boe.models.itemType.Hand + (32 * boe.models.armorType.Heavy),

    LFoot: boe.models.itemType.Foot + (32 * boe.models.armorType.Light),
    MFoot: boe.models.itemType.Foot + (32 * boe.models.armorType.Medium),
    HFoot: boe.models.itemType.Foot + (32 * boe.models.armorType.Heavy),

    Hand: boe.models.itemType.Hand,
    OneH: boe.models.itemType.OneH,
    OneHalf: boe.models.itemType.OneHalf,
    TwoH: boe.models.itemType.TwoH,
    Polearm: boe.models.itemType.Polearm,
    XBow: boe.models.itemType.XBow,
    Bow: boe.models.itemType.Bow,
    Throwing: boe.models.itemType.Throwing,
    Musket: boe.models.itemType.Musket,
    Shield: boe.models.itemType.Shield,
    Horse: boe.models.itemType.Horse,
};