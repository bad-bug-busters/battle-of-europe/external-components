﻿var boe = boe || {};

boe.register = boe.register || {};

boe.register.RegisterViewModel = function () {
    var self = this;

    self.error = ko.observable();

    self.loading = ko.observable(false);

    self.RegisterModel = ko.validatedObservable({
        Username: ko.observable().extend({
            required: { message: 'The account name is required', params: true },
            minLength: { message: 'The account name must be at least {0} character long', params: 3 }
        }),
        Password: ko.observable().extend({
            required: { message: 'The password is required', params: true },
            minLength: { message: 'The password must be at least {0} character long', params: 6 }
        }),
        PasswordConfirm: ko.observable().extend({
            required: { message: 'The password confirmation is required', params: true },
            equal: { message: 'The password and the confirmation must match', params: function () { return self.RegisterModel().Password() || ''; } }
        }),
        Code: ko.observable().extend({
            required: { message: 'The code is required', params: true }
        }),
    });

    self.submit = function () {
        self.loading(true);
        boe.ajax('POST', '/api/Account/Register', ko.mapping.toJS(self.RegisterModel), $('#form-register .loading-gif')).done(function () {
            location.reload();
        }).fail(function (xhr) {
            self.error('Registration failed: ' + xhr.responseText);
        }).always(function () {
            self.loading(false);
        });;
    };

    self.show = function () {
        $(".view").hide();
        $("#register").show();
    };
}