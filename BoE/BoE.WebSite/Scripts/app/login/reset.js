﻿var boe = boe || {};
boe.reset = boe.reset || {};

boe.reset.ResetViewModel = function () {
    var self = this;


    self.loading = ko.observable(false);

    self.error = ko.observable();

    self.ResetModel = ko.validatedObservable({
        Username: ko.observable().extend({
            required: { message: 'The account name is required', params: true },
            minLength: { message: 'The account name must be at least {0} character long', params: 3 }
        }),
        Password: ko.observable().extend({
            required: { message: 'The password is required', params: true },
            minLength: { message: 'The password must be at least {0} character long', params: 6 }
        }),
        PasswordConfirm: ko.observable().extend({
            required: { message: 'The password confirmation is required', params: true },
            equal: { message: 'The password and the confirmation must match', params: function () { return self.ResetModel().Password() || ''; } }
        }),
        Code: ko.observable().extend({
            required: { message: 'The code is required', params: true }
        })
    });

    self.submit = function () {
        self.loading(true);
        boe.ajax('POST', '/api/Account/ResetPassword', ko.mapping.toJS(self.ResetModel), $('#form-reset .loading-gif')).done(function () {
            alert('Your password has been changed');
            location.href = "#/login"
        }).fail(function (xhr) {
            self.ResetModel().Username('');
            self.ResetModel().Password('');
            self.ResetModel().PasswordConfirm('');
            self.ResetModel().Code('');
            self.error('Reset failed');
        }).always(function () {
            self.loading(false);
        });;
    };

    self.show = function () {
        $(".view").hide();
        $("#reset").show();
    };
}