﻿var boe = boe || {};

boe.login = boe.login || {};

boe.login.LoginViewModel = function () {
    var self = this;

    self.loading = ko.observable(false);

    self.error = ko.observable();

    self.LoginModel = ko.validatedObservable({
        Username: ko.observable().extend({
            required: { message: 'The username is required', params: true }
        }),
        Password: ko.observable().extend({
            required: { message: 'The password is required', params: true }
        })
    });

    self.submit = function () {
        self.loading(true);
        boe.ajax('POST', '/api/Account/Login', ko.mapping.toJS(self.LoginModel), $('#form-login .loading-gif')).done(function () {
            location.reload();
        }).fail(function (xhr) {
            self.LoginModel().Username('');
            self.LoginModel().Password('');
            self.error('Login failed');
        }).always(function () {
            self.loading(false);
        });;
    };

    self.show = function () {
        $(".view").hide();
        $("#login").show();
    };
};