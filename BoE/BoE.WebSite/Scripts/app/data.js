﻿var boe = boe || {};

boe.dataContext = function () {
    var self = this;

    self.items = boe.dataSet(self, boe.models.Item, 'ItemId', {});
    self.inventory = boe.dataSet(self, boe.models.Inventory, 'InventoryId', {});
    self.looms = boe.dataSet(self, boe.models.Loom, 'LoomId', {});
    self.builds = boe.dataSet(self, boe.models.Build, 'BuildId', {});

    self.clans = boe.dataSet(self, boe.models.Clan, 'ClanId', {
        'Members': {
            create: function (options) {
                return ko.mapping.fromJS(options.data, {}, new boe.models.Member(self));
            }
        }
    });
    self.clanRoles = boe.dataSet(self, boe.models.ClanRole, 'ClanRoleId', {});

    self.servers = boe.dataSet(self, boe.models.GameServer, 'GameServerId', {});
    self.characters = boe.dataSet(self, boe.models.OtherCharacter, 'CharacterId', {});
    self.news = boe.dataSet(self, boe.models.News, 'NewsId', {});

    self.user = ko.mapping.fromJS({}, {}, new boe.models.User(self));
    self.character = ko.mapping.fromJS({}, {}, new boe.models.Character(self));

    self.charactersCount = ko.observable(0);

    self.load = function () {
        var deferred = $.Deferred();
        var reqs = ko.observable(9);
        var sucess = true;

        //Get char info
        boe.ajax('GET', '/api/Data/GetCharacter').done(function (data) {
            if (!data)
                return sucess = false;
            self.builds.set(data.Builds);
            ko.mapping.fromJS(data, self.character);
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get all items
        self.loadItems().done(function (data) {
            self.items.set(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get inventory
        boe.ajax('GET', '/api/Data/GetInventory').done(function (data) {
            self.inventory.set(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get looms
        boe.ajax('GET', '/api/Data/GetLooms').done(function (data) {
            self.looms.set(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get servers
        boe.ajax('GET', '/api/Data/GetServers').done(function (data) {
            self.servers.set(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get charlist
        boe.ajax('GET', '/api/Data/GetCharacters').done(function (data) {
            self.characters.set(data.Characters);
            self.charactersCount(data.Count)
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get factions
        boe.ajax('GET', '/api/Data/GetClans').done(function (data) {
            self.clans.set(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get clanRoles
        boe.ajax('GET', '/api/Data/GetClanRoles').done(function (data) {
            self.clanRoles.set(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        //Get news
        boe.ajax('GET', '/api/Data/GetNews').done(function (data) {
            self.news.set(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        reqs.subscribe(function (r) {
            if (r <= 0) {
                if (sucess)
                    deferred.resolve();
                else
                    deferred.reject();
            }
        });

        return deferred;
    };

    self.loadItems = function () {
        var deferred = $.Deferred();
        var storedData = $.jStorage.get(boe.storageKey.items);
        var getItems = function () {
            boe.ajax('GET', '/api/Data/GetItems').done(function (data) {
                $.jStorage.set(boe.storageKey.items, data);
                deferred.resolve(data.items);
            }).fail(function (xhr) {
                deferred.fail(xhr);
            });
        };

        if (storedData) {
            boe.ajax('GET', '/api/Data/GetItemsHash').done(function (data) {
                if (storedData.hash === data && storedData.items) {
                    deferred.resolve(storedData.items);
                } else {
                    getItems();
                }
            }).fail(function (xhr) {

            });
        } else {
            getItems();
        }
        return deferred;
    }

    self.refresh = function () {
        var deferred = $.Deferred();
        var reqs = ko.observable(8);

        boe.ajax('GET', '/api/Data/GetCharacter').done(function (data) {
            ko.mapping.fromJS(data, self.character)
            self.builds.refresh(data.Builds);
        }).always(function () {
            reqs(reqs() - 1);
        });
        boe.ajax('GET', '/api/Data/GetInventory').done(function (data) {
            self.inventory.refresh(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        boe.ajax('GET', '/api/Data/GetLooms').done(function (data) {
            self.looms.refresh(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        boe.ajax('GET', '/api/Data/GetServers').done(function (data) {
            self.servers.refresh(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        boe.ajax('GET', '/api/Data/GetCharacters').done(function (data) {
            self.characters.refresh(data.Characters);
            self.charactersCount(data.Count);
        }).always(function () {
            reqs(reqs() - 1);
        });
        boe.ajax('GET', '/api/Data/GetClans').done(function (data) {
            self.clans.refresh(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        boe.ajax('GET', '/api/Data/GetClanRoles').done(function (data) {
            self.clanRoles.refresh(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        boe.ajax('GET', '/api/Data/GetNews').done(function (data) {
            self.news.refresh(data);
        }).always(function () {
            reqs(reqs() - 1);
        });
        reqs.subscribe(function (r) {
            if (r <= 0)
                deferred.resolve();
        });
        return deferred;
    };
};

boe.dataSet = function (context, model, id, mapOption) {
    var dSet = ko.observableArray();
    dSet._context = context;
    dSet._data = {};
    dSet._model = model;
    dSet._id = id;
    dSet._mapOption = mapOption || {};
    //User the model to create the entity
    dSet.mapData = function (data) {
        return ko.mapping.fromJS(data, this._mapOption, new this._model(this._context, data));
    };
    dSet.add = function (data) {
        var self = this;
        var id = ko.utils.unwrapObservable(data[self._id]);
        var exist = self.getById(id);
        if (!exist) {
            var mappedData = self.mapData(data);
            self._data[id] = mappedData;
            this.push(self._data[id]);
            return self._data[id];
        }
        return exist;
    };
    dSet.del = function (data) {
        var self = this;
        self.remove(data);
        delete dSet._data[ko.utils.unwrapObservable(data[self._id])];
    };
    //Used to set the observableArray's value to an array
    dSet.set = function (data) {
        var self = this;
        var els = [];
        var iEls = {};
        _.each(data, function (d) {
            var el = self.mapData(d);
            var id = ko.utils.unwrapObservable(el[self._id]);
            iEls[id] = el;
            els.push(iEls[id]);
        });
        self._data = iEls;
        self.removeAll();
        self(els);
    };
    dSet.delById = function (id) {
        var self = this;
        var el = self.getById(id);
        self.remove(el);
        delete dSet._data[id];
    };
    dSet.getById = function (id) {
        var self = this;
        var idUnWrapped = ko.utils.unwrapObservable(id);
        if (!idUnWrapped)
            return null;
        //return self._data[idUnWrapped];  //Not used because it doesnt subscribe to the computed
        return self.find(function (el) {
            return ko.utils.unwrapObservable(el[self._id]) == idUnWrapped;
        });
    };
    //Update element by id, delete element not in the list, add element not in the set
    dSet.refresh = function (data) {
        var self = this;
        _.each(data, function (d) {
            var el = self.getById(d[self._id]);
            if (el)
                ko.mapping.fromJS(d, el);
            else
                self.add(d);
        });
        var toRemove = [];
        self.each(function (e) {
            if (!self.getById(e[self._id]))
                toRemove.push(e);
        });
        _.each(toRemove, function (tr) {
            self.remove(tr);
        });
    };
    return dSet;
};