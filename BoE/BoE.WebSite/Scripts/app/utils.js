﻿var boe = boe || {};
boe.utils = boe.utils || {};

boe.utils.build = {
    WPF: {
        getWPFPointsToUp: function (value) {
            return Math.max(Math.ceil(value / 50), 1);
        },
        getWPFPointsUsed: function (value) {
            var v = 0;
            for (i = 0; i < value; i++)
                v += boe.utils.build.WPF.getWPFPointsToUp(value - i);
            return v;
        },
        WPFPerWM: 50,
        getMaxWPF: function (level, wm) {
            return wm * boe.utils.build.WPF.WPFPerWM;
        }
    },
    skill: {
        getMaxSkill: function (level) {
            return level;
        },
        skillsForAtt: 2,
        maxSkill: 10
    },
    attr: {
        getMaxAtt: function (level) {
            return level + 5;
        },
        maxAttr: 30,
        minAttr: 3
    }
}

boe.utils.itemTemplateHeight = {
    'item-melee': 320,
    'item-ranged': 270,
    'item-ammo': 205,
    'item-armor': 210,
    'item-shield': 260,
    'item-horse': 250,
    'item-goods': 250,
};
boe.utils.itemTemplates = {
    '0': 'item-melee',
    '1': 'item-horse',
    '2': 'item-melee',
    '3': 'item-melee',
    '4': 'item-melee',
    '5': 'item-ammo',
    '6': 'item-ammo',
    '7': 'item-shield',
    '8': 'item-ranged',
    '9': 'item-ranged',
    '10': 'item-ranged',
    '11': 'item-goods',
    '12': 'item-armor',
    '13': 'item-armor',
    '14': 'item-armor',
    '15': 'item-armor',
    '16': 'item-ranged',
    '17': 'item-ranged',
    '18': 'item-ammo',
    '19': 'item-goods',
    '20': 'item-goods',
    '21': 'item-melee'
};

boe.utils.navLeft = function (href, src, current) {
    var self = this;
    self.href = href;
    self.src = src;
    self.current = ko.observable(current);
};

boe.utils.newsViewModel = function (dataContext) {
    var self = this;

    self.dc = dataContext;

    self.onlinePlayers = ko.pureComputed(function () {
        var count = 0;
        self.dc.servers.each(function (server) {
            count += server.PlayerCount();
        });
        return count;
    });
    self.onlineServers = ko.pureComputed(function () {
        var count = 0;
        self.dc.servers.each(function (server) {
            count++;
        });
        return count;
    });

    self.news = self.dc.news;

    self.showNews = function (news) {
        $('#news-' + news.NewsId()).bPopup();
    };
}

boe.navLeft = {
    char: new boe.utils.navLeft('#/char/main', '/Content/images/icons/char-icon.png', false),
    inv: new boe.utils.navLeft('#/char/inventory', '/Content/images/icons/inv-icon.png', false),
    settings: new boe.utils.navLeft('#/char/settings', '/Content/images/icons/settings-icon.png', false),
    shop: new boe.utils.navLeft('#/market/shop', '/Content/images/icons/shop-icon.png', false),
    loom: new boe.utils.navLeft('#/market/loom', '/Content/images/icons/loom-icon.png', false),
    trade: new boe.utils.navLeft('#/market/trade', '/Content/images/icons/trade-icon.png', false),
    clan: new boe.utils.navLeft('#/clan/main', '/Content/images/icons/clan-icon.png', false),
    clans: new boe.utils.navLeft('#/clan/clans', '/Content/images/icons/clans-icon.png', false)
};

boe.storageKey = {
    items: 'boe-items'
}