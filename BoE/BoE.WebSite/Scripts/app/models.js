﻿/// <reference path='../_references.js' />
var boe = boe || {};
boe.models = boe.models || {};

boe.models.User = function (context) {
    var self = this;
};

boe.models.Character = function (context) {
    var self = this;

    self.Kills = ko.observable(0);
    self.Deaths = ko.observable(0);

    self.Name = ko.observable('');

    self.KD = ko.pureComputed(function () {
        if (self.Deaths() === 0)
            return "∞";
        else
            return (self.Kills() / self.Deaths()).toFixed(1);
    });

    self.Gold = ko.observable();
    self.LoomPoints = ko.observable(0);
    self.Level = ko.observable(0);

    self.ApplicationClanId = ko.observable(0);

    self.ClanId = ko.observable(0);
    self.Clan = ko.pureComputed(function () {
        return context.clans.getById(self.ClanId());
    });

    self.ClanRoleId = ko.observable();
    self.ClanRole = ko.pureComputed(function () {
        return context.clanRoles.getById(self.ClanRoleId());
    });
};
boe.models.OtherCharacter = function (context) {
    var self = this;

    self.Name = ko.observable('');

    self.Level = ko.observable(0);
};

boe.models.Item = function (context, data) {
    var self = this;

    if (data.Swing != undefined)
        boe.models.Weapon.call(self, context);
    else if (data.Head != undefined)
        boe.models.Armor.call(self, context);
    else if (data.Width != undefined)
        boe.models.Shield.call(self, context);
    else if (data.Charge != undefined)
        boe.models.Horse.call(self, context);
    else
        boe.models.Good.call(self, context);

    self.ItemId = ko.observable(0);
    self.LoomLevel = ko.observable(0);

    self.Type = ko.observable(0);

    self.Gender = function () {
        return boe.models.typeToGender[self.Type()];
    };
    self.Loom0Id = function () {
        var loomLevel = self.LoomLevel();
        if (loomLevel == 0)
            return self.ItemId();
        if (loomLevel > 0) {
            var item = self;
            while (item.LoomLevel() != 0) {
                item = context.items.getById(item.PrevLoomId());
            }
            return item.ItemId();
        } else {
            var item = self;
            while (item.LoomLevel() != 0) {
                item = context.items.getById(item.NextLoomId());
            }
            return item.ItemId();
        }
    };

    self.Value = ko.observable(0);
    self.SellValue = ko.pureComputed(function () {
        return self.Value() / 2;
    });
    self.BuyValue = ko.pureComputed(function () {
        return self.Value();
    });
};
boe.models.Weapon = function (context) {
    var self = this;

    self.MissileSpeed = ko.observable(0);
};
boe.models.Armor = function (context) {
    var self = this;
};
boe.models.Shield = function (context) {
    var self = this;
};
boe.models.Horse = function (context) {
    var self = this;
};
boe.models.Good = function (context) {
    var self = this;
};

boe.models.Build = function (context) {
    var self = this;

    self.Item1Id = ko.observable(0);
    self.Item1 = ko.pureComputed(function () {
        return context.inventory.getById(self.Item1Id());
    });

    self.Item2Id = ko.observable(0);
    self.Item2 = ko.pureComputed(function () {
        return context.inventory.getById(self.Item2Id());
    });

    self.Item3Id = ko.observable(0);
    self.Item3 = ko.pureComputed(function () {
        return context.inventory.getById(self.Item3Id());
    });

    self.Item4Id = ko.observable(0);
    self.Item4 = ko.pureComputed(function () {
        return context.inventory.getById(self.Item4Id());
    });

    self.HeadId = ko.observable(0);
    self.Head = ko.pureComputed(function () {
        return context.inventory.getById(self.HeadId());
    });

    self.BodyId = ko.observable(0);
    self.Body = ko.pureComputed(function () {
        return context.inventory.getById(self.BodyId());
    });

    self.HandId = ko.observable(0);
    self.Hand = ko.pureComputed(function () {
        return context.inventory.getById(self.HandId());
    });

    self.FootId = ko.observable(0);
    self.Foot = ko.pureComputed(function () {
        return context.inventory.getById(self.FootId());
    });

    self.HorseId = ko.observable(0);
    self.Horse = ko.pureComputed(function () {
        return context.inventory.getById(self.HorseId());
    });
};

boe.models.Inventory = function (context) {
    var self = this;

    self.RepairPrice = ko.observable(null);

    self.ItemId = ko.observable(0);

    self.Item = ko.pureComputed(function () {
        return context.items.getById(self.ItemId());
    });

    self.Disabled = ko.observable(false);
}

boe.models.Loom = function (context) {
    var self = this;

    self.LoomType = ko.observable(0);
}

boe.models.GameServer = function (context) {
    var self = this;

    self.Name = ko.observable('');

    self.PlayerCount = ko.observable(0);
};

boe.models.News = function (context) {
    var self = this;

    self.Title = ko.observable();
    self.Content = ko.observable();
    self.CreationDate = ko.observable().extend({ toDateFormat: 'DD/MM/YY' });
};

boe.models.Clan = function (context) {
    var self = this;

    self.ClanId = ko.observable();

    self.Members = ko.observableArray([]);
    self.Applications = ko.observableArray([]);
};
boe.models.ClanRole = function (context) {
    var self = this;

};
boe.models.Member = function (context) {
    var self = this;

    self.ClanRoleId = ko.observable(0);
    self.ClanRole = ko.pureComputed(function () {
        return context.clanRoles.getById(self.ClanRoleId());
    });
};