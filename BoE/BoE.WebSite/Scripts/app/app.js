﻿/// <reference path='../_references.js' />
var boe = boe || {};

boe.navLefts = ko.observableArray();
boe.setNavLefts = function (navLefts, current) {
    if (!navLefts || !current) {
        boe.navLefts([]);
        return;
    }
    boe.navLefts(navLefts);
    _.each(navLefts, function (nl) { nl.current(false); });
    current.current(true);
};

boe.apps = {
    login: $.sammy('#app-login', function () {
        var self = this;

        self.loginVm = new boe.login.LoginViewModel();
        self.registerVm = new boe.register.RegisterViewModel();
        self.resetVm = new boe.reset.ResetViewModel();

        self.loadApp = function () {
            self.run();
        }

        this.bind('run', function () {
            $('.app').hide();
            $('#app-login').show();
            ko.applyBindings(self.loginVm, $('#login-dom')[0]);
            ko.applyBindings(self.registerVm, $('#register-dom')[0]);
            ko.applyBindings(self.resetVm, $('#reset-dom')[0]);
        });
        this.bind('unload', function () {
            $('#app-login').hide();
        });

        this.get('#/login', function (context) {
            self.loginVm.show();
        });
        this.get('#/instructions', function (context) {
            $('.view').hide();
            $('#instructions').show();
        });
        this.get('#/register', function (context) {
            self.registerVm.show();
        });
        this.get('#/reset', function (context) {
            $('.view').hide();
            self.resetVm.show();
        });
        this.get(/.*/, function (context) {
            context.redirect('#/login')
        });
    }),
    main: $.sammy('#app-main', function () {
        var self = this;

        self.db = new boe.dataContext();
        self.charVm = null;
        self.invVm = null;
        self.settingsVm = null;
        self.shopVm = null;
        self.tradeVm = null;
        self.infoVm = null;
        self.loomVm = null;
        self.clanVm = null;
        self.clansVm = null;
        self.createClanVm = null;

        self.loadApp = function () {
            var deferred = $.Deferred();
            ko.mapping.fromJS(boe.user, self.db.user);
            self.db.load().done(function () {
                $('.app').hide();
                $('#app-main').show();
                self.run('');
                deferred.resolve();
            });
            return deferred;
        };

        this.bind('run', function () {
            $('.app').hide();
            $('#app-loading').show();
            self.charVm = new boe.char.CharViewModel(self.db);
            self.invVm = new boe.inv.invViewModel(self.db);
            self.settingsVm = new boe.settings.SettingsViewModel(self.db);
            self.shopVm = new boe.shop.ShopViewModel(self.db);
            self.tradeVm = new boe.trade.TradeViewModel(self.db);
            self.infoVm = new boe.info.InfoViewModel(self.db);
            self.loomVm = new boe.loom.LoomViewModel(self.db);
            self.clanVm = new boe.clan.ClanViewModel(self.db);
            self.clansVm = new boe.clan.ClansViewModel(self.db);
            self.createClanVm = new boe.clan.CreateClanViewModel(self.db);
            ko.applyBindings(self.charVm, $('#char-dom')[0]);
            ko.applyBindings(self.invVm, $('#inv-dom')[0]);
            ko.applyBindings(self.settingsVm, $('#settings-dom')[0]);
            ko.applyBindings(self.shopVm, $('#shop-dom')[0]);
            ko.applyBindings(self.tradeVm, $('#trade-dom')[0]);
            ko.applyBindings(self.infoVm, $('#info-dom')[0]);
            ko.applyBindings(self.loomVm, $('#loom-dom')[0]);
            ko.applyBindings(self.clanVm, $('#clan-dom')[0]);
            ko.applyBindings(self.clansVm, $('#clans-dom')[0]);
            ko.applyBindings(self.createClanVm, $('#create-clan-dom')[0]);
            ko.applyBindings(null, $('#nav-left')[0]);
            ko.applyBindings(new boe.utils.newsViewModel(self.db), $('#news-dom')[0]);
            $('#news-dom').show();
            $('.app').hide();
            $('#app-main').show();
        });
        this.bind('unload', function () {
            $('#app-main').hide();
        });

        this.get('#/char', function (context) {
            context.redirect('#/char/main');
        });
        this.get('#/char/main', function (context) {
            self.charVm.show();
        });
        this.get('#/char/inventory', function (context) {
            self.invVm.show();
        });
        this.get('#/char/settings', function (context) {
            self.settingsVm.show();
        });

        this.get('#/market', function (context) {
            context.redirect('#/market/shop');
        });
        this.get('#/market/shop', function (context) {
            self.shopVm.show();
        });
        this.get('#/market/trade', function (context) {
            self.tradeVm.show();
        });
        this.get('#/market/loom', function (context) {
            self.loomVm.show();
        });
        this.get('#/info', function (context) {
            self.infoVm.show();
        });

        this.get('#/clan', function (context) {
            context.redirect('#/clan/main');
        });
        this.get('#/clan/main', function (context) {
            self.clanVm.show();
        });
        this.get('#/clan/clans', function (context) {
            self.clansVm.show();
        });
        this.get('#/clan/create', function (context) {
            self.createClanVm.show();
        });

        this.get(/.*/, function (context) {
            context.redirect('#/char')
        });
    }),
}

$(function () {
    boe.ajax('GET', '/api/Data/GetUser').done(function (data) {
        if (data) {
            if (data.UserId == 1266) {
                alert('To Sir_Osvar: contact GuiKa or Sebastian on steam or TW forum, we have to talk.');
            }
            boe.user = data;
            boe.apps.main.loadApp().done(function () {
                $('#app-refresh').click(function () {
                    $(this).addClass('on');
                    boe.apps.main.db.refresh().always(function () {
                        setTimeout(function () { $('#app-refresh').removeClass('on'); }, 500);
                    });
                });
                ko.applyBindings({}, $('#nav')[0]);
            });
        } else {
            boe.apps.login.loadApp();
        }
    }).fail(function (xhr) {
    });

    $('#logout').click(function () {
        boe.ajax('GET', '/api/Account/Logout').done(function () {
            document.location.reload(true);
        });
    });
});