﻿var boe = boe || {};

ko.bindingHandlers.toolTip = {
    init: function (element, valueAccessor, allBindingsAccessor /*, viewModel, bindingContext */) {
        //Nothing for now
    },
    update: function (element, valueAccessor, allBindingsAccessor /*, viewModel, bindingContext */ ) {
        var content = ko.utils.unwrapObservable(valueAccessor());
        $(element).qtip({
            content: {
                text: content
            },
            style: {
                classes: 'qtip-dark app-qtip',
                padding: 10
            },
            position: {
                at: 'bottom center',
                adjust: {
                    x: 0
                }
            },
            show: {
            },
            hide: {

            }
        });
    }
};

ko.bindingHandlers.tutoTips = {
    init: function (element, valueAccessor, allBindingsAccessor /*, viewModel, bindingContext */) {
        var value = valueAccessor()
           , text = value.text || $(element).attr('title')
           , id = value.id || $(element).attr('id')
           , title = value.title || ''
           , button = value.button || '';
        if (!$.jStorage.get(id, null)) {
            $(element).qtip({
                content: {
                    text: text,
                    title: title,
                    button: button
                },
                style: {
                    classes: 'qtip-dark',
                    width: 300,
                    height: 80
                },
                position: {
                    at: 'center',
                    adjust: {
                        x: 0
                    }
                },
                show: {
                    modal: true,
                    event: false
                },
                hide: false,
                events: {
                    hide: function (event, api) {
                        $.jStorage.set(id, true, { TTL: 86400000 * 7 }) //7 days
                    }
                }
            });
            $(element).qtip('show');
            $('#qtip-overlay').unbind('click');
        };
    },
    update: function (element, valueAccessor, allBindingsAccessor /*, viewModel, bindingContext */) {
        //Nothing
    }
};

ko.bindingHandlers.holdClick = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel/*, bindingContext*/) {
        var func = valueAccessor(),
            options = allBindingsAccessor().options || {};
        var baseTimer = options.baseTimer || 100;
        var minTimer = options.minTimer || 25;
        var decBy = options.decBy || 5;

        var startTimer = function () {
            var timeout = null;
            var time = baseTimer;
            var toDo = function () { func(ko.utils.unwrapObservable(viewModel)); }
            var stopTimer = function () {
                clearTimeout(timeout);
                delete timeout;
            };
            toDo();
            $(element).mouseup(stopTimer);
            $(element).mouseout(stopTimer);
            var toRep = function () {
                timeout = setTimeout(function () {
                    if ($(element).is(':disabled')) {
                        stopTimer();
                        return;
                    }
                    toDo();
                    time -= decBy;
                    toRep();
                }, Math.max(time, minTimer));
            }
            toRep();
        };

        $(element).mousedown(startTimer);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel/*, bindingContext*/) {
        //Noting
    }
};

ko.bindingHandlers.simpleDate = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        var date = moment(value());
        var strDate = date.format('DD/MM/YY');
        $(element).text(strDate);
    }
};

ko.bindingHandlers.displayNumber = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var round = allBindingsAccessor().round;
        if (value || value === 0) {
            if (round)
                value = value.toFixed(round);
            $(element).text(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var round = allBindingsAccessor().round;
        if (value || value === 0) {
            if (round)
                value = value.toFixed(round);
            $(element).text(value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
        }
    }
}

ko.bindingHandlers.collapse = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).data('collapse-inited', 'yes');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value) {
            var height = $(element).height();
            $(element).height(0);
            $(element).show();
            $(element).animate({
                height: height
            }, 500, function () {
                $(element).css({ height: '' });
            });
        }
        else {
            if ($(element).data('collapse-inited') === 'yes') {
                $(element).hide();
                $(element).data('collapse-inited', 'no');
            } else {
                $(element).animate({
                    height: 0
                }, 500, function () {
                    $(element).hide();
                    $(element).css({ height: '' });
                });
            }
        }
    }
};

ko.extenders.toDateFormat = function (target, format) {
    var result = ko.pureComputed({
        read: target,
        write: function (newValue) {
            var current = target()
              , momentDate = moment(newValue)
              , valueToWrite = !momentDate || !momentDate.isValid() ? '' : momentDate.format(format);
            if (valueToWrite !== current) {
                target(valueToWrite);
            }
        }
    });

    result(target());

    return result;
};

ko.extenders.numeric = function (target, precision) {
    //create a writeable computed observable to intercept writes to our observable
    var result = ko.pureComputed({
        read: target,  //always return the original observables value
        write: function (newValue) {
            var current = target(),
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = isNaN(newValue) ? 0 : parseFloat(+newValue),
                valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;

            //only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                //if the rounded value is the same, but a different value was written, force a notification for the current field
                if (newValue !== current) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    });

    //initialize with current value to make sure it is rounded appropriately
    result(target());

    //return the new computed observable
    return result;
};

ko.unapplyBindings = function ($node, remove) {
    // unbind events
    $node.find("*").each(function () {
        $(this).unbind();
    });

    // Remove KO subscriptions and references
    if (remove) {
        ko.removeNode($node[0]);
    } else {
        ko.cleanNode($node[0]);
    }
};

boe.ajax = function (type, url, data, gif) {
    var deferred = $.Deferred();
    if (gif)
        $(gif).show();
    $.ajax(url, {
        type: type,
        contentType: 'application/json; charset=utf-8',
        accepts: 'application/json',
        dataType: 'json',
        data: type.toLowerCase() === 'get' ? data : JSON.stringify(data),
        success: function (res) {
            deferred.resolve(res);
        },
        error: function (xhr) {
            if (xhr.status >= 200 && xhr.status < 300)
                deferred.resolve();
            else
                deferred.reject(xhr);
        },
        complete: function () {
            if (gif)
                $(gif).hide();
        }
    });
    return deferred;
};