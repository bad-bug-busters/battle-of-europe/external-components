﻿using BoE.Formatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BoE.WebSite
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}"
           );

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithAction",
                routeTemplate: "api/{controller}/{action}",
                defaults: null,
                constraints: new { action = @"^[a-zA-Z]+$" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithActionAndId",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: null,
                constraints: new { action = @"^[a-zA-Z]+$" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithId",
                routeTemplate: "api/{controller}/{id}",
                defaults: null,
                constraints: new { id = @"^(\w{8}-\w{4}-\w{4}-\w{4}-\w{12})|[a-zA-Z0-9\+= ]+$" }
            );

            config.Formatters.Insert(0, new WSEFormatter());
        }
    }
}
