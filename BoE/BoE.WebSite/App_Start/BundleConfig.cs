﻿using System.Web;
using System.Web.Optimization;

namespace StrategusEnhancer.WebSite
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.qtip.js",
                        "~/Scripts/select2.js",
                        "~/Scripts/jquery.bpopup-0.9.4.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/jstorage.js"));

            bundles.Add(new ScriptBundle("~/bundles/sammy").Include(
                       "~/Scripts/sammy-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-{version}.js",
                        "~/Scripts/knockout.mapping-latest.js",
                        "~/Scripts/knockout.validation.js",
                        "~/Scripts/underscore.js",
                        "~/Scripts/underscore-ko-1.2.2.js",
                        "~/Scripts/select2-knockout.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/app/lib.js",
                "~/Scripts/app/utils.js",
                "~/Scripts/app/enums.js",
                "~/Scripts/app/models.js",
                "~/Scripts/app/data.js",
                "~/Scripts/app/main/*.js",
                "~/Scripts/app/login/*.js",
                "~/Scripts/app/app.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/admin").Include("~/Areas/Admin/Scripts/utils.js"));

            bundles.Add(new StyleBundle("~/Content/main").Include(
                "~/Content/common.css", 
                "~/Content/libs/jquery.qtip.css", 
                "~/Content/libs/select2.css", 
                "~/Content/main.css"));
            bundles.Add(new StyleBundle("~/Content/admin").Include(
                "~/Content/common.css", 
                "~/Content/libs/jquery.qtip.css", 
                "~/Content/libs/select2.css", 
                "~/Content/admin.css"));
        }
    }
}

//@section scripts {
//    @Scripts.Render("~/bundles/jqueryui")
//    @Styles.Render("~/Content/themes/base/css")

//    <script type="text/javascript">

//    </script>
//}