﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class ApplicationModel
    {
        public ApplicationModel() { }

        public ApplicationModel(ClanApplication clanApplication)
        {
            ClanApplicationId = clanApplication.ClanApplicationId;
            CharacterName = clanApplication.Character.Name;
            CreationDate = clanApplication.CreationDate;
        }

        public int ClanApplicationId { get; set; }

        public string CharacterName { get; set; }

        public DateTime CreationDate { get; set; }
    }
}