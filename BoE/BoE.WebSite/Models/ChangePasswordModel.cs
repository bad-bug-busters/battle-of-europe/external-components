﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class ChangePasswordModel
    {
        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }
    }
}