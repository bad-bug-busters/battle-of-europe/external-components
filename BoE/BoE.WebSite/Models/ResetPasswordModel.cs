﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class ResetPasswordModel
    {
        public string Username { get; set; }

        public string Code { get; set; }

        public string Password { get; set; }
    }
}