﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class CharacterModel
    {
        public CharacterModel() { }

        public CharacterModel(Character character) 
        {
            CharacterId = character.CharacterId;
            Name = character.Name;
            Experience = character.Experience;
            Generation = character.Generation;
            CreationDate = character.CreationDate;
        }

        public int CharacterId { get; set; }

        public string Name { get; set; }

        public int Experience { get; set; }

        public int Generation { get; set; }

        public DateTime CreationDate { get; set; }

        public int Level
        {
            get { return Character.GetLevel(Experience); }
        }
    }
}