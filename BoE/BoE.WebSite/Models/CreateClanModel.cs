﻿using BoE.Contexts;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class CreateClanModel
    {
        public CreateClanModel() { }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public Clan ToClan(BoEContext db)
        {
            Clan clan = db.Clans.Create();
            clan.Name = Name;
            clan.Description = Description;
            clan.ClanRoles = new List<ClanRole>();

            ClanRole leader = db.ClanRoles.Create();
            leader.Name = "Leader";
            leader.Lead = true;
            leader.Recruit = true;

            ClanRole member = db.ClanRoles.Create();
            member.Name = "Member";
            member.Lead = false;
            member.Recruit = false;

            clan.ClanRoles.Add(leader);
            clan.ClanRoles.Add(member);

            return clan;
        }
    }
}