﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class UpKeepCalculationModel
    {
        public int ItemId { get; set; } 

        public int GameId { get; set; }

        public int HitPoint { get; set; }

        public int Ammo { get; set; }

        public int ItemValue { get; set; }

        public ItemType Type { get; set; }
    }
}