﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class ClanModel
    {
        public ClanModel() 
        { 
            Members = new List<MemberModel>();
            Applications = new List<ApplicationModel>();
        }

        public ClanModel(Clan clan) 
        {
            ClanId = clan.ClanId;
            Name = clan.Name;
            Description = clan.Description;
            CreationDate = clan.CreationDate;
            MembersCount = clan.ClanApplications.Count;
            Members = clan.Characters.Select(c => new MemberModel(c));
            Applications = clan.ClanApplications.Select(ca => new ApplicationModel(ca));
        }

        public int ClanId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int MembersCount { get; set; }

        public DateTime CreationDate { get; set; }

        public IEnumerable<MemberModel> Members { get; set; }

        public IEnumerable<ApplicationModel> Applications { get; set; }
    }
}