﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class UserUsagesModel
    {
        public User User { get; set; }

        public RoundResult RoundResult { get; set; }

        public int RespawnCount { get; set; }

        public Dictionary<int, int> Usages { get; set; }
    }
}