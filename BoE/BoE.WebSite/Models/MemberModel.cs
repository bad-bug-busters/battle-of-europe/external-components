﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class MemberModel
    {
        public MemberModel(Character character) 
        {
            CharacterId = character.CharacterId;
            Name = character.Name;
            Experience = character.Experience;
            Generation = character.Generation;
            CreationDate = character.CreationDate;
            ClanRoleId = character.ClanRoleId.HasValue ? character.ClanRoleId.Value : 0;
        }

        public int CharacterId { get; set; }

        public string Name { get; set; }

        public int Experience { get; set; }

        public int Generation { get; set; }

        public int ClanRoleId { get; set; }

        public DateTime CreationDate { get; set; }

        public int Level
        {
            get { return Character.GetLevel(Experience); }
        }
    }
}