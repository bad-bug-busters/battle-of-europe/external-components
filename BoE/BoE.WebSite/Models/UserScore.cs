﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.WebSite.Models
{
    public class UserScore
    {
        public User User { get; set; }

        public int Score { get; set; }

        public int Kills { get; set; }

        public int Deaths { get; set; }

        public int Tick { get; set; }
    }
}