﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Formatters
{
    public class WSEModel
    {
        public WSEModel(HttpStatusCode status, int reqId)
        {
            HttpStatus = status;
            Content = null;
            ReqId = reqId;
        }
        public WSEModel(HttpStatusCode status, int reqId, object content)
        {
            HttpStatus = status;
            Content = content;
            ReqId = reqId;
        }

        public HttpStatusCode HttpStatus { get; set; }

        public object Content { get; set; }

        public int ReqId { get; set; }
    }
}
