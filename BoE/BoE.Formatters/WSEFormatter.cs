﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using BoE.Models;
using System.Net.Http;
using System.Collections;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;

namespace BoE.Formatters
{
    public class WSEFormatter : BufferedMediaTypeFormatter
    {
        private Dictionary<string, Func<object, string>> getFormatter
        {
            get
            {
                return new Dictionary<string, Func<object, string>>
                {
                    {typeof(Character).FullName, GetCharWSE}
                };
            }
        }

        private string GetAssembly(Type type)
        {
            if (type.FullName.Contains("Dynamic"))
                return type.BaseType.FullName;
            else
                return type.FullName;
        }

        public WSEFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override bool CanWriteType(Type type)
        {
            return type == typeof(WSEModel);
        }

        public override bool CanReadType(Type type)
        {
            return false;
        }

        public override void WriteToStream(Type type, object value, Stream stream, HttpContent content)
        {
            using (var writer = new StreamWriter(stream))
            {
                WSEModel model = (WSEModel)value;
                writer.Write((int)model.HttpStatus + "|" + model.ReqId + (model.Content != null ? "|" : ""));
                if (model.Content != null)
                {
                    string assembly = GetAssembly(model.Content.GetType());

                    if (getFormatter.ContainsKey(assembly))
                    {
                        writer.Write(getFormatter[assembly](model.Content));
                    }
                    else
                    {
                        if (IsPrimitive(model.Content))
                            writer.Write(model.Content);
                        else
                            WriteObjectWSE(writer, model.Content);
                    }
                }
            }
            stream.Close();
        }

        //Write recurcively the properties of the object
        private void WriteObjectWSE(StreamWriter writer, object value)
        {
            PropertyInfo[] props = value.GetType().GetProperties();
            for (int i = 0; i < props.Length; i++)
            {
                var propVal = props[i].GetValue(value);
                if (propVal == null)
                    continue;

                string assembly = GetAssembly(propVal.GetType());
                if (getFormatter.ContainsKey(assembly))
                {
                    writer.Write(getFormatter[assembly](propVal));
                }
                else
                {
                    if (IsPrimitive(propVal))
                        writer.Write(propVal);
                    else
                        WriteObjectWSE(writer, propVal);
                }
                if (i < props.Length - 1)
                    writer.Write("|");
            }
        }

        private bool IsPrimitive(object obj)
        {
            Type t = obj.GetType();
            return (t.IsPrimitive || t == typeof(Decimal) || t == typeof(String) || t == typeof(DateTime));
        }

        //Character
        const int noItem = -1;
        const int noClan = -1;
        private string GetCharWSE(object obj)
        {
            Character chara = (Character)obj;
            return GetCharacterStats(chara);
        }

        private string GetCharacterStats(Character chara)
        {
            StringBuilder sb = new StringBuilder();
            int clanId = chara.ClanId.HasValue && chara.Clan.GameId.HasValue ? chara.Clan.GameId.Value : noClan;
            sb.Append(clanId);
            sb.Append("|" + chara.Generation);
            sb.Append("|" + chara.Experience);
            sb.Append("|" + (int)chara.Gold);
            sb.Append("|" + chara.Build.BuildId);
            sb.Append("|" + chara.Build.Strength);
            sb.Append("|" + chara.Build.Agility);
            sb.Append("|" + chara.Build.HorseArchery);
            sb.Append("|" + chara.Build.Riding);
            sb.Append("|" + chara.Build.Athletics);
            sb.Append("|" + chara.Build.WeaponMaster);
            sb.Append("|" + chara.Build.Resistance);
            sb.Append("|" + chara.Build.PowerThrow);
            sb.Append("|" + chara.Build.PowerStrike);
            sb.Append("|" + chara.Build.PowerPull);
            sb.Append("|" + chara.Build.PowerReload);
            sb.Append("|" + chara.Build.OffHand);
            sb.Append("|" + (chara.Build.OneHanded + 100));
            sb.Append("|" + (chara.Build.TwoHanded + 100));
            sb.Append("|" + (chara.Build.Polearm + 100));
            sb.Append("|" + (chara.Build.Archery + 100));
            sb.Append("|" + (chara.Build.Crossbow + 100));
            sb.Append("|" + (chara.Build.Throwing + 100));
            sb.Append("|" + (chara.Build.Firearm + 100));
            //Items
            sb.Append("|" + (chara.Build.Item1Id.HasValue && chara.Build.Item1.RepairPrice == null ? chara.Build.Item1.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.Item2Id.HasValue && chara.Build.Item2.RepairPrice == null ? chara.Build.Item2.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.Item3Id.HasValue && chara.Build.Item3.RepairPrice == null ? chara.Build.Item3.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.Item4Id.HasValue && chara.Build.Item4.RepairPrice == null ? chara.Build.Item4.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.HeadId.HasValue && chara.Build.Head.RepairPrice == null ? chara.Build.Head.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.BodyId.HasValue && chara.Build.Body.RepairPrice == null ? chara.Build.Body.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.FootId.HasValue && chara.Build.Foot.RepairPrice == null ? chara.Build.Foot.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.HandId.HasValue && chara.Build.Hand.RepairPrice == null ? chara.Build.Hand.Item.GameId : noItem));
            sb.Append("|" + (chara.Build.HorseId.HasValue && chara.Build.Horse.RepairPrice == null ? chara.Build.Horse.Item.GameId : noItem));

            //Other items
            IEnumerable<Inventory> uniqueInv = chara.Inventories != null ? chara.Inventories.Where(i => i.RepairPrice == null && !i.Disabled).GroupBy(i => i.ItemId).Select(i => i.First()) : new List<Inventory>();
            List<Inventory> invs = uniqueInv.Where(i => i.InventoryId != chara.Build.Item1Id && i.InventoryId != chara.Build.Item2Id &&
                i.InventoryId != chara.Build.Item3Id && i.InventoryId != chara.Build.Item4Id && i.InventoryId != chara.Build.HeadId &&
                i.InventoryId != chara.Build.BodyId && i.InventoryId != chara.Build.FootId && i.InventoryId != chara.Build.HandId &&
                i.InventoryId != chara.Build.HorseId).ToList();

            foreach (Inventory inv in invs)
                sb.Append("|" + inv.Item.GameId);

            return sb.ToString();
        }
    }
}
