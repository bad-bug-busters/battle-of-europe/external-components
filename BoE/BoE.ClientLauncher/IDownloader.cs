﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ClientLauncher
{
    public interface IDownloader : IDisposable
    {
        string Source { get; }
        void DownloadFile(string fileName, string localFile);
        Stream GetDownloadStream(string fileName);
        string[] GetFiles(string directory);
        long GetFileSize(string fileName);
        void Open();
        void Close();
    }
}
