﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace BoE.ClientLauncher
{
    public static class BoEFilesUtils
    {
        const string warbandName = "Mount&Blade Warband";
        const string warbandSteamName = "MountBlade Warband";
        const string BoEName = "Battle of Europe";
        const string WsePath = @"WSE\WSELoader.exe";
        const string WarbandLauncherPath = "mb_warband.exe";

        public static float GetModuleVersion(string moduleIni)
        {
            string versionLine = moduleIni.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None)
                .First(l => l.StartsWith("module_version"));
            return float.Parse(versionLine.Replace("module_version = ", "").Substring(0, 4));
        }

        public static string GetWarbandDirectory()
        {

            string warbandPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\" + warbandName, "", null);
            string steamPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Valve\Steam", "InstallPath", null);

            if (!string.IsNullOrEmpty(steamPath))
            {
                string steamWarband = Path.Combine(steamPath, @"SteamApps\common\" + warbandSteamName);
                if (Directory.Exists(steamWarband))
                    return steamWarband;
            }
            if (!string.IsNullOrEmpty(warbandPath))
            {
                if (Directory.Exists(warbandPath))
                    return warbandPath;
            }
            return null;
        }

        public static string GetModuleDirectory(string warbandDirectory, bool createIfNotExcist = false)
        {
            string modPath = Path.Combine(warbandDirectory, @"Modules\" + BoEName);
            if (Directory.Exists(modPath))
                return modPath;
            else if (createIfNotExcist)
            {
                Directory.CreateDirectory(modPath);
                return modPath;
            }
            return null;
        }

        public static float? GetWarbandVersion()
        {
            return (float?)Registry.LocalMachine.GetValue(@"Software\Mount&Blade Warband\Version", null, RegistryValueOptions.None);
        }

        public static Process StartBoE(string warbandPath, string modulePath)
        {
            string param = string.Format("-p \"{2}\" -m \"{1}\" \"\" \"\" \"\" \"Play '{3}'\"",
                warbandPath, BoEName, Path.Combine(warbandPath, WarbandLauncherPath), BoEName);
            return Process.Start(Path.Combine(modulePath, WsePath), param);
        }
    }
}
