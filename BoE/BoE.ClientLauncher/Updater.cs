﻿using BoE.ClientLauncher.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace BoE.ClientLauncher
{
    public class Updater
    {
        public Updater(string ftpUrl, string moduleDirectory) 
        {
            FtpUrl = ftpUrl;
            ModuleDirectory = moduleDirectory;
            Queue = new ConcurrentQueue<UpdateAction>();
        }

        public string FtpUrl { get; set; }

        public string ModuleDirectory { get; set; }

        public bool Running { get; set; }

        public ConcurrentQueue<UpdateAction> Queue { get; set; }
        public int activeThreads;
        public List<Thread> Threads { get; set; }

        public void Start(int threadCount)
        {
            Threads = new List<Thread>();
            for (int i = 0; i < threadCount; i++)
                Threads.Add(new Thread(RunUpdaterQueue) { IsBackground = true });

            Running = true;
            activeThreads = threadCount;

            foreach (Thread thread in Threads)
                thread.Start();
        }
        public void Pause()
        {
        }

        private void RunUpdaterQueue()
        {
            IDownloader interDownloader = new FtpDownloader(FtpUrl);
            while (true)
            {
                UpdateAction action;

                if (Queue.TryDequeue(out action))
                {
                    try
                    {
                        string fullPath = Path.Combine(ModuleDirectory, action.Arg != null ? action.Arg.Replace("\\Source\\", "") : "");
                        switch (action.Type)
                        {
                            case UpdateActionType.AddOrUpdateFile:
                                interDownloader.DownloadFile(action.Arg, fullPath);
                                break;
                            case UpdateActionType.DeleteFile:
                                File.Delete(Path.Combine(ModuleDirectory, action.Arg));
                                break;
                        }
                        ActionRan(this, new UpdaterEventArgs(action));
                    }
                    catch (Exception e)
                    {
                        ErrorOccured(this,  new UpdaterEventArgs(action, e.GetBaseException().Message));
                        Queue.Enqueue(action);
                    }
                }
                else if (Running)
                {
                    if (Interlocked.Decrement(ref activeThreads) <= 0)
                    {
                        Finished(this, null);
                    }
                    interDownloader.Dispose();
                    return;
                }
            }
        }
        public void EnQueuAction(UpdateAction action)
        {
            Queue.Enqueue(action);
        }

        public event EventHandler<UpdaterEventArgs> ActionRan;
        public event EventHandler<UpdaterEventArgs> ErrorOccured;
        public event EventHandler<UpdaterEventArgs> Finished;
    }

    public class UpdaterEventArgs : EventArgs
    {
        public UpdaterEventArgs() { }

        public UpdaterEventArgs(UpdateAction action, string message)
        {
            Action = action;
            Message = message;
        }

        public UpdaterEventArgs(string message)
        {
            Message = message;
        }

        public UpdaterEventArgs(UpdateAction action)
        {
            Action = action;
        }

        public UpdateAction Action { get; set; }

        public string Message { get; set; }
    }
}
