﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ClientLauncher.Models
{
    public class UpdateAction
    {
        public UpdateAction() { }
        public UpdateAction(UpdateActionType type, string arg, long value) 
        {
            Arg = arg;
            Type = type;
            Value = value;
        }

        public string Arg { get; set; }

        public long Value { get; set; }

        public UpdateActionType Type { get; set; }
    }

    public enum UpdateActionType
    {
        AddOrUpdateFile,
        DeleteFile,
        Nothing
    }
}
