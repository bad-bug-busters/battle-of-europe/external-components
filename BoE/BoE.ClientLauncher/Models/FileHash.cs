﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ClientLauncher.Models
{
    public class FileHash
    {
        public FileHash() { }

        public FileHash(string path)
        {
            Path = path;
            Hash = FileHash.GetHash(File.ReadAllBytes(path));
            Size = new FileInfo(path).Length;
        }
        public FileHash(string path, string hash, long size)
        {
            Path = path;
            Hash = hash;
            Size = size;
        }

        public string Path { get; set; }

        public string Hash { get; set; }

        public long Size { get; set; }

        public long KbSize
        {
            get { return (long)((decimal)Size / 1000m); }
        }

        public static string GetHash(byte[] data)
        {
            using (var md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(data)).Replace("-", "").ToLower(); ;
            }
        }
    }
}
