﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ClientLauncher.Models
{
    public class AsyncInfoModel
    {
        public AsyncInfoModel(byte[] bytes, Stream stream)
        {
            Bytes = bytes;
            Stream = stream;
        }

        public byte[] Bytes { get; set; }

        public Stream Stream { get; set; }
    }
}
