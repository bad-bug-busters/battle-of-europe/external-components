﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ClientLauncher.Models
{
    public class LogMessage
    {
        public LogMessage(string message, LogType type = LogType.normal)
        {
            Message = message;
            CreationDate = DateTime.Now;
            Type = type;
        }

        public string Message { get; set; }

        public DateTime CreationDate { get; set; }

        public LogType Type { get; set; }

        public string FontColor
        {
            get {
                switch (Type)
                {
                    case LogType.good:
                        return "Green";
                    case LogType.normal:
                        return "Blue";
                    case LogType.bad:
                        return "Red";
                }
                return "black";
            }
        }
    }

    public enum LogType
    {
        good,
        normal,
        bad
    }
}
