using BoE.ClientLauncher.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;
using System.Windows.Threading;
using System.Diagnostics;
using Ionic.Zip;
using form = System.Windows.Forms;
using System.Text.RegularExpressions;

namespace BoE.ClientLauncher.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        const string FTP_URL = @"ftp://ftp.battleofeurope.net";
        const string DATA_FILE = "files_hash.xml";
        const string FULL_MOD_FILE = "boe_full_mod";
        const int ACTION_SIZE_VALUE = 1000;
        const int VERSION = 117;

        private string[] IgnorePaths = new string[] { 
            @".*WSE\\wse_log.txt",
            @".*WSE\\wse_crash_log.txt"
        };

        private Updater updater;
        IDownloader downloader;
        private string warbandDirectory;
        private bool didFullUpdate;

        private int updaterWorking;
        private long actionDownloadsToRun;
        private long actionDownloadsRan;

        public string WindowTitle
        {
            get { return "BoE Launcher v" + ((float)VERSION / 100); }
        }

        private int actionToRun;
        public int ActionToRun
        {
            get { return actionToRun; }
            set { actionToRun = value; RaisePropertyChanged("ActionToRun"); }
        }
        private int actionRan;
        public int ActionRan
        {
            get { return actionRan; }
            set { actionRan = value; RaisePropertyChanged("ActionRan"); }
        }

        private float clientVersion;
        public float ClientVersion
        {
            get { return clientVersion; }
            set { clientVersion = value; RaisePropertyChanged("ClientVersion"); RaisePropertyChanged("ClientVersionString"); }
        }
        public string ClientVersionString
        {
            get { return ClientVersion == -1 ? "n/a" : "0." + ClientVersion; }
        }

        private float serverVersion;
        public float ServerVersion
        {
            get { return serverVersion; }
            set
            {
                serverVersion = value;
                RaisePropertyChanged("ServerVersion");
                RaisePropertyChanged("ServerVersionString");
            }
        }
        public string ServerVersionString
        {
            get { return "0." + ServerVersion; }
        }

        private string moduleDirectory;
        public string ModuleDirectory
        {
            get { return moduleDirectory; }
            set
            {
                moduleDirectory = value;
                RaisePropertyChanged("ModuleDirectory");
            }
        }

        private bool directoryNotFound;
        public bool DirectoryNotFound
        {
            get { return directoryNotFound; }
            set
            {
                directoryNotFound = value;
                RaisePropertyChanged("DirectoryNotFound");
            }
        }

        private bool updating;
        public bool Updating
        {
            get { return updating; }
            set { updating = value; RaisePropertyChanged("Updating"); }
        }

        private bool forceUpdate;
        public bool ForceUpdate
        {
            get { return forceUpdate; }
            set { forceUpdate = value; RaisePropertyChanged("ForceUpdate"); }
        }

        private bool hasNetworkError;
        public bool HasNetworkError
        {
            get { return hasNetworkError; }
            set { hasNetworkError = value; RaisePropertyChanged("HasNetworkError"); }
        }

        private int updateProgress;
        public int UpdateProgress
        {
            get { return updateProgress; }
            set { updateProgress = value; RaisePropertyChanged("UpdateProgress"); }
        }

        private FastObservableCollection<LogMessage> logMessages;
        public FastObservableCollection<LogMessage> LogMessages
        {
            get { return logMessages; }
            set { logMessages = value; RaisePropertyChanged("LogMessages"); }
        }

        public RelayCommand LaunchBoECommand { get; set; }
        public RelayCommand UpdateBoECommand { get; set; }

        public MainViewModel()
        {
            UpdateProgress = 0;
            DirectoryNotFound = false;
            didFullUpdate = false;
            LogMessages = new FastObservableCollection<LogMessage>();
            downloader = new FtpDownloader(FTP_URL);
            LaunchBoECommand = new RelayCommand(LaunchBoE, CanLaunchBoE);
            UpdateBoECommand = new RelayCommand(UpdateBoE, CanUpdateBoE);

            ActionToRun = 3;
            ActionRan = 0;

            CheckUpdate();
            SetModuleDirectory();
        }

        private bool CanLaunchBoE()
        {
            return ((ClientVersion != 0 && ServerVersion != 0 && ClientVersion >= ServerVersion) || HasNetworkError) && !Updating;
        }
        private void LaunchBoE()
        {
            try
            {
                //AddLog("Startin WSE at " + Path.Combine(ModuleDirectory, @"WSE\WSELoader.exe"));
                Process pss = BoEFilesUtils.StartBoE(warbandDirectory, ModuleDirectory);
                App.Current.Shutdown();
            }
            catch (Exception e)
            {
                RunInUI(() => AddLog("Failed to run WSELoader.exe", LogType.bad));
                RunInUI(() => AddLog(e.GetBaseException().Message, LogType.bad));
            }
        }
        private bool CanUpdateBoE()
        {
            return ((ClientVersion != 0 && ServerVersion != 0 && ClientVersion != ServerVersion) || ForceUpdate) && !Updating;
        }
        private void UpdateBoE()
        {
            Updating = true;
            UpdateProgress = 0;
            RunInUI(UpdateBoECommand.RaiseCanExecuteChanged);
            updater = new Updater(FTP_URL, ModuleDirectory);

            Task.Factory.StartNew(InitializeDownloader);
        }

        private void InitializeDownloader()
        {
            RunInUI(() => AddLog("Starting update process ..."));

            XDocument xml = null;
            string fullModeName = null;
            long fullModeSize = 0;
            try
            {
                xml = XDocument.Load(downloader.GetDownloadStream(DATA_FILE));
                fullModeName = GetFullMod(downloader.GetFiles("").Where(f => f.StartsWith(FULL_MOD_FILE)));
                fullModeSize = !string.IsNullOrEmpty(fullModeName) ? downloader.GetFileSize(fullModeName) : 0;
            }
            catch (Exception e)
            {
                RunInUI(() => MessageBox.Show("Error trying to retreive files from server: " + e.GetBaseException().Message));
            }

            RunInUI(() => AddLog("Information file downloaded"));
            RunInUI(() => AddLog("Generating update list ..."));
            IEnumerable<FileHash> fileHashs = xml.Element("FilesHash").Elements()
                .Select(xe => new FileHash(xe.Attribute("Path").Value, xe.Attribute("Hash").Value, long.Parse(xe.Attribute("Size").Value)));

            long totalSize = 0;
            foreach (FileHash fileHash in fileHashs)
            {
                string relativPath = fileHash.Path.Replace("\\Source\\", "");
                string fullPath = Path.Combine(ModuleDirectory, relativPath);
                if (!File.Exists(fullPath))
                {
                    IEnumerable<string> directories = FileUtils.GetDirectoriesPathToFile(relativPath);
                    foreach (string dir in directories)
                        if (!Directory.Exists(Path.Combine(ModuleDirectory, dir)))
                        {
                            Directory.CreateDirectory(Path.Combine(ModuleDirectory, dir));
                            RunInUI(() => AddLog(dir + " directory created"));
                        }
                    updater.EnQueuAction(new UpdateAction(UpdateActionType.AddOrUpdateFile, fileHash.Path, fileHash.KbSize + ACTION_SIZE_VALUE));
                    totalSize += fileHash.KbSize;
                }
                else if (FileHash.GetHash(File.ReadAllBytes(fullPath)) != fileHash.Hash)
                {
                    updater.EnQueuAction(new UpdateAction(UpdateActionType.AddOrUpdateFile, fileHash.Path, fileHash.KbSize + ACTION_SIZE_VALUE));
                    totalSize += fileHash.KbSize;
                }
            }
            RunInUI(() => AddLog("Update list completed"));
            if ((fullModeSize / 1000) < totalSize && !didFullUpdate && !string.IsNullOrEmpty(fullModeName))
            {
                RunInUI(() => AddLog("Running installer with " + Math.Round((fullModeSize / 1000000d), 1) + " mb to download ..."));
                FullUpdate(fullModeSize, fullModeName);
            }
            else
            {
                foreach (string filePath in Directory.GetFiles(ModuleDirectory, "*", SearchOption.AllDirectories)
                    .Where(p => !IgnorePaths.Any(ip => Regex.Match(p, ip).Success)).Select(fp => fp.Replace(ModuleDirectory, "")))
                {
                    string relativPath = filePath.Substring(1, filePath.Length - 1);
                    if (!fileHashs.Any(fh => fh.Path.Replace("\\Source\\", "").ToLower() == relativPath.ToLower()))
                        updater.EnQueuAction(new UpdateAction(UpdateActionType.DeleteFile, relativPath, ACTION_SIZE_VALUE));
                }
                RunInUI(() => AddLog(Math.Round((totalSize / 1000d), 1) + " mb to download"));
                RunInUI(() => AddLog("Running updater with " + updater.Queue.Count + " actions to run ..."));
                StartUpdater(updater.Queue.Count, totalSize);
            }
        }
        private string GetFullMod(IEnumerable<string> fullMods)
        {
            if (fullMods.Count() == 0)
                return null;
            return fullMods.OrderByDescending(fm => GetFullModVersion(fm)).First();
        }
        private int GetFullModVersion(string fullMod)
        {
            int version = 0;
            int.TryParse(Path.GetFileNameWithoutExtension(fullMod).Replace(FULL_MOD_FILE + "_", ""), out version);
            return version;
        }

        private void FullUpdate(long fullSize, string fullMod)
        {
            int extractAction = (int)(fullSize / 10000);
            ActionToRun = (int)(fullSize / 1000);
            ActionRan = 0;

            int toProgress = ActionToRun + extractAction;
            string downloadPath = Path.Combine(ModuleDirectory, fullMod);
            if (File.Exists(downloadPath))
                File.Delete(downloadPath);
            BufferedStream reader = new BufferedStream(downloader.GetDownloadStream(fullMod));
            FileStream writer = new FileStream(downloadPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            try
            {
                byte[] buffer = new byte[4096];
                int intSt = reader.Read(buffer, 0, buffer.Length);
                while (intSt > 0)
                {
                    writer.Write(buffer, 0, intSt);
                    intSt = reader.Read(buffer, 0, buffer.Length);
                    ActionRan = (int)(writer.Length / 1000);
                    UpdateProgress = (int)(((decimal)ActionRan / (decimal)toProgress) * 100);
                }
            }
            catch (Exception e)
            {
                RunInUI(() => MessageBox.Show("Error while downloading boe: " + e.GetBaseException().Message));
            }
            reader.Dispose();
            reader.Close();
            writer.Dispose();
            writer.Close();
            RunInUI(() => AddLog("Extracting files ...."));

            foreach (string filePath in Directory.GetFiles(ModuleDirectory, "*", SearchOption.AllDirectories))
                if (filePath != downloadPath)
                    File.Delete(filePath);
            foreach (string dirPath in Directory.GetDirectories(ModuleDirectory, "*", SearchOption.AllDirectories))
                if (Directory.Exists(dirPath))
                    Directory.Delete(dirPath, true);

            try
            {
                using (ZipFile zip = new ZipFile(downloadPath))
                    zip.ExtractAll(ModuleDirectory, ExtractExistingFileAction.OverwriteSilently);
                UpdateProgress = 100;
            }
            catch (Exception e)
            {
                RunInUI(() => MessageBox.Show("Error while extracting boe: " + e.GetBaseException().Message));
            }
            File.Delete(downloadPath);
            RunInUI(() => AddLog("Finished to install BoE.", LogType.good));

            FullUpdateClosed();
        }
        private void RefreshProgress()
        {
            if (actionDownloadsToRun != 0)
                UpdateProgress = (int)(((decimal)actionDownloadsRan / (decimal)actionDownloadsToRun) * 100);
            else
                UpdateProgress = 100;
        }
        private void StartUpdater(int actions, long size, int number = 3)
        {
            actionDownloadsToRun = actions * ACTION_SIZE_VALUE + size;
            ActionToRun = actions;
            ActionRan = 0;
            updaterWorking = number;
            RefreshProgress();

            updater.ActionRan += (object sender, UpdaterEventArgs param) =>
                {
                    switch (param.Action.Type)
                    {
                        case UpdateActionType.AddOrUpdateFile:
                            RunInUI(() => AddLog(string.Format("{0} downloaded", param.Action.Arg), LogType.normal));
                            break;
                        case UpdateActionType.DeleteFile:
                            RunInUI(() => AddLog(string.Format("{0} deleted", param.Action.Arg), LogType.normal));
                            break;
                    }
                    ActionRan++;
                    actionDownloadsRan += param.Action.Value;
                    RefreshProgress();
                };
            updater.ErrorOccured += (object sender, UpdaterEventArgs param) =>
                {
                    RunInUI(() => AddLog(string.Format("Error: {0}", param.Message), LogType.bad));
                };

            updater.Finished += (object sender, UpdaterEventArgs param) =>
                {
                    ActionRan--;
                    UpdateClosed();
                };

            updater.Start(2);
        }

        private void UpdateClosed()
        {
            updating = false;
            SetClientVersion();
            RunInUI(UpdateBoECommand.RaiseCanExecuteChanged);
            RunInUI(LaunchBoECommand.RaiseCanExecuteChanged);
        }
        private void FullUpdateClosed()
        {
            didFullUpdate = true;
            UpdateBoE();
        }

        private void SetVersions()
        {
            SetServerVersion();
            SetClientVersion();
        }
        private void AddLog(string message, LogType type = LogType.normal)
        {
            LogMessages.Add(new LogMessage(message, type));
        }

        private void CheckUpdate()
        {
            try
            {
                IEnumerable<string> files = downloader.GetFiles("");
                if (files.Where(f => !string.IsNullOrWhiteSpace(f)).Count() == 0)
                {
                    RunInUI(() => AddLog("Failed to list ftp files", LogType.bad));
                    HasNetworkError = true;
                    return;
                }
                string launcher = files.FirstOrDefault(f => f.StartsWith("boe_launcher"));
                if (!string.IsNullOrWhiteSpace(launcher))
                {
                    int version = int.Parse(launcher.Replace("boe_launcher_", "").Replace(".exe", ""));
                    if (version > VERSION && MessageBox.Show("A new version of the launcher is available, do you want to update it now ?", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        string currentPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                        string updaterPath = Path.Combine(Path.GetDirectoryName(currentPath), "boe_updater.exe");
                        downloader.DownloadFile("client_updater.exe", updaterPath);
                        Process.Start(updaterPath, "\"" + currentPath + "\"");
                        Application.Current.Shutdown();
                    }
                }
            }
            catch (Exception e)
            {
                RunInUI(() => AddLog("Error: " + e.GetBaseException().Message, LogType.bad));
                HasNetworkError = true;
            }
        }

        private void SetModuleDirectory()
        {
            warbandDirectory = BoEFilesUtils.GetWarbandDirectory();
            if (string.IsNullOrWhiteSpace(warbandDirectory))
            {
                MessageBoxResult messageResult = System.Windows.MessageBox.Show("Warband folder not found, you may select your \"MountBlade Warband\" folder using the next dialog", "", MessageBoxButton.OKCancel);
                if (messageResult == MessageBoxResult.OK)
                {
                    form.FolderBrowserDialog dlg = new form.FolderBrowserDialog();
                    form.DialogResult result = dlg.ShowDialog();
                    if (result == form.DialogResult.OK && !string.IsNullOrWhiteSpace(dlg.SelectedPath))
                    {
                        warbandDirectory = dlg.SelectedPath;
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(warbandDirectory))
            {
                ModuleDirectory = BoEFilesUtils.GetModuleDirectory(warbandDirectory, true);
                if (ModuleDirectory != null)
                {
                    UpdateProgress += 25;
                    ActionRan++;
                    RunInUI(() => AddLog("Warband folder found", LogType.normal));
                    Task.Factory.StartNew(SetVersions);
                    return;
                }
            }

            RunInUI(() => AddLog("Warband folder not found", LogType.bad));
            DirectoryNotFound = true;
        }
        private void SetServerVersion()
        {
            try
            {
                StreamReader reader = new StreamReader(downloader.GetDownloadStream("Source/module.ini"));
                ServerVersion = BoEFilesUtils.GetModuleVersion(reader.ReadToEnd());
                RunInUI(() => AddLog("Server version is " + ServerVersion));
                RunInUI(LaunchBoECommand.RaiseCanExecuteChanged);
                RunInUI(UpdateBoECommand.RaiseCanExecuteChanged);
                UpdateProgress += 50;
                ActionRan++;
                if (ServerVersion == ClientVersion)
                    BoEReady();
            }
            catch (Exception e)
            {
                RunInUI(() => AddLog(e.GetBaseException().Message, LogType.bad));
            }
        }
        private void SetClientVersion()
        {
            string path = Path.Combine(ModuleDirectory, @"module.ini");
            if (!File.Exists(path))
            {
                ClientVersion = -1;
                RunInUI(() => AddLog("Client version not found"));
                RunInUI(() => AddLog("Click on update to install BoE"));
                RunInUI(LaunchBoECommand.RaiseCanExecuteChanged);
                RunInUI(UpdateBoECommand.RaiseCanExecuteChanged);
            }
            else
            {
                StreamReader reader = new StreamReader(path);
                ClientVersion = BoEFilesUtils.GetModuleVersion(reader.ReadToEnd());
                RunInUI(() => AddLog("Client version is " + ClientVersion));
                UpdateButtons();
                if (ServerVersion == ClientVersion)
                    BoEReady();
            }
            ActionRan++;
            UpdateProgress += 25;
        }
        private void BoEReady()
        {
            RunInUI(() => AddLog("BoE is ready to be launched", LogType.good));
        }
        private void UpdateButtons()
        {
            RunInUI(LaunchBoECommand.RaiseCanExecuteChanged);
            RunInUI(UpdateBoECommand.RaiseCanExecuteChanged);
        }

        private void RunInUI(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }
    }
}