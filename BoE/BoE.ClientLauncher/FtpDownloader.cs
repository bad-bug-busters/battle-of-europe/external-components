﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ClientLauncher
{
    public class FtpDownloader : IDownloader
    {
        private FtpClient client;

        private string _source;
        public string Source
        {
            get { return _source; }
            private set { _source = value; }
        }

        public FtpDownloader(string source)
        {
            Source = source;
            client = new FtpClient(Source, "anonymous", "");
        }

        private string Combine(string uri1, string uri2)
        {
            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return string.Format("{0}/{1}", uri1, uri2);
        }

        public void Open()
        {
        }

        public void Close()
        {
        }


        public void DownloadFile(string fileName, string localFile)
        {
            client.Download(fileName, localFile);
        }

        public Stream GetDownloadStream(string fileName)
        {
            return client.GetDownloadStream(fileName);
        }

        public string[] GetFiles(string directory)
        {
            return client.DirectoryListSimple(directory);
        }

        public void Dispose()
        {
            client.Dispose();
        }

        public long GetFileSize(string fileName)
        {
            return client.GetFileSize(fileName);
        }
    }
}
