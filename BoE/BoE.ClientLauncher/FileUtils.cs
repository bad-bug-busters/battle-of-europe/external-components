﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ClientLauncher
{
    public static class FileUtils
    {
        public static byte[] ReadFully(this Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        public static IEnumerable<string> GetDirectoriesPathToFile(string filePath)
        {
            List<string> directories = new List<string>();
            string dirPath = filePath.Replace(Path.GetFileName(filePath), "");
            List<string> allDirs = dirPath.Split(new char[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (allDirs.Count != 0)
            {
                for (int i = 0; i < allDirs.Count; i++)
                {
                    string dir = allDirs[i];
                    string parentsPath = "";
                    foreach (string parent in allDirs.Take(i))
                        parentsPath += parent + "\\";
                    directories.Add(parentsPath + dir);
                }
            }
            return directories;
        }
    }
}
