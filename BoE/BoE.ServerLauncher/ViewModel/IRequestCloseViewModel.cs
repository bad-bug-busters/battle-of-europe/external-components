﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ServerLauncher.ViewModel
{
    public interface IRequestCloseViewModel
    {
        event EventHandler RequestClose;
    }
}
