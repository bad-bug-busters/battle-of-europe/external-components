using BoE.Lib;
using System.IO;
using BoE.ServerLauncher.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System;
using System.Security;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using form = System.Windows.Forms;

namespace BoE.ServerLauncher.ViewModel
{
    public class MainViewModel : ViewModelBase, IRequestCloseViewModel
    {
        const string TokenPath = "Modules\\Battle of Europe\\strings.txt";
        const string TokenRef = "str_server_security_token";
        const string ServiceURI = "http://battleofeurope.net/api/";

        public MainViewModel()
        {
            gameMods = new List<GameMod>()
            {
                new GameMod("Battle", "Sample_Battle_start.bat"),
                new GameMod("Siege", "Sample_Siege_start.bat"),
                new GameMod("Invasion", "Sample_Invasion_start.bat"),
                new GameMod("Capture the flag", "Sample_Capture_the_Flag_start.bat"),
                new GameMod("Conquest", "Sample_Conquest_start.bat"),
                new GameMod("Deathmatch", "Sample_Deathmatch_start.bat"),
                new GameMod("Duel", "Sample_Duel_start.bat"),
                new GameMod("Fight and destroy", "Sample_Fight_and_Destroy_start.bat"),
                new GameMod("Team deathmatch", "Sample_Team_Deathmatch_start.bat"),
            };
            LaunchCommand = new RelayCommand<PasswordBox>(LaunchServer);
            SelectFileCommand = new RelayCommand(SelectFile);
            SelectedFile = TokenPath;
        }

        private string serverName;
        private string username;
        private List<GameMod> gameMods;
        private string selectedMod;
        private string selectedFile;

        public string Username
        {
            get { return username; }
            set { username = value; RaisePropertyChanged("Username"); }
        }
        public string ServerName
        {
            set { serverName = value; RaisePropertyChanged("ServerName"); }
            get { return serverName; }
        }
        public List<GameMod> GameMods
        {
            get
            {
                return gameMods;
            }
        }
        public string SelectedMod
        {
            set
            {
                selectedMod = value;
                RaisePropertyChanged("SelectedMod");
            }
            get
            {
                return selectedMod;
            }
        }
        public string SelectedFile
        {
            set { selectedFile = value; RaisePropertyChanged("SelectedFile"); }
            get { return selectedFile; }
        }

        private void LaunchServer(PasswordBox pb)
        {
            try
            {
                if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(pb.Password) ||
                    string.IsNullOrEmpty(ServerName) || string.IsNullOrEmpty(SelectedMod) || string.IsNullOrEmpty(SelectedFile))
                {
                    MessageBox.Show("All fields are required.");
                    return;
                }

                string password = pb.Password;
                string result = WebApiClient.Get<string>("Launcher/GetToken", new { username = Username, password = password, server = ServerName }, null, ServiceURI);
                if (string.IsNullOrEmpty(result))
                    MessageBox.Show("Failed.");
                else if (TokenIsValid(result))
                {
                    WriteToken(result);
                    ExecuteBatch(SelectedMod);
                    MessageBox.Show("The server is now running with a new token.");
                }
                else
                {
                    MessageBox.Show(result);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.GetBaseException().Message);
            }
        }

        private void SelectFile()
        {
            form.OpenFileDialog dlg = new form.OpenFileDialog();
            form.DialogResult result = dlg.ShowDialog();

            if (result == form.DialogResult.OK)
            {
                SelectedFile = dlg.FileName;
            }
        }

        private void WriteToken(string value)
        {
            int refLine = GetRefLine();
            if (refLine == 0)
                MessageBox.Show(TokenRef + " variable not found.");
            else
            {
                string[] lines = File.ReadAllLines(SelectedFile);

                using (StreamWriter writer = new StreamWriter(SelectedFile))
                {
                    for (int currentLine = 1; currentLine <= lines.Length; ++currentLine)
                    {
                        if (currentLine == refLine)
                        {
                            writer.WriteLine(TokenRef + " " + value);
                        }
                        else
                        {
                            writer.WriteLine(lines[currentLine - 1]);
                        }
                    }
                }
            }
        }
        private int GetRefLine()
        {
            using (StringReader reader = new StringReader(File.ReadAllText(SelectedFile)))
            {
                string line;
                int i = 0;
                do
                {
                    i++;
                    line = reader.ReadLine();
                }
                while (line != null && !line.Contains(TokenRef));

                return line != null ? i : 0;
            }
        }
        private void ExecuteBatch(string path)
        {
            Process proc = null;
            proc = new Process();
            proc.StartInfo.FileName = path;
            proc.StartInfo.CreateNoWindow = false;
            proc.Start();
            proc.WaitForExit();
        }
        private bool TokenIsValid(string value)
        {
            return value.Length == 10;
        }

        public RelayCommand<PasswordBox> LaunchCommand { get; private set; }

        public RelayCommand SelectFileCommand { get; set; }

        public string UsernameLabel
        {
            get { return "Username:"; }
        }
        public string PasswordLabel
        {
            get { return "Password:"; }
        }
        public string ModLabel
        {
            get { return "Game mod:"; }
        }
        public string ServerNameLabel
        {
            get { return "Server name: "; }
        }

        public event EventHandler RequestClose;
    }
}