﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ServerLauncher.Models
{
    public class GameProcess
    {
        public GameProcess() { }

        public GameProcess(Guid owner, string processId, string name)
        {
            Owner = owner;
            ProcessId = processId;
            Name = name;
        }

        public Guid Owner { get; set; }

        public string ProcessId { get; set; }

        public string Name { get; set; }
    }
}
