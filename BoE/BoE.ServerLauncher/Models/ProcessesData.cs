﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BoE.ServerLauncher.Models
{
    public class ProcessesData
    {
        public ProcessesData()
        {
            GameProcesses = new List<GameProcess>();
        }

        public ProcessesData(string data, int version)
        {
            GameProcesses = new List<GameProcess>();
            if (!string.IsNullOrWhiteSpace(data))
            {
                XDocument document = XDocument.Parse(data);
                Version = int.Parse(GetValue(document.Descendants().FirstOrDefault(e => e.Name == "version")));
                foreach (XElement element in document.Descendants().Where(e => e.Name == "game_process"))
                {
                    GameProcesses.Add(new GameProcess(
                        new Guid(element.Attribute("owner").Value),
                        GetValue(element.Descendants().FirstOrDefault(e => e.Name == "process_id")),
                        GetValue(element.Descendants().FirstOrDefault(e => e.Name == "name"))));
                }
            }
            else
            {
                Version = version;
            }
        }

        public int Version { get; set; }

        public List<GameProcess> GameProcesses { get; set; }

        public XDocument ToXDocument()
        {
            XDocument document = new XDocument();
            document.Add(new XElement("boe_game_processes", ""));
            XElement root = document.Descendants().FirstOrDefault(e => e.Name == "boe_game_processes");
            root.Add(new XElement("version", Version));
            foreach (GameProcess gameProcess in GameProcesses)
            {
                root.Add(new XElement("game_process", new XAttribute("owner", gameProcess.Owner.ToString()),
                            new XElement("process_id", gameProcess.ProcessId),
                            new XElement("name", gameProcess.Name)));
            }
            return document;
        }

        private string GetValue(XElement el)
        {
            if (el != null)
                return el.Value;

            return null;
        }
    }
}
