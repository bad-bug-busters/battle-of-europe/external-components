﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ServerLauncher.Models
{
    public class ServerConfig
    {
        public ServerConfig(string name)
        {
            Name = name;
            Mode = "";
            Core = null;
        }
        public ServerConfig(string name, string mode, int? core)
        {
            Name = name;
            Mode = mode;
            Core = core;
        }

        public string Name { get; set; }

        public string Mode { get; set; }

        public int? Core { get; set; }

        public string CoreString
        {
            get { return Core.HasValue ? Core.Value.ToString() : "ND"; }
        }
        public string CoreXString
        {
            get { return Core.HasValue ? Core.Value.ToString() : ""; }
        }
    }
}
