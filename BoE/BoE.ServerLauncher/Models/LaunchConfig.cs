﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BoE.ServerLauncher.Models
{
    public class LaunchConfig
    {
        public LaunchConfig() 
        {
            ServerConfigs = new List<ServerConfig>();
        }

        public LaunchConfig(string config)
        {
            ServerConfigs = new List<ServerConfig>();
            if (!string.IsNullOrWhiteSpace(config))
            {
                XDocument document = XDocument.Parse(config);

                ServiceUrl = GetValue(document.Descendants().FirstOrDefault(xe => xe.Name == "service_url"));
                StringPath = GetValue(document.Descendants().FirstOrDefault(xe => xe.Name == "string_path"));
                Username = GetValue(document.Descendants().FirstOrDefault(xe => xe.Name == "username"));
                Password = GetValue(document.Descendants().FirstOrDefault(xe => xe.Name == "password"));
                ServerPath = GetValue(document.Descendants().FirstOrDefault(xe => xe.Name == "server_path"));

                foreach (XElement elements in document.Descendants().Where(xe => xe.Name == "server_conf"))
                {
                    string name = GetValue(elements.Descendants().FirstOrDefault(xe => xe.Name == "name"));
                    string mode = GetValue(elements.Descendants().FirstOrDefault(xe => xe.Name == "mode_file"));
                    string coreString = GetValue(elements.Descendants().FirstOrDefault(xe => xe.Name == "core_affinity"));
                    int? core = null;
                    if (!string.IsNullOrWhiteSpace(coreString))
                    {
                        int coreInt = -1;
                        if (int.TryParse(coreString, out coreInt))
                            core = coreInt;
                    }
                    ServerConfigs.Add(new ServerConfig(name, mode, core));
                }
            }
        }

        public string ServiceUrl { get; set; }

        public string StringPath { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ServerPath { get; set; }

        public List<ServerConfig> ServerConfigs { get; set; }

        private string GetValue(XElement el)
        {
            if (el != null)
                return el.Value;

            return null;
        }

        const string DefaultServiceUrl = "http://battleofeurope.net/api/";
        const string DefaultTokenPath = "Modules\\Battle of Europe\\strings.txt";
        public static XDocument CreateDefaultConfig()
        {
            LaunchConfig config = new LaunchConfig();
            config.ServiceUrl = DefaultServiceUrl;
            config.StringPath = DefaultTokenPath;
            config.Username = "[your_username]";
            config.Password = "[your_password]";
            config.ServerPath = "";
            config.ServerConfigs.Add(new ServerConfig("[server_1_name]", "Battle.txt", null));
            config.ServerConfigs.Add(new ServerConfig("[server_2_name]", "Siege.txt", null));
            return config.ToXDocument();
        }

        public XDocument ToXDocument()
        {
            XDocument document = new XDocument();
            document.Add(new XElement("boe_config"));
            XElement root = document.Descendants().First(e => e.Name == "boe_config");

            root.Add(new XElement("service_url", ServiceUrl),
                new XElement("string_path", StringPath),
                new XElement("username", Username),
                new XElement("password", Password),
                new XElement("server_path", ServerPath));

            foreach (ServerConfig serverConfig in ServerConfigs)
            {
                root.Add(new XElement("server_conf", 
                    new XElement("name", serverConfig.Name),
                    new XElement("mode_file", serverConfig.Mode),
                    new XElement("core_affinity", serverConfig.CoreXString)));
            }
            return document;
        }
    }
}
