﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ServerLauncher
{
    public static class TokenUtils
    {
        public static void WriteToken(string token, string stringFile, string tokenRef)
        {
            int refLine = GetRefLine(stringFile, tokenRef);
            if (refLine == 0)
                throw new Exception("Token variable not found");
            else
            {
                string[] lines = File.ReadAllLines(stringFile);

                using (StreamWriter writer = new StreamWriter(stringFile))
                {
                    for (int currentLine = 1; currentLine <= lines.Length; ++currentLine)
                    {
                        if (currentLine == refLine)
                        {
                            writer.WriteLine(tokenRef + " " + token);
                        }
                        else
                        {
                            writer.WriteLine(lines[currentLine - 1]);
                        }
                    }
                }
            }
        }
        public static int GetRefLine(string stringFile, string tokenRef)
        {
            using (StringReader reader = new StringReader(File.ReadAllText(stringFile)))
            {
                string line;
                int i = 0;
                do
                {
                    i++;
                    line = reader.ReadLine();
                }
                while (line != null && !line.Contains(tokenRef));

                return line != null ? i : 0;
            }
        }

        public static bool TokenIsValid(string value)
        {
            return value.Length == 10;
        }
    }
}
