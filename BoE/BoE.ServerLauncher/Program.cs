﻿using BoE.Lib;
using BoE.ServerLauncher.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BoE.ServerLauncher
{
    class Program
    {
        #region ExitHandler
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            switch (sig)
            {
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                    OnExit();
                    Environment.Exit(0);
                    break;
                case CtrlType.CTRL_CLOSE_EVENT:
                    OnExit();
                    Environment.Exit(0);
                    break;
                default:
                    return false;
            }
            return false;
        }
        #endregion

        public static List<Process> ServersProcess { get; set; }
        public static Guid UniqueId { get; set; }

        const string WSEPath = "WSELoader.exe";
        const string TokenRef = "str_server_security_token";
        const string GameProcessesFile = "boe_game_processes.xml";
        const string LauncherConfigFile = "boe_config.xml";
        const string WorkingFile = "boe_working";
        const int SecToWait = 5;
        const int LauncherVersion = 1;

        private static void Initialize()
        {
            Process current = Process.GetCurrentProcess();
            current.StartInfo.UseShellExecute = false;
            current.StartInfo.RedirectStandardOutput = true;

            UniqueId = Guid.NewGuid();
            AppDomain.CurrentDomain.AssemblyResolve += OnResolveAssembly;
            AppDomain.CurrentDomain.ProcessExit += (object sender, EventArgs e) => { OnExit(); };

            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            ServersProcess = new List<Process>();
        }
        private static bool HasLauncherWorking()
        {
            string directory = AppDomain.CurrentDomain.BaseDirectory;
            return File.Exists(WorkingFile);
        }

        private static void Start()
        {
            using (FileStream fs = File.Create(WorkingFile))
            {
                fs.Close();
                fs.Dispose();
            }
            Console.WriteLine("START");
        }
        static void Main(string[] args)
        {
            Initialize();

            if (!File.Exists(LauncherConfigFile))
            {
                LaunchConfig.CreateDefaultConfig().Save(LauncherConfigFile);
                Console.WriteLine(string.Format("The configuration file was not found, a default {0} was created.", LauncherConfigFile));
                Console.ReadKey();
                return;
            }

            while (HasLauncherWorking())
                Thread.Sleep(50);

            Start();

            LaunchConfig launchConfig = null;
            try
            {
                string config = File.ReadAllText(LauncherConfigFile);
                launchConfig = new LaunchConfig(config);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to parse the configuration file with error: '" + e.GetBaseException().Message + "'");
                Console.WriteLine("Press any key to exit ...");
                End();
                Console.ReadKey();
                return;
            }


            if (!Launch(launchConfig))
            {
                return;
            }

            Console.WriteLine("All your servers are now running. Exiting this program will shutdown all servers.");
            End();
            while (Console.ReadLine() != "exit")
            {
            }
        }
        private static void End()
        {
            if (File.Exists(WorkingFile))
            {
                while (!IOUtils.IsFileReady(WorkingFile))
                    Thread.Sleep(40);

                File.Delete(WorkingFile);
            }

            Console.WriteLine("END");
        }

        private static void OnExit()
        {
            End();
            foreach (Process proc in ServersProcess)
            {
                if (!proc.HasExited)
                    proc.Kill();
            }
            using (FileStream fileStream = IOUtils.GetFileStream(GameProcessesFile, FileMode.OpenOrCreate))
            {
                StreamReader reader = new StreamReader(fileStream);
                StreamWriter writer = new StreamWriter(fileStream);

                ProcessesData processData = new ProcessesData(reader.ReadToEnd(), LauncherVersion);
                processData.GameProcesses.RemoveAll(gp => gp.Owner == UniqueId);
                fileStream.Seek(0, 0);
                fileStream.SetLength(0);
                processData.ToXDocument().Save(writer);
            }
        }

        private static bool Launch(LaunchConfig launcheConfig)
        {
            string serverPath = launcheConfig.ServerPath;
            if (string.IsNullOrWhiteSpace(serverPath))
                serverPath = AppDomain.CurrentDomain.BaseDirectory;

            foreach (ServerConfig serverConfig in launcheConfig.ServerConfigs)
            {
                try
                {
                    if (LaunchServer(serverConfig, launcheConfig, serverPath))
                    {
                        Process serverProc;
                        using (FileStream fileStream = IOUtils.GetFileStream(GameProcessesFile, FileMode.OpenOrCreate))
                        {
                            StreamReader reader = new StreamReader(fileStream);
                            StreamWriter writer = new StreamWriter(fileStream);
                            
                            ProcessesData processData = new ProcessesData(reader.ReadToEnd(), LauncherVersion);
                            serverProc = Process.GetProcessesByName("mb_warband_dedicated")
                                .FirstOrDefault(p => !processData.GameProcesses.Any(gp => gp.ProcessId == p.Id.ToString()));

                            if (serverProc == null)
                                throw new Exception(string.Format("Couldn't find server '{0}' process to set affinity.", serverConfig.Name));

                            if (serverConfig.Core.HasValue)
                                serverProc.Threads[0].IdealProcessor = serverConfig.Core.Value;

                            ServersProcess.Add(serverProc);
                            processData.GameProcesses.Add(new GameProcess(UniqueId, serverProc.Id.ToString(), serverConfig.Name));
                            fileStream.Seek(0,0);
                            fileStream.SetLength(0);
                            processData.ToXDocument().Save(writer);
                        }
                        Console.WriteLine(string.Format("Server '{0}' is now running on core '{1}'.", serverConfig.Name, serverConfig.CoreString));
                        Console.WriteLine(string.Format("Waiting {0} seconds ...", SecToWait));
                        Thread.Sleep(SecToWait * 1000);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("Failed to run server '{0}' with error: '{1}'", serverConfig.Name, e.GetBaseException().Message));
                    Console.WriteLine("Press any key to exit ...");
                    End();
                    Console.ReadKey();
                    return false;
                }
            }

            return true;
        }

        private static bool LaunchServer(ServerConfig serverConfig, LaunchConfig launcheConfig, string serverPath)
        {
            string servName = File.ReadAllLines(Path.Combine(serverPath, serverConfig.Mode))
                        .FirstOrDefault(c => c.StartsWith("set_server_name"));
            if (string.IsNullOrWhiteSpace(servName) || servName.Split(' ').Last().Trim() != serverConfig.Name)
                throw new Exception(string.Format("The name in the configuration and the game mode file don't match."));

            string result = WebApiClient.Get<string>("Launcher/GetToken", new
            {
                username = launcheConfig.Username,
                password = launcheConfig.Password,
                server = serverConfig.Name
            }, null, launcheConfig.ServiceUrl);

            if (!TokenUtils.TokenIsValid(result))
                throw new Exception(string.Format("Failed to fetch token: {0}", result));

            TokenUtils.WriteToken(result, Path.Combine(serverPath, launcheConfig.StringPath), TokenRef);
            Process process = Process.Start(Path.Combine(serverPath, WSEPath), string.Format("-r {0} -m \"Battle of Europe\"", serverConfig.Mode));
            process.WaitForExit();

            return true;
        }

        private static Assembly OnResolveAssembly(object sender, ResolveEventArgs args)
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = new AssemblyName(args.Name);

            string path = assemblyName.Name + ".dll";
            if (assemblyName.CultureInfo.Equals(CultureInfo.InvariantCulture) == false)
            {
                path = String.Format(@"{0}\{1}", assemblyName.CultureInfo, path);
            }

            using (Stream stream = executingAssembly.GetManifestResourceStream(path))
            {
                if (stream == null)
                    return null;

                byte[] assemblyRawBytes = new byte[stream.Length];
                stream.Read(assemblyRawBytes, 0, assemblyRawBytes.Length);
                return Assembly.Load(assemblyRawBytes);
            }
        }
    }
}
