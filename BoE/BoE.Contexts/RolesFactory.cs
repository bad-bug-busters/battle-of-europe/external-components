﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoE.Contexts
{
    public static class RolesFactory
    {
        public const string SuperAdmin = "SuperAdmin";
        public const string Admin = "Admin";
        public const string Moderator = "Moderator";
        public const string Hoster = "Hoster";

        public static string[] RolesList
        {
            get {
                return new string[]
                {  
                    SuperAdmin,
                    Admin,
                    Moderator,
                    Hoster
                };
            }
        }
    }
}