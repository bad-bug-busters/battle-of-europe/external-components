namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        State = c.Int(nullable: false),
                        UniqueId = c.Int(),
                        AccountEnabled = c.Boolean(nullable: false),
                        LastPasswordFailure = c.DateTime(),
                        LastPasswordSuccess = c.DateTime(),
                        PasswordFailures = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Characters",
                c => new
                    {
                        CharacterId = c.Int(nullable: false),
                        Name = c.String(),
                        Experience = c.Int(nullable: false),
                        Gold = c.Single(nullable: false),
                        Generation = c.Int(nullable: false),
                        LoomPoint = c.Int(nullable: false),
                        LastTick = c.DateTime(),
                        CreationDate = c.DateTime(nullable: false),
                        BuildId = c.Int(),
                        FactionId = c.Int(),
                        FactionRoleId = c.Int(),
                    })
                .PrimaryKey(t => t.CharacterId)
                .ForeignKey("dbo.Builds", t => t.BuildId)
                .ForeignKey("dbo.Factions", t => t.FactionId)
                .ForeignKey("dbo.FactionRoles", t => t.FactionRoleId)
                .ForeignKey("dbo.Users", t => t.CharacterId)
                .Index(t => t.BuildId)
                .Index(t => t.FactionId)
                .Index(t => t.FactionRoleId)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.Builds",
                c => new
                    {
                        BuildId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CharacterId = c.Int(nullable: false),
                        Item1Id = c.Int(),
                        Item2Id = c.Int(),
                        Item3Id = c.Int(),
                        Item4Id = c.Int(),
                        HeadId = c.Int(),
                        BodyId = c.Int(),
                        FootId = c.Int(),
                        HandId = c.Int(),
                        HorseId = c.Int(),
                        Strength = c.Int(nullable: false),
                        Agility = c.Int(nullable: false),
                        HorseArchery = c.Int(nullable: false),
                        Riding = c.Int(nullable: false),
                        Athletics = c.Int(nullable: false),
                        WeaponMaster = c.Int(nullable: false),
                        Resistance = c.Int(nullable: false),
                        PowerThrow = c.Int(nullable: false),
                        PowerStrike = c.Int(nullable: false),
                        PowerPull = c.Int(nullable: false),
                        PowerReload = c.Int(nullable: false),
                        OffHand = c.Int(nullable: false),
                        OneHanded = c.Int(nullable: false),
                        TwoHanded = c.Int(nullable: false),
                        Polearm = c.Int(nullable: false),
                        Archery = c.Int(nullable: false),
                        Crossbow = c.Int(nullable: false),
                        Throwing = c.Int(nullable: false),
                        Firearm = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BuildId)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .ForeignKey("dbo.Inventories", t => t.Item1Id)
                .ForeignKey("dbo.Inventories", t => t.Item2Id)
                .ForeignKey("dbo.Inventories", t => t.Item3Id)
                .ForeignKey("dbo.Inventories", t => t.Item4Id)
                .ForeignKey("dbo.Inventories", t => t.HeadId)
                .ForeignKey("dbo.Inventories", t => t.BodyId)
                .ForeignKey("dbo.Inventories", t => t.FootId)
                .ForeignKey("dbo.Inventories", t => t.HandId)
                .ForeignKey("dbo.Inventories", t => t.HorseId)
                .Index(t => t.CharacterId)
                .Index(t => t.Item1Id)
                .Index(t => t.Item2Id)
                .Index(t => t.Item3Id)
                .Index(t => t.Item4Id)
                .Index(t => t.HeadId)
                .Index(t => t.BodyId)
                .Index(t => t.FootId)
                .Index(t => t.HandId)
                .Index(t => t.HorseId);
            
            CreateTable(
                "dbo.Inventories",
                c => new
                    {
                        InventoryId = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        CharacterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InventoryId)
                .ForeignKey("dbo.Items", t => t.ItemId)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .Index(t => t.ItemId)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ItemId = c.Int(nullable: false, identity: true),
                        NameId = c.String(nullable: false),
                        GameId = c.Int(nullable: false),
                        LoomLevel = c.Int(nullable: false),
                        ItemSort = c.Int(nullable: false),
                        Name = c.String(),
                        HitPoint = c.Int(nullable: false),
                        Difficulty = c.Int(nullable: false),
                        Value = c.Int(nullable: false),
                        FromKind = c.Boolean(nullable: false),
                        NextLoomId = c.Int(),
                        PrevLoomId = c.Int(),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Items", t => t.NextLoomId)
                .ForeignKey("dbo.Items", t => t.PrevLoomId)
                .Index(t => t.NextLoomId)
                .Index(t => t.PrevLoomId);
            
            CreateTable(
                "dbo.Factions",
                c => new
                    {
                        FactionId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.FactionId);
            
            CreateTable(
                "dbo.FactionApplications",
                c => new
                    {
                        FactionApplicationId = c.Int(nullable: false, identity: true),
                        FactionId = c.Int(nullable: false),
                        CharacterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FactionApplicationId)
                .ForeignKey("dbo.Factions", t => t.FactionId)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .Index(t => t.FactionId)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.FactionRoles",
                c => new
                    {
                        FactionRoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Recruit = c.Boolean(nullable: false),
                        Lead = c.Boolean(nullable: false),
                        FactionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FactionRoleId)
                .ForeignKey("dbo.Factions", t => t.FactionId)
                .Index(t => t.FactionId);
            
            CreateTable(
                "dbo.Codes",
                c => new
                    {
                        CodeId = c.Int(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.CodeId)
                .ForeignKey("dbo.Users", t => t.CodeId)
                .Index(t => t.CodeId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        NewsId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        AuthorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NewsId)
                .ForeignKey("dbo.Users", t => t.AuthorId)
                .Index(t => t.AuthorId);
            
            CreateTable(
                "dbo.GameServers",
                c => new
                    {
                        GameServerId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        CharList = c.String(),
                        LastUpdate = c.DateTime(),
                        LastTick = c.DateTime(),
                        TokenId = c.Int(),
                        Enabled = c.Boolean(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GameServerId)
                .ForeignKey("dbo.Tokens", t => t.TokenId)
                .Index(t => t.TokenId);
            
            CreateTable(
                "dbo.Tokens",
                c => new
                    {
                        TokenId = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        Dealine = c.DateTime(nullable: false),
                        Enable = c.Boolean(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        GameServerId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TokenId)
                .ForeignKey("dbo.GameServers", t => t.GameServerId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.GameServerId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserActions",
                c => new
                    {
                        UserActionId = c.Int(nullable: false, identity: true),
                        AddressIp = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        GameServerId = c.Int(),
                    })
                .PrimaryKey(t => t.UserActionId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.GameServers", t => t.GameServerId)
                .Index(t => t.UserId)
                .Index(t => t.GameServerId);
            
            CreateTable(
                "dbo.RoleUsers",
                c => new
                    {
                        Role_RoleId = c.Int(nullable: false),
                        User_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_RoleId, t.User_UserId })
                .ForeignKey("dbo.Roles", t => t.Role_RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.Role_RoleId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Weapons",
                c => new
                    {
                        ItemId = c.Int(nullable: false),
                        Weight = c.Single(nullable: false),
                        Length = c.Int(nullable: false),
                        Speed = c.Int(nullable: false),
                        HorseBack = c.Boolean(nullable: false),
                        BonusShield = c.Boolean(nullable: false),
                        Thrust = c.Int(nullable: false),
                        ThrustType = c.Int(nullable: false),
                        Swing = c.Int(nullable: false),
                        SwingType = c.Int(nullable: false),
                        Unbalanced = c.Boolean(nullable: false),
                        Knockdown = c.Boolean(nullable: false),
                        Crushthrough = c.Boolean(nullable: false),
                        Couchable = c.Boolean(nullable: false),
                        MissileSpeed = c.Int(nullable: false),
                        Accuracy = c.Int(nullable: false),
                        Ammo = c.Int(nullable: false),
                        Slot = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        SecondaryId = c.Int(),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Items", t => t.ItemId)
                .ForeignKey("dbo.Weapons", t => t.SecondaryId)
                .Index(t => t.ItemId)
                .Index(t => t.SecondaryId);
            
            CreateTable(
                "dbo.Armors",
                c => new
                    {
                        ItemId = c.Int(nullable: false),
                        Weight = c.Single(nullable: false),
                        Head = c.Int(nullable: false),
                        Body = c.Int(nullable: false),
                        Foot = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Items", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.Horses",
                c => new
                    {
                        ItemId = c.Int(nullable: false),
                        Armor = c.Int(nullable: false),
                        Speed = c.Int(nullable: false),
                        Maneuver = c.Int(nullable: false),
                        Charge = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Items", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.Shields",
                c => new
                    {
                        ItemId = c.Int(nullable: false),
                        Weight = c.Single(nullable: false),
                        Armor = c.Int(nullable: false),
                        Speed = c.Int(nullable: false),
                        Thrust = c.Int(nullable: false),
                        ThrustType = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        Slot = c.Int(nullable: false),
                        HorseBack = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Items", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.Goods",
                c => new
                    {
                        ItemId = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Items", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        UserActionId = c.Int(nullable: false),
                        BrowserName = c.String(),
                        BrowserVersion = c.String(),
                    })
                .PrimaryKey(t => t.UserActionId)
                .ForeignKey("dbo.UserActions", t => t.UserActionId)
                .Index(t => t.UserActionId);
            
            CreateTable(
                "dbo.UserTicks",
                c => new
                    {
                        UserActionId = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        Gold = c.Single(nullable: false),
                        Experience = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserActionId)
                .ForeignKey("dbo.UserActions", t => t.UserActionId)
                .Index(t => t.UserActionId);
            
            CreateTable(
                "dbo.UserRetires",
                c => new
                    {
                        UserActionId = c.Int(nullable: false),
                        Experience = c.Int(nullable: false),
                        Generation = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserActionId)
                .ForeignKey("dbo.UserActions", t => t.UserActionId)
                .Index(t => t.UserActionId);
            
            CreateTable(
                "dbo.UserUpKeeps",
                c => new
                    {
                        UserActionId = c.Int(nullable: false),
                        Golds = c.Single(nullable: false),
                        Usages = c.String(),
                    })
                .PrimaryKey(t => t.UserActionId)
                .ForeignKey("dbo.UserActions", t => t.UserActionId)
                .Index(t => t.UserActionId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserUpKeeps", new[] { "UserActionId" });
            DropIndex("dbo.UserRetires", new[] { "UserActionId" });
            DropIndex("dbo.UserTicks", new[] { "UserActionId" });
            DropIndex("dbo.UserLogins", new[] { "UserActionId" });
            DropIndex("dbo.Goods", new[] { "ItemId" });
            DropIndex("dbo.Shields", new[] { "ItemId" });
            DropIndex("dbo.Horses", new[] { "ItemId" });
            DropIndex("dbo.Armors", new[] { "ItemId" });
            DropIndex("dbo.Weapons", new[] { "SecondaryId" });
            DropIndex("dbo.Weapons", new[] { "ItemId" });
            DropIndex("dbo.RoleUsers", new[] { "User_UserId" });
            DropIndex("dbo.RoleUsers", new[] { "Role_RoleId" });
            DropIndex("dbo.UserActions", new[] { "GameServerId" });
            DropIndex("dbo.UserActions", new[] { "UserId" });
            DropIndex("dbo.Tokens", new[] { "UserId" });
            DropIndex("dbo.Tokens", new[] { "GameServerId" });
            DropIndex("dbo.GameServers", new[] { "TokenId" });
            DropIndex("dbo.News", new[] { "AuthorId" });
            DropIndex("dbo.Codes", new[] { "CodeId" });
            DropIndex("dbo.FactionRoles", new[] { "FactionId" });
            DropIndex("dbo.FactionApplications", new[] { "CharacterId" });
            DropIndex("dbo.FactionApplications", new[] { "FactionId" });
            DropIndex("dbo.Items", new[] { "PrevLoomId" });
            DropIndex("dbo.Items", new[] { "NextLoomId" });
            DropIndex("dbo.Inventories", new[] { "CharacterId" });
            DropIndex("dbo.Inventories", new[] { "ItemId" });
            DropIndex("dbo.Builds", new[] { "HorseId" });
            DropIndex("dbo.Builds", new[] { "HandId" });
            DropIndex("dbo.Builds", new[] { "FootId" });
            DropIndex("dbo.Builds", new[] { "BodyId" });
            DropIndex("dbo.Builds", new[] { "HeadId" });
            DropIndex("dbo.Builds", new[] { "Item4Id" });
            DropIndex("dbo.Builds", new[] { "Item3Id" });
            DropIndex("dbo.Builds", new[] { "Item2Id" });
            DropIndex("dbo.Builds", new[] { "Item1Id" });
            DropIndex("dbo.Builds", new[] { "CharacterId" });
            DropIndex("dbo.Characters", new[] { "CharacterId" });
            DropIndex("dbo.Characters", new[] { "FactionRoleId" });
            DropIndex("dbo.Characters", new[] { "FactionId" });
            DropIndex("dbo.Characters", new[] { "BuildId" });
            DropForeignKey("dbo.UserUpKeeps", "UserActionId", "dbo.UserActions");
            DropForeignKey("dbo.UserRetires", "UserActionId", "dbo.UserActions");
            DropForeignKey("dbo.UserTicks", "UserActionId", "dbo.UserActions");
            DropForeignKey("dbo.UserLogins", "UserActionId", "dbo.UserActions");
            DropForeignKey("dbo.Goods", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Shields", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Horses", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Armors", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Weapons", "SecondaryId", "dbo.Weapons");
            DropForeignKey("dbo.Weapons", "ItemId", "dbo.Items");
            DropForeignKey("dbo.RoleUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "Role_RoleId", "dbo.Roles");
            DropForeignKey("dbo.UserActions", "GameServerId", "dbo.GameServers");
            DropForeignKey("dbo.UserActions", "UserId", "dbo.Users");
            DropForeignKey("dbo.Tokens", "UserId", "dbo.Users");
            DropForeignKey("dbo.Tokens", "GameServerId", "dbo.GameServers");
            DropForeignKey("dbo.GameServers", "TokenId", "dbo.Tokens");
            DropForeignKey("dbo.News", "AuthorId", "dbo.Users");
            DropForeignKey("dbo.Codes", "CodeId", "dbo.Users");
            DropForeignKey("dbo.FactionRoles", "FactionId", "dbo.Factions");
            DropForeignKey("dbo.FactionApplications", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.FactionApplications", "FactionId", "dbo.Factions");
            DropForeignKey("dbo.Items", "PrevLoomId", "dbo.Items");
            DropForeignKey("dbo.Items", "NextLoomId", "dbo.Items");
            DropForeignKey("dbo.Inventories", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.Inventories", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Builds", "HorseId", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "HandId", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "FootId", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "BodyId", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "HeadId", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "Item4Id", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "Item3Id", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "Item2Id", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "Item1Id", "dbo.Inventories");
            DropForeignKey("dbo.Builds", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.Characters", "CharacterId", "dbo.Users");
            DropForeignKey("dbo.Characters", "FactionRoleId", "dbo.FactionRoles");
            DropForeignKey("dbo.Characters", "FactionId", "dbo.Factions");
            DropForeignKey("dbo.Characters", "BuildId", "dbo.Builds");
            DropTable("dbo.UserUpKeeps");
            DropTable("dbo.UserRetires");
            DropTable("dbo.UserTicks");
            DropTable("dbo.UserLogins");
            DropTable("dbo.Goods");
            DropTable("dbo.Shields");
            DropTable("dbo.Horses");
            DropTable("dbo.Armors");
            DropTable("dbo.Weapons");
            DropTable("dbo.RoleUsers");
            DropTable("dbo.UserActions");
            DropTable("dbo.Tokens");
            DropTable("dbo.GameServers");
            DropTable("dbo.News");
            DropTable("dbo.Roles");
            DropTable("dbo.Codes");
            DropTable("dbo.FactionRoles");
            DropTable("dbo.FactionApplications");
            DropTable("dbo.Factions");
            DropTable("dbo.Items");
            DropTable("dbo.Inventories");
            DropTable("dbo.Builds");
            DropTable("dbo.Characters");
            DropTable("dbo.Users");
        }
    }
}
