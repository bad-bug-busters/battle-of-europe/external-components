namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LoomsAndRepair : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Characters", "FactionId", "dbo.Factions");
            DropForeignKey("dbo.Characters", "FactionRoleId", "dbo.FactionRoles");
            DropForeignKey("dbo.FactionApplications", "FactionId", "dbo.Factions");
            DropForeignKey("dbo.FactionApplications", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.FactionRoles", "FactionId", "dbo.Factions");
            DropIndex("dbo.Characters", new[] { "FactionId" });
            DropIndex("dbo.Characters", new[] { "FactionRoleId" });
            DropIndex("dbo.FactionApplications", new[] { "FactionId" });
            DropIndex("dbo.FactionApplications", new[] { "CharacterId" });
            DropIndex("dbo.FactionRoles", new[] { "FactionId" });
            CreateTable(
                "dbo.Clans",
                c => new
                    {
                        ClanId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ClanId);
            
            CreateTable(
                "dbo.ClanApplications",
                c => new
                    {
                        ClanApplicationId = c.Int(nullable: false, identity: true),
                        ClanId = c.Int(nullable: false),
                        CharacterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClanApplicationId)
                .ForeignKey("dbo.Clans", t => t.ClanId)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .Index(t => t.ClanId)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "dbo.ClanRoles",
                c => new
                    {
                        ClanRoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Recruit = c.Boolean(nullable: false),
                        Lead = c.Boolean(nullable: false),
                        ClanId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClanRoleId)
                .ForeignKey("dbo.Clans", t => t.ClanId)
                .Index(t => t.ClanId);
            
            CreateTable(
                "dbo.Looms",
                c => new
                    {
                        LoomId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        CharacterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LoomId)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .Index(t => t.CharacterId);
            
            AddColumn("dbo.Characters", "ClanId", c => c.Int());
            AddColumn("dbo.Characters", "ClanRoleId", c => c.Int());
            AddColumn("dbo.Inventories", "RepairPrice", c => c.Int());
            AddForeignKey("dbo.Characters", "ClanId", "dbo.Clans", "ClanId");
            AddForeignKey("dbo.Characters", "ClanRoleId", "dbo.ClanRoles", "ClanRoleId");
            CreateIndex("dbo.Characters", "ClanId");
            CreateIndex("dbo.Characters", "ClanRoleId");
            DropColumn("dbo.Characters", "FactionId");
            DropColumn("dbo.Characters", "FactionRoleId");
            DropTable("dbo.Factions");
            DropTable("dbo.FactionApplications");
            DropTable("dbo.FactionRoles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FactionRoles",
                c => new
                    {
                        FactionRoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Recruit = c.Boolean(nullable: false),
                        Lead = c.Boolean(nullable: false),
                        FactionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FactionRoleId);
            
            CreateTable(
                "dbo.FactionApplications",
                c => new
                    {
                        FactionApplicationId = c.Int(nullable: false, identity: true),
                        FactionId = c.Int(nullable: false),
                        CharacterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FactionApplicationId);
            
            CreateTable(
                "dbo.Factions",
                c => new
                    {
                        FactionId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.FactionId);
            
            AddColumn("dbo.Characters", "FactionRoleId", c => c.Int());
            AddColumn("dbo.Characters", "FactionId", c => c.Int());
            DropIndex("dbo.Looms", new[] { "CharacterId" });
            DropIndex("dbo.ClanRoles", new[] { "ClanId" });
            DropIndex("dbo.ClanApplications", new[] { "CharacterId" });
            DropIndex("dbo.ClanApplications", new[] { "ClanId" });
            DropIndex("dbo.Characters", new[] { "ClanRoleId" });
            DropIndex("dbo.Characters", new[] { "ClanId" });
            DropForeignKey("dbo.Looms", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.ClanRoles", "ClanId", "dbo.Clans");
            DropForeignKey("dbo.ClanApplications", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.ClanApplications", "ClanId", "dbo.Clans");
            DropForeignKey("dbo.Characters", "ClanRoleId", "dbo.ClanRoles");
            DropForeignKey("dbo.Characters", "ClanId", "dbo.Clans");
            DropColumn("dbo.Inventories", "RepairPrice");
            DropColumn("dbo.Characters", "ClanRoleId");
            DropColumn("dbo.Characters", "ClanId");
            DropTable("dbo.Looms");
            DropTable("dbo.ClanRoles");
            DropTable("dbo.ClanApplications");
            DropTable("dbo.Clans");
            CreateIndex("dbo.FactionRoles", "FactionId");
            CreateIndex("dbo.FactionApplications", "CharacterId");
            CreateIndex("dbo.FactionApplications", "FactionId");
            CreateIndex("dbo.Characters", "FactionRoleId");
            CreateIndex("dbo.Characters", "FactionId");
            AddForeignKey("dbo.FactionRoles", "FactionId", "dbo.Factions", "FactionId");
            AddForeignKey("dbo.FactionApplications", "CharacterId", "dbo.Characters", "CharacterId");
            AddForeignKey("dbo.FactionApplications", "FactionId", "dbo.Factions", "FactionId");
            AddForeignKey("dbo.Characters", "FactionRoleId", "dbo.FactionRoles", "FactionRoleId");
            AddForeignKey("dbo.Characters", "FactionId", "dbo.Factions", "FactionId");
        }
    }
}
