namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedWinsAndLoses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "Wins", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.Characters", "Loses", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.Characters", "WinARow", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.UserUpKeeps", "HasWon", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserUpKeeps", "HasWon");
            DropColumn("dbo.Characters", "WinARow");
            DropColumn("dbo.Characters", "Loses");
            DropColumn("dbo.Characters", "Wins");
        }
    }
}
