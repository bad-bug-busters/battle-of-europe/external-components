namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBanForPlayers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bans",
                c => new
                    {
                        BanId = c.Int(nullable: false, identity: true),
                        Reason = c.String(),
                        Expiration = c.DateTime(),
                        CreationDate = c.DateTime(nullable: false),
                        CharacterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BanId)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .Index(t => t.CharacterId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Bans", new[] { "CharacterId" });
            DropForeignKey("dbo.Bans", "CharacterId", "dbo.Characters");
            DropTable("dbo.Bans");
        }
    }
}
