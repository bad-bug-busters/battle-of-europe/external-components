namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDatesAndLoom : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "LoomPoints", c => c.Int(nullable: false));

            AddColumn("dbo.Inventories", "CreationDate", c => c.DateTime(nullable: false));
            Sql("UPDATE dbo.Inventories SET CreationDate='2014-03-21 00:00:00.000'");

            AddColumn("dbo.Clans", "CreationDate", c => c.DateTime(nullable: false));
            Sql("UPDATE dbo.Clans SET CreationDate='2014-03-21 00:00:00.000'");

            AddColumn("dbo.ClanApplications", "CreationDate", c => c.DateTime(nullable: false));
            Sql("UPDATE dbo.ClanApplications SET CreationDate='2014-03-21 00:00:00.000'");

            AddColumn("dbo.ClanRoles", "CreationDate", c => c.DateTime(nullable: false));
            Sql("UPDATE dbo.ClanRoles SET CreationDate='2014-03-21 00:00:00.000'");

            AddColumn("dbo.Looms", "CreationDate", c => c.DateTime(nullable: false));
            Sql("UPDATE dbo.Looms SET CreationDate='2014-03-21 00:00:00.000'");

            AddColumn("dbo.Inventories", "Disabled", c => c.Boolean(nullable: false));

            Sql("UPDATE dbo.Characters SET LoomPoints=LoomPoint");
            DropColumn("dbo.Characters", "LoomPoint");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Characters", "LoomPoint", c => c.Int(nullable: false));
            DropColumn("dbo.Looms", "CreationDate");
            DropColumn("dbo.ClanRoles", "CreationDate");
            DropColumn("dbo.ClanApplications", "CreationDate");
            DropColumn("dbo.Clans", "CreationDate");
            DropColumn("dbo.Inventories", "CreationDate");
            DropColumn("dbo.Inventories", "Disabled");
            DropColumn("dbo.Characters", "LoomPoints");
        }
    }
}
