namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImprovedUserActions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserUpKeepUsages",
                c => new
                    {
                        UserUpKeepUsageId = c.Int(nullable: false, identity: true),
                        Usage = c.Int(nullable: false),
                        UserUpKeepId = c.Int(nullable: false),
                        ItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserUpKeepUsageId)
                .ForeignKey("dbo.UserUpKeeps", t => t.UserUpKeepId)
                .ForeignKey("dbo.Items", t => t.ItemId)
                .Index(t => t.UserUpKeepId)
                .Index(t => t.ItemId);
            
            AddColumn("dbo.UserTicks", "Tick", c => c.Int(nullable: false));
            DropColumn("dbo.UserUpKeeps", "Usages");

            Sql("DELETE FROM dbo.UserTicks");
            Sql("DELETE FROM dbo.UserUpKeeps");
            Sql("DELETE FROM dbo.UserLogins");
            Sql("DELETE FROM dbo.UserRetires");
            Sql("DELETE FROM dbo.UserActions");
            Sql("DBCC CHECKIDENT (UserActions, reseed, 0)");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserUpKeeps", "Usages", c => c.String());
            DropIndex("dbo.UserUpKeepUsages", new[] { "ItemId" });
            DropIndex("dbo.UserUpKeepUsages", new[] { "UserUpKeepId" });
            DropForeignKey("dbo.UserUpKeepUsages", "ItemId", "dbo.Items");
            DropForeignKey("dbo.UserUpKeepUsages", "UserUpKeepId", "dbo.UserUpKeeps");
            DropColumn("dbo.UserTicks", "Tick");
            DropTable("dbo.UserUpKeepUsages");
        }
    }
}
