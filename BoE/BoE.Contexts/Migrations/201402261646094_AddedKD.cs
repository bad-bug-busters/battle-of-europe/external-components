namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedKD : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "Kills", c => c.Int(nullable: false));
            AddColumn("dbo.Characters", "Deaths", c => c.Int(nullable: false));
            AddColumn("dbo.UserTicks", "Kills", c => c.Int(nullable: false));
            AddColumn("dbo.UserTicks", "Deaths", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserTicks", "Deaths");
            DropColumn("dbo.UserTicks", "Kills");
            DropColumn("dbo.Characters", "Deaths");
            DropColumn("dbo.Characters", "Kills");
        }
    }
}
