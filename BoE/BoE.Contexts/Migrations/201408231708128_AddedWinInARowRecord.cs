namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedWinInARowRecord : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserTicks", "WinARow", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserTicks", "WinARow");
        }
    }
}
