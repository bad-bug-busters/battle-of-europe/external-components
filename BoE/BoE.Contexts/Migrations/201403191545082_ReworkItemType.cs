namespace BoE.Contexts.Migrations
{
    using BoE.Models;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReworkItemType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "Type", c => c.Int(nullable: false, defaultValue: (int?)ItemType.OneH));
            AddColumn("dbo.Armors", "ArmorType", c => c.Int(nullable: false, defaultValue: (int?)ArmorType.Light));
            DropColumn("dbo.Weapons", "Type");
            DropColumn("dbo.Armors", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Armors", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Weapons", "Type", c => c.Int(nullable: false));
            DropColumn("dbo.Armors", "ArmorType");
            DropColumn("dbo.Items", "Type");
        }
    }
}
