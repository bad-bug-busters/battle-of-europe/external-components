namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedClans : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clans", "Description", c => c.String());
            AddColumn("dbo.Clans", "Banner", c => c.Binary());
            AddColumn("dbo.Clans", "GameId", c => c.Int());
            AddColumn("dbo.Clans", "DefaultRoleId", c => c.Int());
            AddForeignKey("dbo.Clans", "DefaultRoleId", "dbo.ClanRoles", "ClanRoleId");
            CreateIndex("dbo.Clans", "DefaultRoleId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Clans", new[] { "DefaultRoleId" });
            DropForeignKey("dbo.Clans", "DefaultRoleId", "dbo.ClanRoles");
            DropColumn("dbo.Clans", "DefaultRoleId");
            DropColumn("dbo.Clans", "GameId");
            DropColumn("dbo.Clans", "Banner");
            DropColumn("dbo.Clans", "Description");
        }
    }
}
