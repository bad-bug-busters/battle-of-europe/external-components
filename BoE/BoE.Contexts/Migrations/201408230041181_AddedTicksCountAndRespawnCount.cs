namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTicksCountAndRespawnCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "TicksSinceLastUpkeep", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.UserUpKeeps", "RespawnCount", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.UserUpKeeps", "RoundResult", c => c.Int(nullable: false));
            Sql("UPDATE dbo.UserUpKeeps SET RoundResult=HasWon");
            DropColumn("dbo.UserUpKeeps", "HasWon");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserUpKeeps", "HasWon", c => c.Boolean(nullable: false));
            DropColumn("dbo.UserUpKeeps", "RespawnCount");
            DropColumn("dbo.UserUpKeeps", "RoundResult");
            DropColumn("dbo.Characters", "TicksSinceLastUpkeep");
        }
    }
}
