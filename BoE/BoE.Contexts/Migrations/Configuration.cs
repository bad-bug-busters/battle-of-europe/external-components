namespace BoE.Contexts.Migrations
{
    using BoE.Lib;
    using BoE.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BoE.Contexts.BoEContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BoEContext db)
        {
            db.Users.AddOrUpdate(u => u.Username, new User()
            {
                Username = "Administrator",
                Password = Utils.HashPassword("Gr3n4deIzOP!", "TUBjay9EdWRl"),
                LastPasswordSuccess = DateTime.Now,
                AccountEnabled = true,
                State = UserState.Normal
            });
            db.SaveChanges();
            foreach (string role in RolesFactory.RolesList)
                db.Roles.AddOrUpdate(r => r.Name, new Role() { Name = role, Users = new List<User>() { db.Users.First(u => u.Username == "Administrator") } });
            db.SaveChanges();
        }
    }
}
