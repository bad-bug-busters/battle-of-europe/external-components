namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTickCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserUpKeeps", "TickCount", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserUpKeeps", "TickCount");
        }
    }
}
