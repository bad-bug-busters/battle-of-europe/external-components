namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GoldToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Characters", "Gold", c => c.Int(nullable: false));
            AlterColumn("dbo.UserTicks", "Gold", c => c.Int(nullable: false));
            AlterColumn("dbo.UserUpKeeps", "Golds", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserUpKeeps", "Golds", c => c.Single(nullable: false));
            AlterColumn("dbo.UserTicks", "Gold", c => c.Single(nullable: false));
            AlterColumn("dbo.Characters", "Gold", c => c.Single(nullable: false));
        }
    }
}
