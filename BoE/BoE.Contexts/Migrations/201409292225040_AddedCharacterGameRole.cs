namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCharacterGameRole : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CharacterGameRoles",
                c => new
                    {
                        CharacterGameRoleId = c.Int(nullable: false, identity: true),
                        GameRoleType = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        CharacterId = c.Int(nullable: false),
                        GameServerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CharacterGameRoleId)
                .ForeignKey("dbo.Characters", t => t.CharacterId)
                .ForeignKey("dbo.GameServers", t => t.GameServerId)
                .Index(t => t.CharacterId)
                .Index(t => t.GameServerId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.CharacterGameRoles", new[] { "GameServerId" });
            DropIndex("dbo.CharacterGameRoles", new[] { "CharacterId" });
            DropForeignKey("dbo.CharacterGameRoles", "GameServerId", "dbo.GameServers");
            DropForeignKey("dbo.CharacterGameRoles", "CharacterId", "dbo.Characters");
            DropTable("dbo.CharacterGameRoles");
        }
    }
}
