namespace BoE.Contexts.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MultipleCodesType : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Codes");
            CreateTable(
                "dbo.Codes",
                c => new
                {
                    CodeId = c.Int(nullable: false, identity: true),
                    UserId = c.Int(nullable: false),
                    Type = c.Int(nullable: false),
                    CreationDate = c.DateTime(nullable: false),
                    Value = c.String(),
                })
                .PrimaryKey(t => t.CodeId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Codes", new[] { "UserId" });
            DropForeignKey("dbo.Codes", "UserId", "dbo.Users");
            AlterColumn("dbo.Codes", "CodeId", c => c.Int(nullable: false));
            DropColumn("dbo.Codes", "CreationDate");
            DropColumn("dbo.Codes", "Type");
            DropColumn("dbo.Codes", "UserId");
            CreateIndex("dbo.Codes", "CodeId");
            AddForeignKey("dbo.Codes", "CodeId", "dbo.Users", "UserId");
        }
    }
}
