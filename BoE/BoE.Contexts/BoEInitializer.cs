﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoE.Contexts.Migrations;
using System.Data.Entity.Migrations;

namespace BoE.Contexts
{
    public class BoEInitializer : IDatabaseInitializer<BoEContext>
    {
        public void InitializeDatabase(BoEContext db)
        {
            DbMigrator migrator = new DbMigrator(new Configuration());
            migrator.Update();
        }
    }
}
