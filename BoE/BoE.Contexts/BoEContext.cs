﻿using BoE.Contexts.Migrations;
using BoE.Models;
using System.Data.Entity;

namespace BoE.Contexts
{
    public class BoEContext : DbContext
    {
        public BoEContext()
        {
            Database.SetInitializer<BoEContext>(new BoEInitializer());
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Character> Characters { get; set; }

        public DbSet<CharacterGameRole> CharacterGameRoles { get; set; }

        public DbSet<Ban> Bans { get; set; }

        public DbSet<Build> Builds { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<Loom> Looms { get; set; }

        public DbSet<Inventory> Inventories { get; set; }

        public DbSet<GameServer> GameServers { get; set; }

        public DbSet<Token> Tokens { get; set; }

        public DbSet<Code> Codes { get; set; }

        public DbSet<UserAction> UserActions { get; set; }
        public DbSet<UserUpKeepUsage> UserUpKeepUsages { get; set; }
        
        public DbSet<Clan> Clans { get; set; }
        public DbSet<ClanApplication> ClanApplications { get; set; }
        public DbSet<ClanRole> ClanRoles { get; set; }

        public DbSet<News> Newses { get; set; }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            mb.Entity<Weapon>()
                .ToTable("Weapons");
            mb.Entity<Armor>()
                .ToTable("Armors");
            mb.Entity<Horse>()
                .ToTable("Horses");
            mb.Entity<Shield>()
                .ToTable("Shields");
            mb.Entity<Good>()
                .ToTable("Goods");

            mb.Entity<UserLogin>()
                .ToTable("UserLogins");
            mb.Entity<UserTick>()
                .ToTable("UserTicks");
            mb.Entity<UserRetire>()
                .ToTable("UserRetires");
            mb.Entity<UserUpKeep>()
                .ToTable("UserUpKeeps");

            #region Inventory
            mb.Entity<Inventory>()
                .HasRequired(i => i.Item)
                .WithMany(i => i.Inventories)
                .HasForeignKey(i => i.ItemId)
                .WillCascadeOnDelete(false);

            mb.Entity<Inventory>()
                .HasRequired(i => i.Character)
                .WithMany(i => i.Inventories)
                .HasForeignKey(i => i.CharacterId)
                .WillCascadeOnDelete(false);

            #endregion

            #region GameServer
            mb.Entity<Token>()
                .HasRequired(t => t.GameServer)
                .WithMany()
                .HasForeignKey(t => t.GameServerId)
                .WillCascadeOnDelete(false);

            mb.Entity<GameServer>()
                .HasOptional(gs => gs.Token)
                .WithMany()
                .HasForeignKey(gs => gs.TokenId)
                .WillCascadeOnDelete(false);
            #endregion

            #region User
            mb.Entity<User>()
                .HasOptional(u => u.Character)
                .WithRequired(c => c.User)
                .WillCascadeOnDelete(false);
            #endregion

            #region Code
            mb.Entity<Code>()
                .HasRequired(c => c.User)
                .WithMany(u => u.Codes)
                .HasForeignKey(c => c.UserId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Build
            mb.Entity<Character>()
                .HasOptional(c => c.Build)
                .WithMany()
                .HasForeignKey(c => c.BuildId)
                .WillCascadeOnDelete(false);

            mb.Entity<Build>()
                .HasRequired(b => b.Character)
                .WithMany(c => c.Builds)
                .HasForeignKey(b => b.CharacterId)
                .WillCascadeOnDelete(false);

            mb.Entity<Build>()
                .HasOptional(b => b.Item1)
                .WithMany(i => i.BuildItem1s)
                .HasForeignKey(b => b.Item1Id)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Item2)
                .WithMany(i => i.BuildItem2s)
                .HasForeignKey(b => b.Item2Id)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Item3)
                .WithMany(i => i.BuildItem3s)
                .HasForeignKey(b => b.Item3Id)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Item4)
                .WithMany(i => i.BuildItem4s)
                .HasForeignKey(b => b.Item4Id)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Head)
                .WithMany(i => i.BuildHeads)
                .HasForeignKey(b => b.HeadId)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Body)
                .WithMany(i => i.BuildBodies)
                .HasForeignKey(b => b.BodyId)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Foot)
                .WithMany(i => i.BuildFoots)
                .HasForeignKey(b => b.FootId)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Hand)
                .WithMany(i => i.BuildHands)
                .HasForeignKey(b => b.HandId)
                .WillCascadeOnDelete(false);
            mb.Entity<Build>()
                .HasOptional(b => b.Horse)
                .WithMany(i => i.BuildHorses)
                .HasForeignKey(b => b.HorseId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Items

            mb.Entity<Weapon>()
                .HasOptional(w => w.Secondary)
                .WithMany()
                .HasForeignKey(w => w.SecondaryId)
                .WillCascadeOnDelete(false);

            mb.Entity<Item>()
                .HasOptional(i => i.NextLoom)
                .WithMany()
                .HasForeignKey(i => i.NextLoomId);
            mb.Entity<Item>()
                .HasOptional(i => i.PrevLoom)
                .WithMany()
                .HasForeignKey(i => i.PrevLoomId);
            #endregion

            #region News
            mb.Entity<News>()
                .HasRequired(n => n.Author)
                .WithMany(u => u.Newses)
                .HasForeignKey(n => n.AuthorId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Clan
            mb.Entity<ClanRole>()
                .HasRequired(fr => fr.Clan)
                .WithMany(f => f.ClanRoles)
                .HasForeignKey(fr => fr.ClanId)
                .WillCascadeOnDelete(false);
            mb.Entity<ClanApplication>()
                .HasRequired(fa => fa.Clan)
                .WithMany(f => f.ClanApplications)
                .HasForeignKey(fa => fa.ClanId)
                .WillCascadeOnDelete(false);
            mb.Entity<ClanApplication>()
                .HasRequired(fa => fa.Character)
                .WithMany(c => c.ClanApplications)
                .HasForeignKey(fa => fa.CharacterId)
                .WillCascadeOnDelete(false);

            mb.Entity<Clan>()
                .HasOptional(cl => cl.DefaultRole)
                .WithMany()
                .HasForeignKey(cl => cl.DefaultRoleId)
                .WillCascadeOnDelete(false);

            #endregion

            #region Character
            mb.Entity<Character>()
                .HasOptional(c => c.Clan)
                .WithMany(f => f.Characters)
                .HasForeignKey(c => c.ClanId)
                .WillCascadeOnDelete(false);
            mb.Entity<Character>()
                .HasOptional(c => c.ClanRole)
                .WithMany(f => f.Charaters)
                .HasForeignKey(c => c.ClanRoleId)
                .WillCascadeOnDelete(false);
            #endregion

            #region UserAction
            mb.Entity<UserAction>()
                .HasRequired(ua => ua.User)
                .WithMany(u => u.UserActions)
                .HasForeignKey(ua => ua.UserId)
                .WillCascadeOnDelete(false);

            mb.Entity<UserAction>()
                .HasOptional(ua => ua.GameServer)
                .WithMany(gs => gs.UserActions)
                .HasForeignKey(ua => ua.GameServerId)
                .WillCascadeOnDelete(false);

            mb.Entity<UserUpKeepUsage>()
                .HasRequired(uuu => uuu.Item)
                .WithMany(i => i.UserUpKeepUsages)
                .HasForeignKey(uuu => uuu.ItemId)
                .WillCascadeOnDelete(false);

            mb.Entity<UserUpKeepUsage>()
                .HasRequired(uuu => uuu.UserUpKeep)
                .WithMany(uk => uk.UserUpKeepUsages)
                .HasForeignKey(uuu => uuu.UserUpKeepId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Loom
            mb.Entity<Loom>()
                .HasRequired(l => l.Character)
                .WithMany(c => c.Looms)
                .HasForeignKey(l => l.CharacterId)
                .WillCascadeOnDelete(false);
            #endregion

            #region Ban

            mb.Entity<Ban>()
                .HasRequired(b => b.Character)
                .WithMany(c => c.Bans)
                .HasForeignKey(b => b.CharacterId)
                .WillCascadeOnDelete(false);

            #endregion

            #region CharacterGameRole

            mb.Entity<CharacterGameRole>()
                .HasRequired(cgr => cgr.Character)
                .WithMany(c => c.CharacterGameRoles)
                .HasForeignKey(cgr => cgr.CharacterId)
                .WillCascadeOnDelete(false);

            mb.Entity<CharacterGameRole>()
                .HasRequired(cgr => cgr.GameServer)
                .WithMany(c => c.CharacterGameRoles)
                .HasForeignKey(cgr => cgr.GameServerId)
                .WillCascadeOnDelete(false);

            #endregion
        }
    }
}
