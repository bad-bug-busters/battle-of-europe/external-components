﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ServerLauncher.Models
{
    public class DataAction
    {
        public DataAction() { }
        public DataAction(ActionType type, string path)
        {
            Type = type;
            FullPath = path;
        }
        public DataAction(ActionType type, string path, string newFullPath)
        {
            Type = type;
            FullPath = path;
            NewFullPath = newFullPath;
        }

        public ActionType Type { get; set; }

        public string FullPath { get; set; }

        public string NewFullPath { get; set; }
    }

    public enum ActionType
    {
        AddorUpdate,
        Delete,
        DeleteFolder,
        Rename
    }
}
