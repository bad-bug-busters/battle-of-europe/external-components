﻿using BoE.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BoE.ServerLauncher.Models
{
    public class FileHash
    {
        public FileHash() { }

        public FileHash(string path)
        {
            Path = path;
            while (true)
            {
                try
                {
                    Hash = FileHash.GetHash(IOUtils.ReadAllBytes(path));
                    Size = new FileInfo(path).Length;
                    break;
                }catch(IOException e)
                {
                    Thread.Sleep(10);
                }
            }
        }
        public FileHash(string path, string hash, long size)
        {
            Path = path;
            Hash = hash;
            Size = size;
        }

        public string Path { get; set; }

        public string Hash { get; set; }

        public long Size { get; set; }

        public static string GetHash(byte[] data)
        {
            using (var md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(data)).Replace("-", "").ToLower(); ;
            }
        }

        public XElement ToXElement(string altPath = null)
        {
            return new XElement("FileHash",
                    new XAttribute("Path", altPath != null ? altPath :  Path),
                    new XAttribute("Hash", Hash),
                    new XAttribute("Size", Size));
        }
    }
}
