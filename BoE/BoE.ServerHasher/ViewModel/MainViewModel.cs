using System.Windows.Forms;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.IO;
using System.Linq;
using BoE.ServerLauncher.Models;
using System.Security.Cryptography;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace BoE.ServerLauncher.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private ConcurrentQueue<DataAction> cQueue;
        private bool hasSaved;
        public bool HasSaved
        {
            get { return hasSaved; }
            set { hasSaved = value; RaisePropertyChanged("HasSaved"); RaisePropertyChanged("LoadingVisibility"); }
        }
        public string LoadingVisibility
        {
            get { return !HasSaved ? "Visible" : "Hidden"; }
        }

        private IDataFile dataFile;
        private FileSystemWatcher watcher;

        private string directoryPath;
        public string DirectoryPath
        {
            get { return directoryPath; }
            set { 
                directoryPath = value;
                RaisePropertyChanged("DirectoryPath");
                RaisePropertyChanged("HasDirectory");
            }
        }

        private string fileDirectory;
        public string FileDirectory
        {
            get { return fileDirectory; }
            set
            {
                fileDirectory = value;
                RaisePropertyChanged("FileDirectory");
            }
        }

        private bool loggingEnabled;
        public bool LoggingEnabled
        {
            get { return loggingEnabled; }
            set { 
                loggingEnabled = value; 
                RaisePropertyChanged("LoggingEnabled"); 
            }
        }

        private FastObservableCollection<string> logs;
        public FastObservableCollection<string> Logs
        {
            get { return logs; }
            set { logs = value; RaisePropertyChanged("Log"); }
        }

        public RelayCommand ChangeCommand { get; set; }
        public RelayCommand ChangeFileCommand { get; set; }
        public RelayCommand StartCommand { get; set; }
        public RelayCommand StopCommand { get; set; }

        public MainViewModel()
        {
            cQueue = new ConcurrentQueue<DataAction>();
            Logs = new FastObservableCollection<string>();
            ChangeCommand = new RelayCommand(ChangeDirectory, CanChange);
            ChangeFileCommand = new RelayCommand(ChangeFile, CanChange);
            StartCommand = new RelayCommand(StartTracking, CanStartTrack);
            StopCommand = new RelayCommand(StopTracking, CanStopTrack);
            watcher = new FileSystemWatcher() { IncludeSubdirectories=true };
            watcher.EnableRaisingEvents = false;
            watcher.Created += new FileSystemEventHandler(FileCreated);
            watcher.Deleted += new FileSystemEventHandler(FileDeleted);
            watcher.Changed += new FileSystemEventHandler(FileChanged);
            watcher.Renamed += new RenamedEventHandler(FileRenamed);

            HasSaved = true;
            Task.Factory.StartNew(() => RunTask());
        }

        public bool CanStartTrack()
        {
            return !string.IsNullOrWhiteSpace(DirectoryPath) && !string.IsNullOrWhiteSpace(FileDirectory) 
                && watcher.EnableRaisingEvents == false;
        }
        public bool CanStopTrack()
        {
            return watcher.EnableRaisingEvents == true;
        }
        private bool CanChange()
        {
            return watcher.EnableRaisingEvents == false;
        }

        private void ChangeDirectory()
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                DirectoryPath = dlg.SelectedPath;
                watcher.Path = dlg.SelectedPath;
            }
        }
        private void ChangeFile()
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                FileDirectory = dlg.SelectedPath;
            }
        }

        private void StartTracking()
        {
            dataFile = new XmlDataFile(Path.Combine(FileDirectory, "files_hash.xml"));
            watcher.EnableRaisingEvents = true;
            dataFile.CreateFile(GetFileDirectorys(DirectoryPath).Where(fi => fi != dataFile.FilePath)
                .Select(fi => new FileHash(fi)));
            dataFile.SaveChanges();
        }
        private void StopTracking()
        {
            watcher.EnableRaisingEvents = false;
        }

        private void RunTask()
        {
            while (true)
            {
                DataAction action;
                if (cQueue.TryDequeue(out action))
                {
                    switch (action.Type)
                    {
                        case ActionType.AddorUpdate:
                            dataFile.AddOrUpdateFileHash(new FileHash(action.FullPath));
                            break;
                        case ActionType.Delete:
                            dataFile.DeleteFileHash(action.FullPath);
                            break;
                        case ActionType.DeleteFolder:
                            dataFile.DeleteFileHashDirectory(action.FullPath);
                            break;
                        case ActionType.Rename:
                            dataFile.RenameFileHash(action.FullPath, action.NewFullPath);
                            break;
                    }
                }
                else if (!HasSaved)
                {
                    dataFile.SaveChanges();
                    HasSaved = true;
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }
        }
        private void AddTask(DataAction action)
        {
            cQueue.Enqueue(action);
            HasSaved = false;
        }

        private void FileCreated(object source, FileSystemEventArgs e)
        {
            if (e.FullPath == dataFile.FilePath)
                return;
            if (IsFolder(e.FullPath))
                GetFileDirectorys(e.FullPath).ToList()
                    .ForEach(p => AddTask(new DataAction(ActionType.AddorUpdate, p)));
            else
                AddTask(new DataAction(ActionType.AddorUpdate, e.FullPath));
        }
        private void FileDeleted(object source, FileSystemEventArgs e)
        {
            if (e.FullPath == dataFile.FilePath)
                return;
            if (IsFolder(e.FullPath))
                AddTask(new DataAction(ActionType.DeleteFolder, e.FullPath));
            else
                AddTask(new DataAction(ActionType.Delete, e.FullPath));
        }
        private void FileChanged(object source, FileSystemEventArgs e)
        {
            if (e.FullPath == dataFile.FilePath || IsFolder(e.FullPath))
                return;
            AddTask(new DataAction(ActionType.AddorUpdate, e.FullPath));
        }
        private void FileRenamed(object source, RenamedEventArgs e)
        {
            if (e.OldFullPath == dataFile.FilePath)
                return;
            if (IsFolder(e.FullPath))
                GetFileDirectorys(e.FullPath).ToList()
                    .ForEach(p => new DataAction(ActionType.Rename, p.Replace(e.FullPath, e.OldFullPath), p));
            else
                AddTask(new DataAction(ActionType.Rename, e.FullPath));
        }

        private bool IsFolder(string path)
        {
            return string.IsNullOrEmpty(Path.GetExtension(path)) && !File.Exists(path);
        }

        private string[] GetFileDirectorys(string path)
        {
            while (true)
            {
                try
                {
                    return Directory.GetFiles(path, "*", SearchOption.AllDirectories);
                }
                catch (IOException e)
                {
                    Thread.Sleep(10);
                }
            }
        }

        private void AddLog(string[] logs)
        {
            if (LoggingEnabled)
                foreach (string log in logs)
                    Logs.Add(log);
        }
    }
}