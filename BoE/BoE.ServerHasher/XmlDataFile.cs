﻿using BoE.ServerLauncher.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BoE.ServerLauncher
{
    public class XmlDataFile : IDataFile
    {
        private XDocument xml;
        private List<string> changeLogs;

        public string FilePath { get; private set; }

        public XmlDataFile(string filePath)
        {
            FilePath = filePath;
            changeLogs = new List<string>();
        }

        public void CreateFile(IEnumerable<FileHash> filesHash)
        {
            xml = new XDocument(new XElement("FilesHash", filesHash.Select(f => 
                f.ToXElement(RelativePath(f.Path)))));
        }

        public void DeleteFile()
        {
            File.Delete(FilePath);
        }

        public void AddOrUpdateFileHash(FileHash fileHash)
        {
            string path = RelativePath(fileHash.Path);

            XElement xFile = xml.Element("FilesHash").Elements()
                .FirstOrDefault(xe => xe.Attribute("Path").Value == path);

            if (xFile != null)
            {
                if (xFile.Attribute("Hash").Value == fileHash.Hash)
                    return;
                xFile.SetAttributeValue("Hash", fileHash.Hash);
                xFile.SetAttributeValue("Size", fileHash.Size);
            }
            else
            {
                xml.Element("FilesHash").Add(fileHash.ToXElement(path));
            }
        }

        public FileHash GetFileHash(string path)
        {
            return xml.Element("FilesHash").Elements()
                .Select(xe => new FileHash(xe.Attribute("Path").Value, xe.Attribute("Hash").Value, long.Parse(xe.Attribute("Size").Value)))
                .FirstOrDefault(fh => fh.Path == RelativePath(path));
        }

        public IEnumerable<FileHash> GetAllFileHash()
        {
            return xml.Element("FilesHash").Elements()
                .Select(xe => new FileHash(xe.Attribute("Path").Value, xe.Attribute("Hash").Value, long.Parse(xe.Attribute("Size").Value)));
        }

        public void DeleteFileHash(string path)
        {
            XElement xFile = xml.Element("FilesHash").Elements()
                .FirstOrDefault(xe => xe.Attribute("Path").Value == RelativePath(path));
            if (xFile == null)
                return;
            xFile.Remove();
        }

        public void DeleteFileHashDirectory(string path)
        {
            IEnumerable<XElement> xFiles = xml.Element("FilesHash").Elements()
                .Where(xe => xe.Attribute("Path").Value.StartsWith(RelativePath(path)));
            foreach (XElement xFile in xFiles)
            {
                xFile.Remove();
            }
        }

        public void SaveChanges()
        {
            xml.Save(FilePath);
        }

        public void RenameFileHash(string path, string newPath)
        {
            XElement xFile = xml.Element("FilesHash").Elements()
                .FirstOrDefault(xe => xe.Attribute("Path").Value == RelativePath(path));
            xFile.SetAttributeValue("Path", RelativePath(newPath));
        }

        private string RelativePath(string path)
        {
            return path.Replace(Path.GetDirectoryName(FilePath), "");
        }
    }
}
