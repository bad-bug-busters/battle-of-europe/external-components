﻿using BoE.ServerLauncher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.ServerLauncher
{
    public interface IDataFile
    {
        string FilePath { get; }
        void CreateFile(IEnumerable<FileHash> filesHash);
        void DeleteFile();
        void AddOrUpdateFileHash(FileHash fileHash);
        FileHash GetFileHash(string path);
        IEnumerable<FileHash> GetAllFileHash();
        void DeleteFileHash(string path);
        void DeleteFileHashDirectory(string path);
        void RenameFileHash(string path, string newPath);
        void SaveChanges();
    }
}
