﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace BoE.Lib
{
    public static class ImageUtility
    {
        public static byte[] GetImageContent(byte[] content, int width, int height, bool onlyResizeIfWider = true, long quality = 80)
        {
            Image image = ResizeImage(byteArrayToImage(content), width, height, onlyResizeIfWider);

            EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, quality);
            ImageCodecInfo jpegCodec = getEncoderInfo("image/jpeg");

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            using (var memStream = new MemoryStream())
            {
                image.Save(memStream, jpegCodec, encoderParams);
                image.Dispose();
                return memStream.ToArray();
            }

        }

        private static Image ResizeImage(Image fullSizeImage, int newWidth, int maxHeight, bool onlyResizeIfWider, bool resizeHeightOnly = false)
        {
            // Prevent using images internal thumbnail
            fullSizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            fullSizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (onlyResizeIfWider)
            {
                if (fullSizeImage.Width <= newWidth)
                {
                    newWidth = fullSizeImage.Width;
                }
            }

            int newHeight = fullSizeImage.Height * newWidth / fullSizeImage.Width;
            if (newHeight > maxHeight || resizeHeightOnly)
            {
                // Resize with height instead
                newWidth = fullSizeImage.Width * maxHeight / fullSizeImage.Height;
                newHeight = maxHeight;
            }
            return fullSizeImage.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);
        }

        private static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private static ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }
    }
}
