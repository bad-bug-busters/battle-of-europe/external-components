﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Lib
{
    public static class Utils
    {
        public static bool Between(this int value, int left, int right)
        {
            return value > left && value < right;
        }

        public static bool GetBit(this byte b, int bitNumber)
        {
            return (b & (1 << bitNumber)) != 0;
        }

        public static byte[] ShiftRight(this byte[] value, int bitcount)
        {
            byte[] temp = new byte[value.Length];
            if (bitcount >= 8)
            {
                Array.Copy(value, 0, temp, bitcount / 8, temp.Length - (bitcount / 8));
            }
            else
            {
                Array.Copy(value, temp, temp.Length);
            }
            if (bitcount % 8 != 0)
            {
                for (int i = temp.Length - 1; i >= 0; i--)
                {
                    temp[i] >>= bitcount % 8;
                    if (i > 0)
                    {
                        temp[i] |= (byte)(temp[i - 1] << 8 - bitcount % 8);
                    }
                }
            }
            return temp;
        }

        public static byte[] ToByteArray(this string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static byte ConvertToByte(this BitArray bits)
        {
            if (bits.Count != 8)
                throw new ArgumentException("The bits length must be 8.");
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }

        public static string HashPassword(string pass, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] src = Convert.FromBase64String(salt);
            byte[] dst = new byte[src.Length + bytes.Length];
            byte[] inArray = null;
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
            inArray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inArray);
        }

        public static Expression<Func<TEntity, bool>> ConstantEqualsValueSelectorExpression<TEntity>(string constant, Expression<Func<TEntity, string>> valueSelector)
        {
            var expression = Expression.Equal(
            valueSelector.Body,
            Expression.Constant(
            constant,
            typeof(string)));
            return Expression.Lambda<Func<TEntity, bool>>(expression, valueSelector.Parameters.Single());
        }

        public static Expression<Func<TEntity, bool>> CollectionContainsValueSelectorExpression<TEntity>(ICollection<int> collection, Expression<Func<TEntity, int>> valueSelector)
        {
            var containsMethod = typeof(ICollection<int>).GetMethod("Contains", new[] { typeof(int) });
            var collectionExpression = Expression.Constant(collection, typeof(ICollection<int>));
            var expression = Expression.Call(collectionExpression, containsMethod, valueSelector.Body);
            return Expression.Lambda<Func<TEntity, bool>>(expression, valueSelector.Parameters.Single());
        }

        public static Expression<Func<TEntity, bool>> CollectionContainsValueSelectorExpression<TEntity>(ICollection<string> collection, Expression<Func<TEntity, string>> valueSelector)
        {
            var containsMethod = typeof(ICollection<string>).GetMethod("Contains", new[] { typeof(string) });
            var collectionExpression = Expression.Constant(collection, typeof(ICollection<string>));
            var expression = Expression.Call(collectionExpression, containsMethod, valueSelector.Body);
            return Expression.Lambda<Func<TEntity, bool>>(expression, valueSelector.Parameters.Single());
        }

        public static string GenerateCode(Random random, int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
