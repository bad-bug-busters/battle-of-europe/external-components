﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;

namespace BoE.Lib
{
    public static class WebApiClient
    {
        private static HttpClient GetClient(string serviceUri)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(serviceUri != null ? serviceUri : ConfigurationManager.AppSettings["ServiceUri"]);

            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public static T Get<T>(string action, object parameters = null, JsonSerializerSettings settings = null, string serviceUri = null)
        {
            HttpClient client = GetClient(serviceUri);
            HttpResponseMessage response;
            if(parameters == null)
                response = client.GetAsync(action).Result;  // Blocking call!
            else
                response = client.GetAsync(action + GetParameters(parameters)).Result;

            if (response.IsSuccessStatusCode)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                if (settings != null)
                    return JsonConvert.DeserializeObject<T>(json, settings);
                else
                    return JsonConvert.DeserializeObject<T>(json);
            }
            else
                return default(T);
        }

        public static T Post<T>(string action, object data = null, JsonSerializerSettings settings = null, string serviceUri = null)
        {
            HttpClient client = GetClient(serviceUri);

            HttpResponseMessage response = client.PostAsJsonAsync(action, data).Result;
            if (response.IsSuccessStatusCode)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                if(settings != null)
                    return JsonConvert.DeserializeObject<T>(json, settings);
                else
                    return JsonConvert.DeserializeObject<T>(json);
            }
            else
                return default(T);
        }

        private static string GetParameters(object oparams)
        {
            IList<PropertyInfo> props = new List<PropertyInfo>(oparams.GetType().GetProperties());
            string parameters = "";
            for (int i = 0; i < props.Count; i++)
            {
                string param = props[i].Name + "=" + props[i].GetValue(oparams);
                parameters += i == 0 ? "?" + param : "&" + param;
            }
            return parameters;
        }
    }
}
