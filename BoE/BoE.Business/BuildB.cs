﻿using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Business
{
    public static class BuildB
    {
        const int MaxSkill = 10;
        const int MaxAttr = 30;
        const int MinAttr = 3;

        public static int GetSkillsForAtt
        {
            get { return 2; }
        }

        public static Build GetNewBuild(int userId)
        {
            return new Build()
            {
                Name = "New build",
                CharacterId = userId,
                Strength = 3,
                Agility = 3
            };
        }

        public static void ResetBuild(Build build)
        {
            build.Item1Id = null;
            build.Item2Id = null;
            build.Item3Id = null;
            build.Item4Id = null;
            build.HeadId = null;
            build.BodyId = null;
            build.FootId = null;
            build.HandId = null;
            build.HorseId = null;

            build.Strength = 3;
            build.Agility = 3;
            build.HorseArchery = 0;
            build.Riding = 0;
            build.Athletics = 0;
            build.WeaponMaster = 0;
            build.Resistance = 0;
            build.PowerThrow = 0;
            build.PowerStrike = 0;
            build.PowerPull = 0;
            build.PowerReload = 0;
            build.OffHand = 0;
            build.OneHanded = 0;
            build.TwoHanded = 0;
            build.Polearm = 0;
            build.Archery = 0;
            build.Crossbow = 0;
            build.Throwing = 0;
            build.Firearm = 0;
        }

        public static bool IsValid(Build build)
        {
            List<int> Atts = build.GetAttsList;
            List<int> Skills = build.GetSkillsList;
            List<int> WPFs = build.GetWPFsList;
            int maxAtt = build.GetMaxAttPoint;
            int maxSkill = build.GetMaxSkillPoint;

            int total = 0;
            foreach (var n in Atts) 
            {
                maxAtt -= n; 
                if (n > MaxAttr) { return false; }
                if (n < MinAttr) { return false; } 
            };
            if (maxAtt < 0)
            {
                while (maxAtt < 0)
                {
                    maxAtt++;
                    maxSkill -= BuildB.GetSkillsForAtt;
                }
                if(maxSkill < 0)
                    return false;
            }
            foreach (var n in Skills) { maxSkill -= n; if (n > MaxSkill) { return false; } };
            if (maxSkill < 0)
            {
                while (maxSkill < 0)
                {
                    maxSkill += BuildB.GetSkillsForAtt;
                    maxAtt--;
                }
                if (maxAtt < 0)
                    return false;
            }
            total = 0;
            WPFs.ForEach(i => total += GetWpfPointUsed(i));
            if (total > build.GetMaxWpfPoint)
                return false;

            foreach (int val in build.GetAgiSkillList)
                if (val > build.Agility / 3)
                    return false;
            foreach (int val in build.GetStrSkillList)
                if (val > build.Strength / 3)
                    return false;

            return true;
        }

        public static bool SetBuild(Build model, Build buildDb)
        {
            List<int> Stats = model.GetAttsList.Concat(model.GetSkillsList).Concat(model.GetWPFsList).ToList();
            List<int> OldStats = buildDb.GetAttsList.Concat(buildDb.GetSkillsList).Concat(buildDb.GetWPFsList).ToList();
            for (int i = 0; i < Stats.Count; i++)
            {
                if (Stats[i] < OldStats[i])
                    return false;
            }

            if (!IsItemValid(model.Item1Id, buildDb) || !IsItemValid(model.Item2Id, buildDb) || !IsItemValid(model.Item3Id, buildDb) || 
                !IsItemValid(model.Item4Id, buildDb) || !IsItemValid(model.HeadId, buildDb) || !IsItemValid(model.BodyId, buildDb) ||
                !IsItemValid(model.FootId, buildDb) || !IsItemValid(model.HeadId, buildDb) || !IsItemValid(model.HorseId, buildDb))
                return false;

            buildDb.Name = model.Name;

            buildDb.Item1Id = model.Item1Id;
            buildDb.Item2Id = model.Item2Id;
            buildDb.Item3Id = model.Item3Id;
            buildDb.Item4Id = model.Item4Id;
            buildDb.HeadId = model.HeadId;
            buildDb.BodyId = model.BodyId;
            buildDb.FootId = model.FootId;
            buildDb.HandId = model.HandId;
            buildDb.HorseId = model.HorseId;

            buildDb.Strength = model.Strength;
            buildDb.Agility = model.Agility;
            buildDb.HorseArchery = model.HorseArchery;
            buildDb.Riding = model.Riding;
            buildDb.Athletics = model.Athletics;
            buildDb.WeaponMaster = model.WeaponMaster;
            buildDb.Resistance = model.Resistance;
            buildDb.PowerThrow = model.PowerThrow;
            buildDb.PowerStrike = model.PowerStrike;
            buildDb.PowerPull = model.PowerPull;
            buildDb.PowerReload = model.PowerReload;
            buildDb.OffHand = model.OffHand;
            buildDb.OneHanded = model.OneHanded;
            buildDb.TwoHanded = model.TwoHanded;
            buildDb.Polearm = model.Polearm;
            buildDb.Archery = model.Archery;
            buildDb.Crossbow = model.Crossbow;
            buildDb.Throwing = model.Throwing;
            buildDb.Firearm = model.Firearm;

            return true;
        }
        private static bool IsItemValid(int? itemId, Build buildDb)
        {
            return !itemId.HasValue || buildDb.Character.Inventories.FirstOrDefault(i => i.InventoryId == itemId.Value) != null; 
        }

        public static int GetWpfPointToUp(int value)
        {
            return (int)Math.Ceiling((double)value / 50);
        }
        public static int GetWpfPointUsed(int value)
        {
            int v = 0;
            for (int i = 0; i < value; i++)
                v += GetWpfPointToUp(value - i);
            return v;
        }
    }
}
