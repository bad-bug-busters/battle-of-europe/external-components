﻿using BoE.Contexts;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoE.Business
{
    public static class ShopB
    {
        public static bool CanBuyItem(Item item, Character character, BoEContext db)
        {
            if (item.LoomLevel == LoomLevel.Zero)
                return true;

            LoomType? loomType = item.LoomType;
            if (loomType.HasValue && character.Looms.Where(lo => lo.Type == loomType.Value).Any(lo => lo.Level == item.LoomLevel))
                return true;

            return false;
        }

        public static void SellItem(Inventory inventory, bool fullRecup = false, BoEContext db = null)
        {
            if (db == null)
                db = new BoEContext();

            inventory.Character.Gold += fullRecup ? inventory.Item.Value : inventory.Item.SellValue;
            inventory.BuildItem1s.ToList().ForEach(b => b.Item1 = null);
            inventory.BuildItem2s.ToList().ForEach(b => b.Item2 = null);
            inventory.BuildItem3s.ToList().ForEach(b => b.Item3 = null);
            inventory.BuildItem4s.ToList().ForEach(b => b.Item4 = null);
            inventory.BuildHeads.ToList().ForEach(b => b.Head = null);
            inventory.BuildBodies.ToList().ForEach(b => b.Body = null);
            inventory.BuildFoots.ToList().ForEach(b => b.Foot = null);
            inventory.BuildHands.ToList().ForEach(b => b.Hand = null);
            inventory.BuildHorses.ToList().ForEach(b => b.Horse = null);
            db.Inventories.Remove(inventory);
        }
    }
}
