﻿using BoE.Contexts;
using BoE.Lib;
using BoE.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BoE.Business
{
    public static class ItemB
    {
        public static string GetChangeLogFromChangeList(List<List<string[]>> changesList)
        {
            if (changesList.Count == 0)
                return "";

            StringBuilder sb = new StringBuilder();
            foreach (List<string[]> changes in changesList)
            {
                sb.AppendLine(changes[0][0]);
                sb.AppendLine("");
                for (int i = 1; i < changes.Count; i++)
                {
                    if(changes[i].Length >= 3)
                        sb.AppendLine(string.Format("{0}: {1} => {2}", changes[i][0], changes[i][1], changes[i][2]));
                }
                sb.AppendLine("--------------------");
            }
            return sb.ToString();
        }

        public static List<List<string[]>> AddOrUpdateItemsFromKind(List<Item> allItems, Dictionary<string, Item> itemsDbByNameId, BoEContext db, out int added, out int updated)
        {
            List<List<string[]>> allChanges = new List<List<string[]>>();
            added = 0;
            updated = 0;
            for (int i = 0; i < allItems.Count; i++)
            {
                Item item = allItems[i];
                Item itemDb = itemsDbByNameId.ContainsKey(item.NameId) ? itemsDbByNameId[item.NameId] : null;
                if (itemDb == null) 
                {
                    db.Items.Add(item);
                    added++;
                }
                else
                {
                    itemsDbByNameId.Remove(item.NameId);
                    item.ItemId = itemDb.ItemId;
                    List<string[]> changes = itemDb.UpdateItem(item);
                    if (changes.Count > 1) //always 1 because of the name
                    {
                        updated++;
                        allChanges.Add(changes);
                    }
                }
            }
            return allChanges;
        }

        public static void RemoveInventoryByKinds(Dictionary<string, Item> itemsDbByNameId, BoEContext db)
        {
            foreach (KeyValuePair<string, Item> kvItem in itemsDbByNameId)
            {
                Item item = kvItem.Value;
                foreach (Inventory inv in item.Inventories.ToList())
                {
                    ShopB.SellItem(inv, true, db);
                }
                item.NextLoomId = null;
                item.PrevLoomId = null;
            }
        }

        public static int RemoveItemsByKinds(Dictionary<string, Item> itemsDbByNameId, BoEContext db)
        {
            int removed = 0;
            foreach (KeyValuePair<string, Item> kvItem in itemsDbByNameId)
            {
                Item item = kvItem.Value;

                foreach (UserUpKeepUsage uuu in item.UserUpKeepUsages.ToList())
                    db.UserUpKeepUsages.Remove(uuu);

                db.Items.Remove(item);
                removed++;
            }
            return removed;
        }

        public static void SetLoomsRelations(List<Item> items0, List<Item> items1, List<Item> items2, List<Item> items3, Dictionary<int, Item> itemsDbByGameId)
        {
            for (int i = 0; i < items0.Count; i++)
            {
                Item item0 = itemsDbByGameId[items0[i].GameId];
                Item item1 = itemsDbByGameId[items1[i].GameId];
                Item item2 = itemsDbByGameId[items2[i].GameId];
                Item item3 = itemsDbByGameId[items3[i].GameId];

                item0.PrevLoomId = null;
                item0.NextLoomId = item1.ItemId;

                item1.PrevLoomId = item0.ItemId;
                item1.NextLoomId = item2.ItemId;

                item2.PrevLoomId = item1.ItemId;
                item2.NextLoomId = item3.ItemId;

                item3.PrevLoomId = item2.ItemId;
                item3.NextLoomId = null;
            }
        }

        public static ItemGender GetGenderFromType(ItemType type)
        {
            if (type == ItemType.Arrow || type == ItemType.Bolt || type == ItemType.Bow || type == ItemType.Bullet || 
                type == ItemType.Musket || type == ItemType.OneH || type == ItemType.Pistol || type == ItemType.Polearm ||
                type == ItemType.Throwing || type == ItemType.TwoH || type == ItemType.XBow || type == ItemType.OneHalf)
            {
                return ItemGender.Weapon;
            }
            else if (type == ItemType.Body || type == ItemType.Foot || type == ItemType.Hand || type == ItemType.Head)
            {
                return ItemGender.Armor;
            }
            else if (type == ItemType.Shield)
            {
                return ItemGender.Shield;
            }
            else if (type == ItemType.Horse)
            {
                return ItemGender.Horse;
            }
            else
            {
                return ItemGender.Goods;
            }
        }

        public static List<Item> GetItemsFromKind(byte[] kind, LoomLevel loomLevel, ItemSort itemSort, string startFlag, string endFlag)
        {
            List<Item> items = new List<Item>();
            int startIndex;
            List<string> lines = GetLines(kind, startFlag, endFlag, out startIndex);
            foreach (string line in lines)
                items.Add(GetItemFromLine(line, loomLevel, itemSort, startIndex++));
            //Setting up the secondary mods
            for (int i = 0; i < items.Count; i++)
                if (items[i] is Weapon && ((Weapon)items[i]).Secondary != null)
                    ((Weapon)items[i]).Secondary = items[i + 1] as Weapon;
            return items.ToList();
        }

        private enum CarryType
        {
            itcf_carry_quiver_back = 8,
            itcf_carry_axe_back = 16,
            itcf_carry_sword_back = 17,
            itcf_carry_spear = 23,
            itcf_carry_crossbow_back = 21,
            itcf_carry_bow_back = 22,
            itcf_carry_kite_shield = 18,
            itcf_carry_round_shield = 19,
            itcf_carry_board_shield = 24,
            itcf_carry_pistol_front_left = 10,
            itcf_carry_dagger_front_left = 3,
            itcf_carry_dagger_front_right = 4,
            itcf_carry_sword_left_hip = 1,
            itcf_carry_axe_left_hip = 2,
            itcf_carry_mace_left_hip = 12,
            itcf_carry_katana = 33,
            itcf_carry_wakizashi = 34,
            itcf_carry_bowcase_left = 11,
            itcf_carry_buckler_left = 20,
            itcf_carry_quiver_right_vertical = 7,
            itcf_carry_quiver_front_right = 5,
            itcf_carry_quiver_back_right = 6,
            itcf_carry_revolver_right = 9,
            cant_sheath = 0
        }

        private static Slot GetSlotFromCarryType(CarryType cType)
        {
            if (cType == CarryType.cant_sheath)
                return Slot.CantSheath;
            if (cType == CarryType.itcf_carry_quiver_back || cType == CarryType.itcf_carry_axe_back ||
                cType == CarryType.itcf_carry_sword_back || cType == CarryType.itcf_carry_spear)
                return Slot.Back1;
            if (cType == CarryType.itcf_carry_crossbow_back || cType == CarryType.itcf_carry_bow_back)
                return Slot.Back2;
            if (cType == CarryType.itcf_carry_kite_shield || cType == CarryType.itcf_carry_round_shield ||
                cType == CarryType.itcf_carry_board_shield)
                return Slot.Back3;
            if (cType == CarryType.itcf_carry_pistol_front_left || cType == CarryType.itcf_carry_dagger_front_left)
                return Slot.LeftStomach;
            if (cType == CarryType.itcf_carry_dagger_front_right)
                return Slot.RightStomach;
            if (cType == CarryType.itcf_carry_sword_left_hip || cType == CarryType.itcf_carry_axe_left_hip ||
                cType == CarryType.itcf_carry_mace_left_hip || cType == CarryType.itcf_carry_katana)
                return Slot.LeftHip1;
            if (cType == CarryType.itcf_carry_wakizashi)
                return Slot.LeftHip2;
            if (cType == CarryType.itcf_carry_bowcase_left)
                return Slot.LeftHip3;
            if (cType == CarryType.itcf_carry_buckler_left)
                return Slot.LeftHip4;
            if (cType == CarryType.itcf_carry_quiver_right_vertical || cType == CarryType.itcf_carry_quiver_front_right ||
                cType == CarryType.itcf_carry_quiver_back_right)
                return Slot.RightHip;
            if (cType == CarryType.itcf_carry_revolver_right)
                return Slot.RightUpperLeg;

            return Slot.None;
        }

        private static List<string> GetLines(byte[] kind, string startFlag, string endFlag, out int startIndex)
        {
            startIndex = 0;
            var stream = new StreamReader(new MemoryStream(kind));
            string itemsKind = stream.ReadToEnd();
            List<string> allLines = Regex.Split(itemsKind, "itm").Where(s => s.StartsWith("_")).ToList();
            for (int i = 0; i < allLines.Count; i++) { allLines[i] = "itm" + allLines[i]; } //Meh optimisation here, it's call like once every month
            List<string> lines = new List<string>();
            bool start = false;
            for(int i = 0; i < allLines.Count; i++)
            {
                if (allLines[i].Contains(endFlag))
                    break;
                if (start)
                    lines.Add(allLines[i]);

                if (allLines[i].Contains(startFlag))
                {
                    start = true;
                    startIndex = i + 1;
                }
            }
            return lines;
        }

        private static Item GetItemFromLine(string line, LoomLevel loomLevel, ItemSort itemSort, int ind)
        {
            Item item;
            string[] values = Regex.Split(line, " +");
            int offset = int.Parse(values[3]) * 2;

            byte[] flags = BitConverter.GetBytes(long.Parse(values[4 + offset]));
            byte[] caps = BigInteger.Parse(values[5 + offset]).ToByteArray();
            BitArray flagsBits = new BitArray(flags);
            BitArray capsBits = new BitArray(caps);

            ItemType type = (ItemType)flags[0];
            int gameId = ind;
            string nameId = values[0];
            int value = int.Parse(values[6 + offset]);
            string name = values[1].Replace('_', ' ');
            int difficulty =  int.Parse(values[13 + offset]);

            switch (ItemB.GetGenderFromType(type))
            {
                case ItemGender.Weapon:
                    item = new Weapon()
                    {
                        NameId = nameId,
                        GameId = gameId,
                        LoomLevel = loomLevel,
                        ItemSort = itemSort,
                        FromKind = true,
                        Value = value,
                        Name = name,
                        Difficulty = difficulty,
                        HitPoint = int.Parse(values[14 + offset]),
                        Type = type == ItemType.TwoH && !flagsBits.Get((int)BoolFlags.TwoHanded) ? ItemType.OneHalf : (ItemType)type,
                        Weight = float.Parse(values[8 + offset], CultureInfo.InvariantCulture),
                        Length = int.Parse(values[17 + offset]),
                        Speed = int.Parse(values[15 + offset]),
                        HorseBack = flagsBits.Get((int)BoolFlags.HorseBack) || flagsBits.Get((int)BoolFlags.HorseBackReload),
                        BonusShield = flagsBits.Get((int)BoolFlags.BonusShield),
                        Thrust = GetDamageValue(int.Parse(values[19 + offset])),
                        ThrustType = GetDamageType(int.Parse(values[19 + offset])),
                        Swing = GetDamageValue(int.Parse(values[20 + offset])),
                        SwingType = GetDamageType(int.Parse(values[20 + offset])),
                        Unbalanced = flagsBits.Get((int)BoolFlags.Unbalanced),
                        Knockdown = flagsBits.Get((int)BoolFlags.Knockdown),
                        Crushthrough = flagsBits.Get((int)BoolFlags.Crushthrough),
                        Couchable = flagsBits.Get((int)BoolFlags.Couchable),
                        MissileSpeed = int.Parse(values[16 + offset]),
                        Accuracy = int.Parse(values[12 + offset]),
                        Ammo = int.Parse(values[18 + offset]),
                        Secondary = flagsBits.Get((int)BoolFlags.NextAsMelee) ? new Weapon() : null, //Just to say it's different than null, we set this afterward
                        Slot = GetSlotFromCaps(caps)
                    };
                    return item;
                case ItemGender.Shield:
                    item = new Shield()
                    {
                        NameId = nameId,
                        GameId = gameId,
                        Value = value,
                        LoomLevel = loomLevel,
                        ItemSort = itemSort,
                        FromKind = true,
                        Name = name,
                        Difficulty = difficulty,
                        HitPoint = int.Parse(values[14 + offset]),
                        Type = (ItemType)type,
                        Weight = float.Parse(values[8 + offset], CultureInfo.InvariantCulture),
                        Armor = int.Parse(values[11 + offset]),
                        Speed = int.Parse(values[15 + offset]),
                        Height = int.Parse(values[16 + offset]) != 0 ? int.Parse(values[16 + offset]) : int.Parse(values[17 + offset]),
                        Width = int.Parse(values[17 + offset]),
                        Thrust = GetDamageValue(int.Parse(values[19 + offset])),
                        ThrustType = GetDamageType(int.Parse(values[19 + offset])),
                        HorseBack = flagsBits.Get((int)BoolFlags.HorseBack),
                        Slot = GetSlotFromCaps(caps)
                    };
                    return item;
                case ItemGender.Armor:
                    float weight = float.Parse(values[8 + offset], CultureInfo.InvariantCulture);
                    item = new Armor()
                    {
                        NameId = nameId,
                        GameId = gameId,
                        Value = value,
                        LoomLevel = loomLevel,
                        ItemSort = itemSort,
                        FromKind = true,
                        Name = name,
                        Difficulty = difficulty,
                        HitPoint = int.Parse(values[14 + offset]),
                        Type = (ItemType)type,
                        Weight = weight,
                        Head = int.Parse(values[10 + offset]),
                        Body = int.Parse(values[11 + offset]),
                        Foot = int.Parse(values[12 + offset]),
                        ArmorType = GetArmorTypeFromWeigth(weight, (ItemType)type)
                    };
                    return item;
                case ItemGender.Horse:
                    item = new Horse()
                    {
                        NameId = nameId,
                        GameId = gameId,
                        Value = value,
                        FromKind = true,
                        LoomLevel = loomLevel,
                        ItemSort = itemSort,
                        Name = name,
                        Difficulty = difficulty,
                        HitPoint = int.Parse(values[14 + offset]),
                        Type = (ItemType)type,
                        Armor = int.Parse(values[11 + offset]),
                        Maneuver = int.Parse(values[15 + offset]),
                        Speed = int.Parse(values[16 + offset]),
                        Charge = int.Parse(values[19 + offset])
                    };
                    return item;
            }

            return null;
        }

        private static int GetDamageValue(int value)
        {
            if (value < 256)
                return value;
            else if (value < 512)
                return value - 256;
            else
                return value - 512;
        }

        private static DamageType GetDamageType(int value)
        {
            if (value < 256)
                return DamageType.Cut;
            else if (value < 512)
                return DamageType.Pierce;
            else
                return DamageType.Blunt;
        }

        private enum BoolFlags
        {
            HorseBackReload = 47,
            HorseBack = 28,
            BonusShield = 26,
            Unbalanced = 35,
            Knockdown = 38,
            Crushthrough = 32,
            Couchable = 31,
            NextAsMelee = 29,
            TwoHanded = 21,
        }

        private enum ArmorGender
        {
            Cloth = 1,
            Padded = 2,
            Leather = 3,
            Mail = 4,
            Plate = 5,
        }

        private static Slot GetSlotFromCaps(byte[] caps)
        {
            if (caps.Length < 4)
                return Slot.None;

            byte byt = caps.ShiftRight(4)[3];
            CarryType cType = (CarryType)byt;
            return GetSlotFromCarryType(cType);
        }

        const float HEAD_WEIGHT_MEDIUM = 1.5f;
        const float HEAD_WEIGHT_HEAVY = 3f;

        const float BODY_WEIGHT_MEDIUM = 10f;
        const float BODY_WEIGHT_HEAVY = 20f;

        const float HAND_WEIGHT_MEDIUM = 1f;
        const float HAND_WEIGHT_HEAVY = 2.5f;

        const float FOOT_WEIGHT_MEDIUM = 2f;
        const float FOOT_WEIGHT_HEAVY = 6f;
        private static ArmorType GetArmorTypeFromWeigth(float weigth, ItemType type)
        {
            switch (type)
            {
                case ItemType.Head:
                    if (weigth >= HEAD_WEIGHT_HEAVY)
                        return ArmorType.Heavy;
                    if (weigth >= HEAD_WEIGHT_MEDIUM)
                        return ArmorType.Medium;
                    return ArmorType.Light;
                case ItemType.Body:
                    if (weigth >= BODY_WEIGHT_HEAVY)
                        return ArmorType.Heavy;
                    if (weigth >= BODY_WEIGHT_MEDIUM)
                        return ArmorType.Medium;
                    return ArmorType.Light;
                case ItemType.Hand:
                    if (weigth >= HAND_WEIGHT_HEAVY)
                        return ArmorType.Heavy;
                    if (weigth >= HAND_WEIGHT_MEDIUM)
                        return ArmorType.Medium;
                    return ArmorType.Light;
                case ItemType.Foot:
                    if (weigth >= FOOT_WEIGHT_HEAVY)
                        return ArmorType.Heavy;
                    if (weigth >= FOOT_WEIGHT_MEDIUM)
                        return ArmorType.Medium;
                    return ArmorType.Light;
            }
            return ArmorType.Light;
        }
    }
}
