﻿using BoE.Contexts;
using BoE.Lib;
using BoE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BoE.Business
{
    public static class GameB
    {
        public static Dictionary<TokenStatus, string> TokenMessages = new Dictionary<TokenStatus, string>
        {
            { TokenStatus.WrongToken, "The token is incorrect or expired, please use the launcher" },
            { TokenStatus.ServerDisabled, "The server is banned" },
        };

        const float SCORE_MULT_GOLD = 0.1f;
        const float SCORE_MULT_EXP = 1;
        const int MAX_WON_A_ROW = 4;
        public static int GetTickGold(int gold, int score, int gen, int wonInARow)
        {
            wonInARow = Math.Min(wonInARow, MAX_WON_A_ROW);
            return  Math.Min(Convert.ToInt32(gold + (score * SCORE_MULT_GOLD) + (5 * wonInARow)), 100);
        }

        public static int GetTickExp(int exp, int score, int gen, int wonInARow)
        {
            wonInARow = Math.Min(wonInARow, MAX_WON_A_ROW);
            return Math.Min(Convert.ToInt32(exp + GetGenExp(gen) + (score * SCORE_MULT_EXP) + (500 * wonInARow)), 10000);
        }

        private static int GetGenExp(int gen)
        {
            int start = 60;
            int total = 0;
            for(int i = gen - 1; i >= 0; i--)
            {
                total += Math.Max(start, 1);
                start -= 2;
            }
            return total;
        }

        const int MIN_TICK_FOR_UPKEEP = 5;
        const int UPKEEP_MOD = 1000000;
        public static int GetUpkeepValue(int itemHps, int itemAmmo, int itemValue, int usages, ItemType? type, int respawnCount, int ticksSinceLastUpkeep)
        {
            int itemValueMod;
            int priceUsageRatioMod;
            int upkeepMod;
            int maxUsage = 0;
            if (type.HasValue && type == ItemType.Arrow || type == ItemType.Throwing ||
                type == ItemType.Bolt || type == ItemType.Bullet)
            {
                maxUsage = itemAmmo;
                if (type.Value == ItemType.Throwing)
                    maxUsage *= itemHps;

                maxUsage *= 2;
            }
            else
                maxUsage = itemHps;

            if (type == ItemType.Horse)
                maxUsage *= 10;
            else if(type == ItemType.Shield)
                maxUsage *= 4;

            itemValueMod = itemValue * UPKEEP_MOD;
            priceUsageRatioMod = itemValueMod / maxUsage;
            upkeepMod = priceUsageRatioMod * usages;
            upkeepMod *= ticksSinceLastUpkeep;
            upkeepMod /= MIN_TICK_FOR_UPKEEP;
            upkeepMod /= respawnCount;

            return (int)(upkeepMod / UPKEEP_MOD);
        }

        public static bool NameIsValid(string name)
        {
            return Regex.IsMatch(name, @"^[a-zA-Z0-9_]+$") && name.Length < 40;
        }

        public static GameServer CreateGS(string address, BoEContext db = null)
        {
            if (db == null)
                db = new BoEContext();

            GameServer gs = new GameServer()
            {
                Address = address
            };
            db.GameServers.Add(gs);
            db.SaveChanges();
            return gs;
        }

        public static Token CreateToken(GameServer gs, int ttl, int userId, BoEContext db = null)
        {
            if (db == null)
                db = new BoEContext();
            Random random = new Random();
            Token token = new Token()
            {
                Dealine = DateTime.Now.AddHours(ttl),
                GameServerId = gs.GameServerId,
                Value = Utils.GenerateCode(random, 10),
                UserId = userId
            };
            db.Tokens.Add(token);
            gs.Token = token;
            db.SaveChanges();
            return token;
        }

        public static GameServer CreateGameServer(string address, string name, BoEContext db = null)
        {
            if (db == null)
                db = new BoEContext();
            GameServer gs = db.GameServers.Create();
            gs.Address = address;
            gs.Name = name;
            db.GameServers.Add(gs);
            db.SaveChanges();
            return gs;
        }

        public static TokenStatus GetTokenStatus(Token token, string name, string address)
        {
            if (token == null || !token.Enable || token.Dealine < DateTime.Now || token.GameServer.Name != name)
                return TokenStatus.WrongToken;
            if (!token.GameServer.Enabled)
                return TokenStatus.ServerDisabled;

            return TokenStatus.TokenOK;
        }

        public static bool KDIsBullShit(int kill, int death, int score)
        {
            if (kill > 20 || kill < -20)
                return true;

            if (death >= 10 || death <= -10)
                return true;

            if (kill >= 20)
                return true;

            if (kill >= 8 && score < 80)
                return true;

            if (kill < -5 && score >= 0)
                return true;

            return false;
        }
    }

    public enum GetCharStatus
    {
        Ok = 1,
        WrongName = 2,
        NameTaken = 3,
        NewUser = 4,
        Banned = 5,
        Admin = 6
    }

    public enum TokenStatus
    {
        TokenOK = 1,
        WrongToken = 2,
        ServerDisabled = 3
    }
}
