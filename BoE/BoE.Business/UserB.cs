﻿using BoE.Models;
using BoE.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoE.Lib;

namespace BoE.Business
{
    public static class UserB
    {
        public static Code GetCode(CodeType type, BoEContext db)
        {
            Code code = db.Codes.Create();
            code.Value = GetNewCode(db);
            code.Type = type;
            return code;
        }

        const int defaultWPF = 0;
        const int startAtt = 3;
        public static Build GetStartBuild(BoEContext db, Character character)
        {
            Build build = db.Builds.Create();
            build.Name = "Peasant";
            build.Strength = startAtt;
            build.Agility = startAtt;
            build.OneHanded = defaultWPF;
            build.TwoHanded = defaultWPF;
            build.Polearm = defaultWPF;
            build.Archery = defaultWPF;
            build.Crossbow = defaultWPF;
            build.Throwing = defaultWPF;
            build.Firearm = defaultWPF;
            build.Character = character;

            return build;
        }

        public static User CreateUser(int uniqueId, string charName, BoEContext db = null)
        {
            if (db == null)
                db = new BoEContext();

            User user = db.Users.Create();
            user.UniqueId = uniqueId;
            user.State = UserState.New;
            db.Users.Add(user);

            Character character = db.Characters.Create();
            character.Name = charName;
            character.User = user;
            character.Experience = Character.GetXPForLevel(1);

            string[] weaponsNameId = new string[] 
            { 
                "itm_wooden_stick",
                "itm_staff",
                "itm_heavy_practice_sword",
                "itm_cudgel"
            };
            string[] clothNameId = new string[] { 
                "itm_nobleman_outfit",
                "itm_linen_tunic",
                "itm_priest_coif"
            };
            string[] throwingNameId = new string[] {
                "itm_stones",
                "itm_throwing_knives" 
            };
            Random random = new Random();
            string wpNameId = weaponsNameId[random.Next(weaponsNameId.Length)];
            string twNameId = throwingNameId[random.Next(throwingNameId.Length)];
            string ctNameId = clothNameId[random.Next(clothNameId.Length)];

            var wp = db.Items.Select(i => new
            {
                ItemId = i.ItemId,
                NameId = i.NameId
            }).FirstOrDefault(i => i.NameId == wpNameId);
            var tw = db.Items.Select(i => new
            {
                ItemId = i.ItemId,
                NameId = i.NameId
            }).FirstOrDefault(i => i.NameId == twNameId);
            var ct = db.Items.Select(i => new
            {
                ItemId = i.ItemId,
                NameId = i.NameId
            }).FirstOrDefault(i => i.NameId == ctNameId);

            character.Inventories = new List<Inventory>();

            Inventory weapon = db.Inventories.Create();
            weapon.ItemId = wp.ItemId;
            character.Inventories.Add(weapon);

            Inventory throwing = db.Inventories.Create();
            throwing.ItemId = tw.ItemId;
            character.Inventories.Add(throwing);

            Inventory dress = db.Inventories.Create();
            dress.ItemId = ct.ItemId;
            character.Inventories.Add(dress);

            Build build = GetStartBuild(db, character);

            Code code = GetCode(CodeType.Register, db);
            user.Codes = new List<Code>() { code };

            db.Builds.Add(build);
            db.Characters.Add(character);
            db.SaveChanges();
            character.BuildId = build.BuildId;
            build.Item1Id = weapon.InventoryId;
            build.Item2Id = throwing.InventoryId;
            build.BodyId = dress.InventoryId;
            db.SaveChanges();

            return user;
        }

        public static string GetNewCode(BoEContext db)
        {
            Random random = new Random();
            string code;
            do
            {
                code = Utils.GenerateCode(random, 5);
            }
            while (db.Codes.FirstOrDefault(c => c.Value == code) != null);

            return code;
        }
    }
}
